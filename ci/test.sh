#! /bin/sh
#
# test.sh
# Copyright (C) 2017 Mateusz Pawlowski <mateusz@generik.co.uk>
#
# Distributed under terms of the MIT license.
#


echo "tested in build step"

mvn clean test -Denv.name="dev" -Dfrom="jenkins" -Dcucumber.options="--tag @migration"

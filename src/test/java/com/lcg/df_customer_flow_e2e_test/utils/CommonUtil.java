package com.lcg.df_customer_flow_e2e_test.utils;

import static com.lcg.df_customer_flow_e2e_test.helpers.PlayerDataHelper.BRAND_SEQ_COLUMN;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomUtils;

import com.lcg.df_customer_flow_e2e_test.helpers.BrandMap;
import com.lcg.df_customer_flow_e2e_test.pojos.CommonState;
import com.lcg.df_customer_flow_e2e_test.pojos.Player;
import com.lcg.df_customer_flow_e2e_test.salesforce.SalesforceClient;
import com.lcg.df_customer_flow_e2e_test.salesforce.SalesforceHandler;

import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommonUtil {

    private static SalesforceClient salesforceClient = new SalesforceClient();

    public static int generateRandomCustomerId() {
        return Integer.valueOf(String.valueOf(RandomUtils.nextInt(100000000, 999999999)));
    }

    public static Map<String, Object> createSalesforceRequestPayload(Integer brandSeq, Integer customerId) {
        Map<String, Object> playerRecord = new HashMap<>();
        playerRecord.put("LastName", "My name is Workaround");
        playerRecord.put("Player_Id__c", customerId);
        playerRecord.put("Brand__c", BrandMap.getBrand(brandSeq));
        return playerRecord;
    }

    public static void upsertPlayerToSalesforce(Integer brandSeq, Integer customerId) {
        Map<String, Object> playerRecord = createSalesforceRequestPayload(brandSeq, customerId);
        String targetCustomerIdentifier = getTargetCustomerIdentifier(customerId, brandSeq);
        Response resp = salesforceClient.upsertPlayer(targetCustomerIdentifier, playerRecord);

        if (resp.getBody().asString().equals("")) {
            assertThat(resp.statusCode(), equalTo(204));
        } else {
            assertThat(resp.statusCode(), equalTo(201));
        }
    }

    public static void verifyExpectedDataIsInSalesforce(Integer customerId, String targetCustomerIdentifier, Map<String, String> assertion) {
        getSalesforceHandler().verifyExpectedDataIsInSalesforce(customerId, targetCustomerIdentifier, assertion);
    }

    public static void verifyExpectedPlayerDataIsInSalesforce(Player p, String extractionType, String batchNumber) {
        getSalesforceHandler().resetPolling();
        Integer customerId = p.getCustomerId();

        List<Map<String, String>> assertions = (batchNumber != null)? new ArrayList<Map<String,String>>() : p.getDataToAssert();
        assertions.add(getPlayerIdAssertion(p));
        assertions.add(getCustomerIdentifierAssertion(p));
        assertions.add(getDfExtractionDateAssertion(p));
        assertions.add(getBatchNumberAssertion(p, extractionType, batchNumber));
        assertions.stream().forEach(assertion -> verifyExpectedDataIsInSalesforce(
                customerId,
                getTargetCustomerIdentifier(customerId, Integer.parseInt(assertion.get(BRAND_SEQ_COLUMN))),
                assertion));
        assertions.remove(getBatchNumberAssertion(p, extractionType, batchNumber));
    }

    public static String getTargetBrandName(Integer brandSeq) {
        return BrandMap.getTargetBrand(brandSeq);
    }

    public static String getTargetCustomerIdentifier(Integer customerId, Integer brandSeq) {
        String targetBrandName = getTargetBrandName(brandSeq);
        return String.format("%s_%d", targetBrandName, customerId);
    }

    public static SalesforceHandler getSalesforceHandler() {
        return CommonState.getCommonState().getSalesforceHandler();
    }

    public static SalesforceClient getSalesforceClient() {
        return salesforceClient;
    }

    public static Integer getBrandSeq(Map<String, String> dataRow) {
        return Integer.parseInt(dataRow.get(BRAND_SEQ_COLUMN));
    }

    public static void waitForEmrClusterToExtractTheData(int secondsToWait) {
        log.info("Waiting {}s for EMR cluster to do its job...", secondsToWait);
        try {
            Thread.sleep(secondsToWait * 1000);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
    }

    private static Map<String, String> getPlayerIdAssertion(Player p) {
        int customerId = p.getCustomerId();
        String brandSeq = p.getDataToAssert().get(0).get("brand_seq");

        Map<String, String> playedIdAssertion = new HashMap<>();
        playedIdAssertion.put("ApiName", "Player_Id__c");
        playedIdAssertion.put("Value", String.valueOf(customerId));
        playedIdAssertion.put("brand_seq", brandSeq);
        return playedIdAssertion;
    }

    private static Map<String, String> getCustomerIdentifierAssertion(Player p) {
        int customerId = p.getCustomerId();
        int brandSeq = Integer.parseInt(p.getDataToAssert().get(0).get("brand_seq"));

        Map<String, String> playedIdAssertion = new HashMap<>();
        playedIdAssertion.put("ApiName", "IMS_Customer_Identifier__c");
        playedIdAssertion.put("Value", getTargetCustomerIdentifier(customerId, brandSeq));
        playedIdAssertion.put("brand_seq", String.valueOf(brandSeq));
        return playedIdAssertion;
    }

    private static Map<String, String> getDfExtractionDateAssertion(Player p) {
        String brandSeq = p.getDataToAssert().get(0).get("brand_seq");
        String extractionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        Map<String, String> DfExtractionDateAssertion = new HashMap<>();
        DfExtractionDateAssertion.put("ApiName", "DF_ExtractionDate__c");
        DfExtractionDateAssertion.put("Value", extractionDate);
        DfExtractionDateAssertion.put("brand_seq", brandSeq);
        return DfExtractionDateAssertion;
    }


    private static Map<String, String> getBatchNumberAssertion(Player p, String extractionType, String batchNumber) {
        String brandSeq = p.getDataToAssert().get(0).get("brand_seq");
        String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String dplBatchNumber = null;
        if (extractionType.contains("DIFF")) {
            dplBatchNumber = extractionType + "-" + date + "_" + batchNumber;
        } else if (extractionType.contains("MIGRATION")) {
            dplBatchNumber = extractionType + "-" + date;
        }
        Map<String, String> dplBatchNumberAssertion = new HashMap<>();
        dplBatchNumberAssertion.put("ApiName", "DPL_Batch__c");
        dplBatchNumberAssertion.put("Value", dplBatchNumber);
        dplBatchNumberAssertion.put("brand_seq", brandSeq);
        return dplBatchNumberAssertion;
    }
}

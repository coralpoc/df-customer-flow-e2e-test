package com.lcg.df_customer_flow_e2e_test.pojos;

import static com.lcg.df_customer_flow_e2e_test.utils.CommonUtil.generateRandomCustomerId;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class Player {

    private Integer customerSeq;
    private Integer customerId;
    private List<Map<String,String>> dataToInsert;
    private List<Map<String,String>> dataToAssert;
    private String folderName;

    public Player() {
        customerId = generateRandomCustomerId();
        customerSeq = customerId;
    }
}

package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class BalanceRecord extends BaseRecord  {

    private BigDecimal bonusBalance = new BigDecimal(BigInteger.ZERO);
    private BigDecimal cashBalance = new BigDecimal(BigInteger.ZERO);
    private Timestamp balanceUpdateTime = new Timestamp(0);
    private BigDecimal compPoints = new BigDecimal(BigInteger.ZERO);
    private BigDecimal buzzPointsBalance = new BigDecimal(BigInteger.ZERO);
    private Timestamp ETL_UPDATE_DATE = getETLUpdateTimestamp();
}

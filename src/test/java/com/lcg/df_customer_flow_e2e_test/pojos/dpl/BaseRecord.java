package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.math.MathContext.DECIMAL128;

@Getter
@Setter
@Slf4j
public class BaseRecord {

    private int CUSTOMER_SEQ;
    private int CUSTOMER_ID;
    private int BRAND_SEQ;

    public void setValue(String column, Object value) {
        if (value == null) {
            ReflectionTestUtils.setField(this, column, value);
        } else {
            try {
                if (ReflectionTestUtils.getField(this, column).getClass().getSimpleName().equals("BigDecimal")) {
                    BeanUtils.setProperty(this, column, new BigDecimal(value.toString(), DECIMAL128));
                } else {
                    BeanUtils.setProperty(this, column, value);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException("Something went wrong while setting object properties. It never happened before, so if it happened to you, you may feel special. Very special", e);
            }
        }
    }

    public Timestamp getETLUpdateTimestamp() {
        LocalDateTime ldt = LocalDateTime.now();
        ZonedDateTime zdt = ZonedDateTime.of(ldt, ZoneId.systemDefault());
        ZonedDateTime gmt = zdt.withZoneSameInstant(ZoneId.of("UTC"));
        return Timestamp.valueOf(gmt.toLocalDateTime());
    }

}

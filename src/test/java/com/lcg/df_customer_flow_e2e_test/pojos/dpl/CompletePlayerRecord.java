package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;

/**
 * Representation of a complete player entry in the database.
 * Represents relevant fields in all relevant tables.
 */
@Getter
public class CompletePlayerRecord extends BaseRecord{

    private BalanceRecord balanceRecord = new BalanceRecord();
    private BetEarlyRecord betEarlyRecord = new BetEarlyRecord();
    private BetLifeRecord betLifeRecord = new BetLifeRecord();
    private CustomerInfoEarlyRecord customerInfoEarlyRecord = new CustomerInfoEarlyRecord();
    private CustomerInfoLifeRecord customerInfoLifeRecord = new CustomerInfoLifeRecord();
    private DepositEarlyRecord depositEarlyRecord = new DepositEarlyRecord();
    private DepositLifeRecord depositLifeRecord = new DepositLifeRecord();
    private DepositRelativeRecord depositRelativeRecord = new DepositRelativeRecord();
    private LoginRecord loginRecord = new LoginRecord();
    private PredictionsRecord predictionsRecord = new PredictionsRecord();
    private SegmentationsRecord segmentationsRecord = new SegmentationsRecord();
    private StatusRecord statusRecord = new StatusRecord();
    private RetailCustomersConnectRecord retailCustomersConnectRecord = new RetailCustomersConnectRecord();
    private SfMasterSubcribersRecord sfMasterSubcribersRecord = new SfMasterSubcribersRecord();

    public CompletePlayerRecord(int customerSeq, int customerId, int brandSeq) {

        // TODO: remove once Francois' fix is in
        customerInfoEarlyRecord.setPlayerID(customerId);

        loginRecord.setCUSTOMER_SEQ(customerSeq);
        balanceRecord.setCUSTOMER_SEQ(customerSeq);
        betEarlyRecord.setCUSTOMER_SEQ(customerSeq);
        betLifeRecord.setCUSTOMER_SEQ(customerSeq);
        customerInfoEarlyRecord.setCUSTOMER_SEQ(customerSeq);
        customerInfoLifeRecord.setCUSTOMER_SEQ(customerSeq);
        depositEarlyRecord.setCUSTOMER_SEQ(customerSeq);
        depositLifeRecord.setCUSTOMER_SEQ(customerSeq);
        depositRelativeRecord.setCUSTOMER_SEQ(customerSeq);
        loginRecord.setCUSTOMER_SEQ(customerSeq);
        predictionsRecord.setCUSTOMER_SEQ(customerSeq);
        segmentationsRecord.setCUSTOMER_SEQ(customerSeq);
        statusRecord.setCUSTOMER_SEQ(customerSeq);
        sfMasterSubcribersRecord.setCUSTOMER_SEQ(customerSeq);

        loginRecord.setCUSTOMER_ID(customerId);
        balanceRecord.setCUSTOMER_ID(customerId);
        betEarlyRecord.setCUSTOMER_ID(customerId);
        betLifeRecord.setCUSTOMER_ID(customerId);
        customerInfoEarlyRecord.setCUSTOMER_ID(customerId);
        customerInfoLifeRecord.setCUSTOMER_ID(customerId);
        depositEarlyRecord.setCUSTOMER_ID(customerId);
        depositLifeRecord.setCUSTOMER_ID(customerId);
        depositRelativeRecord.setCUSTOMER_ID(customerId);
        loginRecord.setCUSTOMER_ID(customerId);
        predictionsRecord.setCUSTOMER_ID(customerId);
        segmentationsRecord.setCUSTOMER_ID(customerId);
        statusRecord.setCUSTOMER_ID(customerId);
        retailCustomersConnectRecord.setCUSTOMER_ID(customerId);
        sfMasterSubcribersRecord.setCUSTOMER_ID(customerId);

        loginRecord.setBRAND_SEQ(brandSeq);
        balanceRecord.setBRAND_SEQ(brandSeq);
        betEarlyRecord.setBRAND_SEQ(brandSeq);
        betLifeRecord.setBRAND_SEQ(brandSeq);
        customerInfoEarlyRecord.setBRAND_SEQ(brandSeq);
        customerInfoLifeRecord.setBRAND_SEQ(brandSeq);
        depositEarlyRecord.setBRAND_SEQ(brandSeq);
        depositLifeRecord.setBRAND_SEQ(brandSeq);
        depositRelativeRecord.setBRAND_SEQ(brandSeq);
        loginRecord.setBRAND_SEQ(brandSeq);
        predictionsRecord.setBRAND_SEQ(brandSeq);
        segmentationsRecord.setBRAND_SEQ(brandSeq);
        statusRecord.setBRAND_SEQ(brandSeq);
        retailCustomersConnectRecord.setBRAND_SEQ(brandSeq);
        sfMasterSubcribersRecord.setBRAND_SEQ(brandSeq);
    }

    public void setPlayerData(String table, String column, Object value) {
        Object data = value;
        if (value.toString().equals("null")) {
            data = null;
        }
        switch (table.toUpperCase()) {
            case "SFMC_BALANCE":
                balanceRecord.setValue(column, data);
                break;
            case "SFMC_BET_EARLY":
                betEarlyRecord.setValue(column, data);
                break;
            case "SFMC_BET_LIFE":
                betLifeRecord.setValue(column, data);
                break;
            case "SFMC_CUSTOMER_INFO_EARLY":
                customerInfoEarlyRecord.setValue(column, data);
                break;
            case "SFMC_CUSTOMER_INFO_LIFE":
                customerInfoLifeRecord.setValue(column, data);
                break;
            case "SFMC_DEPOSIT_EARLY":
                depositEarlyRecord.setValue(column, data);
                break;
            case "SFMC_DEPOSIT_LIFE":
                depositLifeRecord.setValue(column, data);
                break;
            case "SFMC_DEPOSIT_RELATIVE":
                depositRelativeRecord.setValue(column, data);
                break;
            case "SFMC_LOGIN":
                loginRecord.setValue(column, data);
                break;
            case "SFMC_PREDICTIONS":
                predictionsRecord.setValue(column, data);
                break;
            case "SFMC_SEGMENTATIONS":
                segmentationsRecord.setValue(column, data);
                break;
            case "SFMC_STATUS":
                statusRecord.setValue(column, data);
                break;
            case "DIM_RTL_CUSTOMERS_CONNECT":
                retailCustomersConnectRecord.setValue(column, data);
                break;
            case "SF_MASTER_SUBCRIBERS":
                sfMasterSubcribersRecord.setValue(column, data);
                break;
            default:
                String errorMessage = String.format("Unknown table name: %s", table);
                throw new RuntimeException(errorMessage);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
public class RetailCustomersConnectRecord extends BaseRecord {

    private String mobileVerified = "";
    private String emailVerified = "";
    private String lastShopPlayedin = "";
    private String lastShopPlayedInName = "";
    private String lastRouletteFOBTBetDate = "";
    private String lastSlotsFOBTBetDate = "";
    private String latstFOBTOtherBetDate = "";
    private String lastFootballOTCBetDate = "";
    private String hasBetFOBTRetail = "";
    private String hasBetSSBTRetail = "";
    private String hasBetIpRetail = "";
    private String retailFOBTMasterSegmentFlag = "";
    private String preferredStoreName = "";
    private BigDecimal fobtTotalStakeAmt = BigDecimal.ZERO;
    private Integer fobtPlayerDays = new Integer(0);
    private BigDecimal fobtTotalStakeOver50Amt = BigDecimal.ZERO;
    private BigDecimal fobtLast7daysStakeAmt = BigDecimal.ZERO;
    private Integer fobtLast7daysPlayerDays = new Integer(0);
    private BigDecimal fobtLast28DaysStakeAmt = BigDecimal.ZERO;
    private Integer fobtLast28daysPlayerDays = new Integer(0);
    private String retailLifeCycleSegmentFlag = "";
    private BigDecimal fobtTotalBonusAmtRedeemed = BigDecimal.ZERO;
    private String lastOTCOtherBetDate = "";
    private BigDecimal otcTotalStakeAmt = BigDecimal.ZERO;
    private Integer otcPlayerDays = new Integer(0);
    private BigDecimal otcLast7daysStakeAmt = BigDecimal.ZERO;
    private Integer otcLast7daysPlayerDays = new Integer(0);
    private BigDecimal otcLast28DaysStakeAmt = BigDecimal.ZERO;
    private Integer otcLast28daysPlayerDays = new Integer(0);
    private BigDecimal otcTotalBonusAmtRedeemed = BigDecimal.ZERO;
    private BigDecimal bipTotalStakeAmt = BigDecimal.ZERO;
    private Integer bipPlayerDays = new Integer(0);
    private String firstRetailSSBTBetDate = "";
    private String latestRetailSSBTBetDate = "";
    private BigDecimal ssbtTotalStakeAmt = BigDecimal.ZERO;
    private Integer ssbtPlayerDays = new Integer(0);
    private String latestRetailVIPBetDate = "";
    private BigDecimal retailVIPTotalStakes = BigDecimal.ZERO;
    private Integer retailVIPTotalPlayerDays = new Integer(0);
    private BigDecimal retailVIPTotalStakesOver50 = BigDecimal.ZERO;
    private BigDecimal retailVIPLast28DayStakes = BigDecimal.ZERO;
    private Integer retailVIPLast28DayPlayerDays = new Integer(0);
    private BigDecimal retailIVIPLast28DayStakesOvr50 = BigDecimal.ZERO;
    private String bonusAbuserFOBT = "";
    private String bonusAbuserOTC = "";
    private String retailFallowCell = "";
    private String retailOTCCRMStatus = "";
    private String currentRetailVIP = "";
    private String acctVRFlag = "";
    private String retailCustomerType = "";
    private String retailFOBTCrmStatus = "";
    private String hasBetOTCRetail = "";
    private String processingDateId = new SimpleDateFormat("yyyyMMdd").format(new Date());
}

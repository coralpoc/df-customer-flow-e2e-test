package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class CustomerInfoEarlyRecord extends BaseRecord {

    private String emailAddress = "";
    private int playerID;
    private String sportsbookCustId = "";
    private String bingoVFUsername = "";
    private String username = "";
    private String mobileNumber = "";
    private String homeNumber = "";
    private String firstName = "";
    private String lastName = "";
    private String address1 = "";
    private String city = "";
    private String postcode = "";
    private String brand = "";
    private String country = "";
    private String gender = "M";
    private Timestamp DOB = new Timestamp(0);
    private String bettingCurrency = "";
    private String IMSProfileId = "";
    private String signupProdVertical = "";
    private Timestamp regDateTime = new Timestamp(0);
    private String ukCountry = "";
    private Timestamp ETL_UPDATEDATE_CUST_INFO_EARLY = getETLUpdateTimestamp();
}

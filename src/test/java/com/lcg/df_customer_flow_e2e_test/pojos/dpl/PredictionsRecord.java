package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class PredictionsRecord extends BaseRecord {

    private String pvipFlag = "False";
    private BigDecimal pvipScore = new BigDecimal(BigInteger.ZERO);
    private BigDecimal predLtv = new BigDecimal(BigInteger.ZERO);
    private BigDecimal predChurnScore = new BigDecimal(BigInteger.ZERO);
    private String predWiseGuyFlag = "False";
    private String predChurnFlag = "False";
    private BigDecimal predWiseGuyScore = new BigDecimal(BigInteger.ZERO);
    private Integer featurespaceScore = new Integer(0);
    private Integer featurespaceReason = new Integer(0);
    private Integer bonusUpliftPrediction = new Integer(0);
    private BigDecimal pharmScore = new BigDecimal(BigInteger.ZERO);
    private Integer pharmReason = new Integer(0);
    private Timestamp ETL_UPDATE_DATE_PREDICTIONS = getETLUpdateTimestamp();
}

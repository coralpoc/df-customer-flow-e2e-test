package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class DepositEarlyRecord extends BaseRecord {

    private Timestamp firstDepositDateTime = new Timestamp(0);
    private Timestamp secondDepositDateTime = new Timestamp(0);
    private Timestamp thirdDepositDateTime = new Timestamp(0);
    private BigDecimal firstDepositAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal secondDepositAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal thirdDepositAmt = new BigDecimal(BigInteger.ZERO);
    private Timestamp firstDeclinedDepositDate = new Timestamp(0);
    private Timestamp firstShopDepositDate = new Timestamp(0);
    private BigDecimal firstShopDepositAmt = new BigDecimal(BigInteger.ZERO);
    private Timestamp ETL_UPDATE_DATE_DEPOSIT_EARLY = getETLUpdateTimestamp();
}

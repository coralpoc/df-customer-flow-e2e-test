package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class CustomerInfoLifeRecord extends BaseRecord {

    private String accountType = "";
    private String VIPLevel = "";
    private Timestamp VIPLevelChangeDate = new Timestamp(0);
    private Integer creditCardExpiryDate = new Integer(0);
    private Integer last4CardDigts = new Integer(0);
    private String sportsbookStakeFactor = "";
    private int bonusAbuse;
    private String shopCardCust = "";
    private Timestamp shopCardDateTime = new Timestamp(0);
    private String shopCardRegStore = "";
    private Timestamp sportsStakeFacChangeDate = new Timestamp(0);
    private String hasLadbrokesAccount = "";
    private String hasCoralAccount = "";
    private String hasGalaBingoAccount = "";
    private String hasGalaCasinoAccount = "";
    private String salutation = "";
    private Integer doNotCall = new Integer(0);
    private Integer promoEmail = new Integer(0);
    private Integer promoSMS = new Integer(0);
    private Integer promoDirectMail = new Integer(0);
    private Integer promoPhone = new Integer(0);
    private String accountStatusIMS = "";
    private String verificationStatus = "";
    private Timestamp selfExclusionEndDate = new Timestamp(0);
    private Timestamp selfExclusionStartDate = new Timestamp(0);
    private Timestamp ETL_UPDATEDATE_CUST_INFO_LIFE = getETLUpdateTimestamp();
}

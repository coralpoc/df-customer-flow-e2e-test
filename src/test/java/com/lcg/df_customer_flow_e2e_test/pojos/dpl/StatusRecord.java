package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class StatusRecord extends BaseRecord {

    private String custPreviousVIP = "";
    private String custsPreviousVIPLevel = "";
    private String accountStatus = "";
    private String optinEmail = "";
    private String optinSMS = "";
    private Integer optinPush = new Integer(0);
    private String optinDirectMail = "";
    private String optinOBTM = "";
    private String pokerCRMStatus = "";
    private String sportsCRMStatus = "";
    private String casinoCRMStatus = "";
    private String bingoCRMStatus = "";
    private String selfExclude = "";
    private String nearestRetailShop = "";
    private String shopCardAccountType = "";
    private BigDecimal distanceToShop = new BigDecimal(BigInteger.ZERO);
    private String referredAFriend = "";
    private String sportsStakeFactorPreValue = "";
    private String autoSuppress = "";
    private BigDecimal retailDistanceToPreferredShop = new BigDecimal(BigInteger.ZERO);
    private String retailNearestShopName = "";
    private Timestamp crmAccountStatusLastUpdated = new Timestamp(0);
    private Timestamp autoSuppressLastUpdated = new Timestamp(0);
    private Timestamp consentExpiryDate = new Timestamp(0);
    private Timestamp ETL_UPDATE_DATE_STATUS = getETLUpdateTimestamp();
}

package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class LoginRecord extends BaseRecord {

    private Timestamp lastLoginDate = new Timestamp(0);
    private Timestamp dateLastFailedLogin = new Timestamp(0);
    private Timestamp ETL_UPDATE_DATE_WEBSITE_LIFE = getETLUpdateTimestamp();
}

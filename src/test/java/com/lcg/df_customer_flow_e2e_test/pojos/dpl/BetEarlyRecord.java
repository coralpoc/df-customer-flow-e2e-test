package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class BetEarlyRecord extends BaseRecord {

    private Timestamp firstBetDateTime = new Timestamp(0);
    private String firstBetProdType = "";
    private BigDecimal firstBetAmt = new BigDecimal(BigInteger.ZERO);
    private String firstBetOutcomeWinLose = "";
    private String secondBetTypeProd = "";
    private BigDecimal secondBetAmtProd = new BigDecimal(BigInteger.ZERO);
    private String secondBetOutcome = "";
    private String thirdBetTypeProd = "";
    private BigDecimal thirdBetAmtProd = new BigDecimal(BigInteger.ZERO);
    private String thirdBetOutcome = "";
    private Timestamp sportsFirstBetDate = new Timestamp(0);
    private Timestamp casinoFirstBetDate = new Timestamp(0);
    private Timestamp bingoFirstBetDate = new Timestamp(0);
    private Timestamp pokerFirstBetDate = new Timestamp(0);
    private Timestamp ETL_UPDATE_DATE_BET_EARLY = getETLUpdateTimestamp();
}

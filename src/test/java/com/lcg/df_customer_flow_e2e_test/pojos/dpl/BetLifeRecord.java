package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class BetLifeRecord extends BaseRecord {

    private Timestamp lastBetDateTime = new Timestamp(0);
    private String lastBetProdType = "";
    private BigDecimal lastBetAmt = new BigDecimal(BigInteger.ZERO);
    private String lastBetOutcomeWinLose = "";
    private BigDecimal totalNumBets = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalWinAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalLossAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalGrossWinAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalNGR = new BigDecimal(BigInteger.ZERO);
    private int numActiveDays;
    private Timestamp lastUnsettledBetDate = new Timestamp(0);
    private String preferredBingoRoom = "";
    private String mobilePlayer = "";
    private String cashoutCust = "";
    private String partialCashoutCust = "";
    private String hasBetOnCasino = "";
    private String hasBetInPlay = "";
    private Timestamp sportsLastBetDate = new Timestamp(0);
    private BigDecimal sportsTotalStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal sportsTotalNGRAmt = new BigDecimal(BigInteger.ZERO);
    private Timestamp casinoLastBetDate = new Timestamp(0);
    private BigDecimal casinoTotalStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal casinoTotalNGRAmt = new BigDecimal(BigInteger.ZERO);
    private Timestamp bingoLastBetDate = new Timestamp(0);
    private BigDecimal bingoTotalStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal bingoTotalNGRAmt = new BigDecimal(BigInteger.ZERO);
    private Timestamp pokerLastBetDate = new Timestamp(0);
    private BigDecimal pokerTotalStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal pokerTotalNGRAmt = new BigDecimal(BigInteger.ZERO);
    private String betOnline = "";
    private Timestamp secToLastBetDate = new Timestamp(0);
    private Timestamp lastPlayedSlotsDate = new Timestamp(0);
    private Timestamp lastUsedMainApp = new Timestamp(0);
    private Timestamp lastUsedSlotsGamesApp = new Timestamp(0);
    private Timestamp ETL_UPDATE_DATE_BET_LIFE = getETLUpdateTimestamp();
    private BigDecimal sports3mnthAvgStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal casino3mnthAvgStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal bingo3mnthAvgStakeAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal liveCasino3mnthAvgStakeAmt = new BigDecimal(BigInteger.ZERO);
}

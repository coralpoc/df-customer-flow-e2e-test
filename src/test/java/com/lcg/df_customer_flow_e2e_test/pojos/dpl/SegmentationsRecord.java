package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class SegmentationsRecord extends BaseRecord {

    private String custProdSegmntTopLevel = "";
    private String custProdSegmntMidLevel = "";
    private String custProdSegmntLowLevel = "";
    private String brandBehavSuperSegmnt = "";
    private Integer brandBehavSubSegmnt = new Integer(0);
    private String brandValueSuperSegmnt = "";
    private Integer brandValueSubSegmnt = new Integer(0);
    private String sourceOfPlaySegmnt = "";
    private String custChannelSegmnt = "";
    private String shopCardMgmntSegmnt = "";
    private String lifecycleSegmnt = "";
    private String custSportsBetSegmnt = "";
    private String custSportsMarketSegmnt = "";
    private String custSportsTimeSegmnt = "";
    private String custBonusSegmnt = "";
    private String preferredDepositingQOM = "";
    private String preferredDepositingDOW = "";
    private String paymentPattern = "";
    private String custCohortPersona = "";
    private Timestamp ETL_UPDATEDATE = getETLUpdateTimestamp();
}

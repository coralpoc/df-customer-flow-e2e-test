package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class DepositLifeRecord extends BaseRecord {

    private BigDecimal totalNumDeposit = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalAmtDeposited = new BigDecimal(BigInteger.ZERO);
    private Timestamp lastDepositDate = new Timestamp(0);
    private BigDecimal lastDepositAmt = new BigDecimal(BigInteger.ZERO);
    private Integer totalNumDepositDays = new Integer(0);
    private Timestamp lastDeclinedDepositDate = new Timestamp(0);
    private BigDecimal lastDepositDayAmt = new BigDecimal(BigInteger.ZERO);
    private Integer numFailedDeposit = new Integer(0);
    private BigDecimal retailLastShopDepositAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal retailTotalAmtDeposited = new BigDecimal(BigInteger.ZERO);
    private Timestamp retailLastWithdrawDate = new Timestamp(0);
    private BigDecimal retailLastWithdrawAmt = new BigDecimal(BigInteger.ZERO);
    private BigDecimal totalRetailAmtWithdraw = new BigDecimal(BigInteger.ZERO);
    private Timestamp retailLastShopDepsitDate = new Timestamp(0);
    private Timestamp ETL_UPDATE_DATE_DEPOSIT_LIFE = getETLUpdateTimestamp();
    private Timestamp lastWithdrawalDate = new Timestamp(0);
}

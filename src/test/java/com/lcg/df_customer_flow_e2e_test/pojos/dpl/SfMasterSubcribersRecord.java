package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;
import java.sql.Date;
import java.text.SimpleDateFormat;

@Setter
@Getter
public class SfMasterSubcribersRecord extends BaseRecord{

    private Date firstRetailBIPBetDate = new Date(System.currentTimeMillis());
    private Date firstRetailFOBTBetDate = new Date(System.currentTimeMillis());
    private Date firstRetailOTCBetDate = new Date(System.currentTimeMillis());
    private Date lastRetailBIPBetDate = new Date(System.currentTimeMillis());
    private Date lastRetailFOBTBetDate = new Date(System.currentTimeMillis());
    private Date lastRetailOTCBetDate = new Date(System.currentTimeMillis());

    // Not nullable fields and not in scope for testing from SDW MSF table as these fields are extracting from DPL
    private String emailAddress = "test@test.com";
    private String username = "tester";
    private String firstName = "tester firstname";
    private String lastName = "tester lastname";
    private String address1 = "1 teser road";
    private String city = "tester city";
    private String postcode = "AB1 2YZ";
    private String country = "United Kingdom";
    private String gender = "M";
    private Date dob = java.sql.Date.valueOf("1985-08-20");
    private String bettingCurrency = "GBP";
    private Date regDateTime = java.sql.Date.valueOf("2016-08-20");
    private int daysSinceReg = 377;
    private String accountType = "online";
    private String macroSegmnt = "test segmt";
    private int vipLevel = 23;
    private Date lastLoginDate = java.sql.Date.valueOf("2017-08-20");
    private int daysSinceLastLogin = 262;
    private Double currentBalance = 34.90;
    private Double bonusBalance = 34.90;
    private Double cashBalance = 34.90;
    private String accountStatus = "Active";
    private String optInEmail = "None";
    private String optInSMS = "None";
    private String optInPush = "None";
    private String optInDirectMail = "None";
    private String optInOBTM = "None";
    private String selfExclude = "true";
    private String autosuppress = "testing";
    private int loadDateId = Integer.valueOf(new SimpleDateFormat("yyyyMMdd").format(new java.util.Date()));

}

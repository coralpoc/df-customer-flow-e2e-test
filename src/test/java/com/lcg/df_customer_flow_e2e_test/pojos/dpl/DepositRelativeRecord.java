package com.lcg.df_customer_flow_e2e_test.pojos.dpl;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Getter
@Setter
public class DepositRelativeRecord extends BaseRecord {

    private int numDepositLast7Days;
    private BigDecimal totalAmtDepositLast7Days = new BigDecimal(BigInteger.ZERO);
    private int numDepositLast28Days;
    private BigDecimal totalAmtDepositLast28Days = new BigDecimal(BigInteger.ZERO);
    private int numDepositPrevPeriod;
    private BigDecimal totalAmtDepositPrevPeriod = new BigDecimal(BigInteger.ZERO);
    private int numDepositCurrPeriod;
    private BigDecimal totalAmtDepositCurrPeriod = new BigDecimal(BigInteger.ZERO);
    private BigDecimal retailAmtDepositLast7Days = new BigDecimal(BigInteger.ZERO);
    private BigDecimal retailAmtDepositLast28Days = new BigDecimal(BigInteger.ZERO);
    private BigDecimal retailAmtWithdrawLast7Days = new BigDecimal(BigInteger.ZERO);
    private BigDecimal retailAmtWithdrawLast28Days = new BigDecimal(BigInteger.ZERO);
    private Timestamp ETL_UPDATE_DATE_DEPOSIT_RELATIVE = getETLUpdateTimestamp();
}

package com.lcg.df_customer_flow_e2e_test.pojos;

import java.util.ArrayList;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.salesforce.SalesforceHandler;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class CommonState {

    private static CommonState commonState = new CommonState();
    private List<String> tables;

    @Setter
    private SalesforceHandler salesforceHandler = new SalesforceHandler();

    @Setter
    private List<Player> players;

    private CommonState() {
        tables = new ArrayList<>();
        players = new ArrayList<>();
    }

    public void resetTables() {
        log.info("Resetting tables");
        tables = new ArrayList<>();
    }

    public void resetSalesforceHandler() {
        log.info("Resetting salesforceHandler");
        salesforceHandler = new SalesforceHandler();
    }

    public void addTable(String tableName) {
        tables.add(tableName);
    }

    public static CommonState getCommonState() {
        return commonState;
    }
}

package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerNetworkRecord extends BaseRecord {

    private String bingoNickName = "test bingo nack name";
    private String derUpdateTime;

}

package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class PlayerSelfExclusionsRecord extends BaseRecord {

    private String type = "2";
    private String derUpdateTime;
    private Date startDate = null;
    private Date endDate = null;
}

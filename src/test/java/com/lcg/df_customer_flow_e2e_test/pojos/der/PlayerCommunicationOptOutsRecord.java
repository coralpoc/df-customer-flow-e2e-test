package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class PlayerCommunicationOptOutsRecord extends BaseRecord {

    private int playerCode;
    private String casinoName;
    private String channelType = "0";
    private String optOutType = "0";
    private Date syncDate = null;
    private String derUpdateTime;
}

package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Setter
@Getter
public class PlayerPersonalRecord extends BaseRecord {

    private String address = "Test house, Test street, Test City" ;
    private Date birthDate = null;
    private String cellPhone = "07667777123";
    private String city = "London";
    private String countryName = "United Kingdom";
    private String derUpdateTime;
    private String email = "test@test.com";
    private String firstName = "Tester Firstname";
    private String gender = "M";
    private String lastName = "Tester Lastname";
    private String phone = "02038396474";
    private String state = "no state";
    private String title = "mr";
    private String userName;
    private String zip = "AB1 2CD";
}

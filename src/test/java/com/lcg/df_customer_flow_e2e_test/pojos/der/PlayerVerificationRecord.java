package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerVerificationRecord extends BaseRecord {

    private String ageVerification = "Passed";
    private String derUpdateTime;
}

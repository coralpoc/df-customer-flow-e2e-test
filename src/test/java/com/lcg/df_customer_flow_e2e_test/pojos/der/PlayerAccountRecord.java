package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Setter
@Getter
public class PlayerAccountRecord extends BaseRecord {

    private String accountBusinessPhase = "online";
    private String casinoName;
    private String currencyCode = "GBP";
    private String derUpdateTime;
    private Integer frozen = 0;
    private Timestamp lastLoginDate = new Timestamp(0);
    private String profileId = "Test Id";
    private String tokenCode = "500000006";
    private String verificationAnswer = "I am a Tester";
    private String verificationQuestion = "Who are you ?";
    private Integer vipLevel = 7;
}
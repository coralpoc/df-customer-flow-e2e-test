package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class PlayerSignUpRecord extends BaseRecord {

    private Timestamp signUpDate = new Timestamp(0);
    private String derUpdateTime;

}

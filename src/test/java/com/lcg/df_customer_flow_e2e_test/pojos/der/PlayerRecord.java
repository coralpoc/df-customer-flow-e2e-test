package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.text.RandomStringGenerator;

import com.lcg.df_customer_flow_e2e_test.config.ConfigDetector;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.apache.commons.text.CharacterPredicates.LETTERS;

/**
 * Representation of a complete player entry in the database.
 * Represents relevant fields in all relevant tables.
 */
@Getter
@Slf4j
public class PlayerRecord {

    private int playerCode = RandomUtils.nextInt(10000000, 99999999);
    private String casinoName;
    private String targetBrand;

    private PlayerAccountRecord playerAccountRecord = new PlayerAccountRecord();
    private PlayerPersonalRecord playerPersonalRecord = new PlayerPersonalRecord();
    private PlayerVerificationRecord playerVerificationRecord = new PlayerVerificationRecord();
    private PlayerNetworkRecord playerNetworkRecord = new PlayerNetworkRecord();
    private PlayerSignUpRecord playerSignUpRecord = new PlayerSignUpRecord();
    private PlayerSelfExclusionsRecord playerSelfExclusionsRecord = new PlayerSelfExclusionsRecord();
    private PlayerCommunicationOptOutsRecord playerCommunicationOptOutsRecord = new PlayerCommunicationOptOutsRecord();

    public PlayerRecord() {
        setPlayerCodeForAllPlayerRecords(playerCode);
        setCasinoName();
        setTargetBrand();
        setDerUpdateTimeForAllPlayerRecords(getCurrentUtcTimestamp());
        playerPersonalRecord.setUserName(getRandomUsername());
    }

    public void setPlayerData(Object value, String column, String table) {
        Object data = value;
        if (value.toString().equals("null")) {
            data = null;
        }
        switch (table.toLowerCase()) {
            case "player_account":
                if (column.equalsIgnoreCase("playerCode")) {
                    playerCode = Integer.valueOf((String) data);
                    setPlayerCodeForAllPlayerRecords(playerCode);
                } else {
                    playerAccountRecord.setValue(column, data);
                }
                break;
            case "player_personal":
                playerPersonalRecord.setValue(column, data);
                break;
            case "player_verification":
                playerVerificationRecord.setValue(column, data);
                break;
            case "player_network":
                playerNetworkRecord.setValue(column, data);
                break;
            case "player_signup":
                playerSignUpRecord.setValue(column, data);
                break;
            case "playerselfexclusions":
                playerSelfExclusionsRecord.setValue(column, data);
                break;
            case "playercommunicationoptouts":
                playerCommunicationOptOutsRecord.setValue(column, data);
                break;
            default:
                String errorMessage = String.format("Unknown table name: %s", table);
                throw new RuntimeException(errorMessage);
        }
    }

    public void setPlayerCodeForAllPlayerRecords(int player) {
        playerAccountRecord.setCode(player);
        playerPersonalRecord.setCode(player);
        playerVerificationRecord.setCode(player);
        playerNetworkRecord.setCode(player);
        playerSignUpRecord.setCode(player);
        playerSelfExclusionsRecord.setCode(player);

        // please note column name is different here: playercode, not code as for other tables
        playerCommunicationOptOutsRecord.setPlayerCode(player);
    }

    private void setCasinoName() {
        String dataBase = ConfigDetector.getDerDataBase();
        String env = ConfigDetector.getEnv();
        if (env.matches("inmemory|dev|sit")) {
            casinoName = dataBase;
        } else {
            String message = String.format("Not allowed to run DER - ETL tests on %s envt, so we can't set casino name on this envt.", env);
            throw new RuntimeException(message);
        }
        playerAccountRecord.setCasinoName(casinoName);
        playerCommunicationOptOutsRecord.setCasinoName(casinoName);
    }

    public void setDerUpdateTimeForAllPlayerRecords(String derUpdateTime) {
        playerAccountRecord.setDerUpdateTime(derUpdateTime);
        playerPersonalRecord.setDerUpdateTime(derUpdateTime);
        playerVerificationRecord.setDerUpdateTime(derUpdateTime);
        playerNetworkRecord.setDerUpdateTime(derUpdateTime);
        playerSignUpRecord.setDerUpdateTime(derUpdateTime);
        playerSelfExclusionsRecord.setDerUpdateTime(derUpdateTime);
        playerCommunicationOptOutsRecord.setDerUpdateTime(derUpdateTime);
    }

    private String getCurrentUtcTimestamp() {
        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTimeFormatter.format(utc);
    }

    private void setTargetBrand() {
        switch (getCasinoName()) {
            case "coral":
                targetBrand = "Coral";
                break;
            case "galabingo":
                targetBrand = "Gala Bingo";
                break;
            case "galacasino":
                targetBrand = "Gala Casino";
                break;
            case "ladbrokesvegas":
                targetBrand = "Ladbrokes";
                break;
            default:
                String errorMessage = String.format("Unknown casino name: %s", getCasinoName());
                throw new RuntimeException(errorMessage);
        }
    }

    private String getRandomUsername() {
        RandomStringGenerator randomUsernameGenerator = new RandomStringGenerator.Builder()
                .withinRange('a', 'z')
                .filteredBy(LETTERS)
                .build();
        return randomUsernameGenerator.generate(10);
    }
}

package com.lcg.df_customer_flow_e2e_test.pojos.der;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.InvocationTargetException;

@Getter
@Setter
public class BaseRecord {

    private int code;

    /**
     * If anyone knows a better way to do that (i.e. without reflection and lombok), I'm happy to discuss the alternatives.
     */
    public void setValue(String column, Object value) {
        if (value == null) {
            ReflectionTestUtils.setField(this, column, value);
        } else {
            try {
                BeanUtils.setProperty(this, column, value);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException("Something went wrong while setting object properties", e);
            }
        }
    }
}

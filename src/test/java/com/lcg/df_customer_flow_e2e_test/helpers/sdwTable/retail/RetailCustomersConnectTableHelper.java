package com.lcg.df_customer_flow_e2e_test.helpers.sdwTable.retail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.RetailCustomersConnectRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RetailCustomersConnectTableHelper extends RetailBaseTableHelper {

    public void insertPlayerRecord(RetailCustomersConnectRecord retailCustomersConnectRecord) {
        String stm = "INSERT INTO ST.DIM_RTL_CUSTOMERS_CONNECT (PlayerId, MobileVerified,  EmailVerified, LastShopPlayedin,  LastShopPlayedInName, LastRouletteFOBTBetDate, LastSlotsFOBTBetDate, LatstFOBTOtherBetDate, " +
                "LastFootballOTCBetDate, HasBetFOBTRetail, HasBetSSBTRetail, HasBetIpRetail, RetailFOBTMasterSegmentFlag, PreferredStoreName, FOBTTotalStakeAmt, FOBTPlayerDays, FOBTTotalStakeOver50Amt, FOBTLast7daysStakeAmt, " +
                "FOBTLast7daysPlayerDays, FOBTLast28DaysStakeAmt, FOBTLast28daysPlayerDays, RetailLifeCycleSegmentFlag, FOBTTotalBonusAmtRedeemed, LastOTCOtherBetDate, OTCTotalStakeAmt, OTCPlayerDays, OTCLast7daysStakeAmt, " +
                "OTCLast7daysPlayerDays, OTCLast28DaysStakeAmt, OTCLast28daysPlayerDays, OTCTotalBonusAmtRedeemed, BIPTotalStakeAmt, BIPPlayerDays, FirstRetailSSBTBetDate, LatestRetailSSBTBetDate, SSBTTotalStakeAmt, " +
                "SSBTPlayerDays, LatestRetailVIPBetDate, RetailVIPTotalStakes, RetailVIPTotalPlayerDays, RetailVIPTotalStakesOver50, RetailVIPLast28DayStakes, RetailVIPLast28DayPlayerDays, RetailIVIPLast28DayStakesOvr50, " +
                "BonusAbuserFOBT, BonusAbuserOTC, RetailFallowCell, RetailOTCCRMStatus, CurrentRetailVIP, ACCT_VR_FLAG, RetailCustomerType, RetailFOBTCrmStatus, HasBetOTCRetail, PROCESSING_DATE_ID)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        loadOracleDbDriver();
        try (
                Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PWD);
                PreparedStatement preparedStatement = connection.prepareStatement(stm)
        ) {
            connection.setAutoCommit(false);

            preparedStatement.setString(1, String.valueOf(retailCustomersConnectRecord.getCUSTOMER_ID()));
            preparedStatement.setString(2, retailCustomersConnectRecord.getMobileVerified());
            preparedStatement.setString(3, retailCustomersConnectRecord.getEmailVerified());
            preparedStatement.setString(4, retailCustomersConnectRecord.getLastShopPlayedin());
            preparedStatement.setString(5, retailCustomersConnectRecord.getLastShopPlayedInName());
            preparedStatement.setString(6, retailCustomersConnectRecord.getLastRouletteFOBTBetDate());
            preparedStatement.setString(7, retailCustomersConnectRecord.getLastSlotsFOBTBetDate());
            preparedStatement.setString(8, retailCustomersConnectRecord.getLatstFOBTOtherBetDate());
            preparedStatement.setString(9, retailCustomersConnectRecord.getLastFootballOTCBetDate());
            preparedStatement.setString(10, retailCustomersConnectRecord.getHasBetFOBTRetail());
            preparedStatement.setString(11, retailCustomersConnectRecord.getHasBetSSBTRetail());
            preparedStatement.setString(12, retailCustomersConnectRecord.getHasBetIpRetail());
            preparedStatement.setString(13, retailCustomersConnectRecord.getRetailFOBTMasterSegmentFlag());
            preparedStatement.setString(14, retailCustomersConnectRecord.getPreferredStoreName());
            preparedStatement.setBigDecimal(15, retailCustomersConnectRecord.getFobtTotalStakeAmt());
            preparedStatement.setObject(16, retailCustomersConnectRecord.getFobtPlayerDays());
            preparedStatement.setBigDecimal(17, retailCustomersConnectRecord.getFobtTotalStakeOver50Amt());
            preparedStatement.setBigDecimal(18, retailCustomersConnectRecord.getFobtLast7daysStakeAmt());
            preparedStatement.setObject(19, retailCustomersConnectRecord.getFobtLast7daysPlayerDays());
            preparedStatement.setBigDecimal(20, retailCustomersConnectRecord.getFobtLast28DaysStakeAmt());
            preparedStatement.setObject(21, retailCustomersConnectRecord.getFobtLast28daysPlayerDays());
            preparedStatement.setString(22, retailCustomersConnectRecord.getRetailLifeCycleSegmentFlag());
            preparedStatement.setBigDecimal(23, retailCustomersConnectRecord.getFobtTotalBonusAmtRedeemed());
            preparedStatement.setString(24, retailCustomersConnectRecord.getLastOTCOtherBetDate());
            preparedStatement.setBigDecimal(25, retailCustomersConnectRecord.getOtcTotalStakeAmt());
            preparedStatement.setObject(26, retailCustomersConnectRecord.getOtcPlayerDays());
            preparedStatement.setBigDecimal(27, retailCustomersConnectRecord.getOtcLast7daysStakeAmt());
            preparedStatement.setObject(28, retailCustomersConnectRecord.getOtcLast7daysPlayerDays());
            preparedStatement.setBigDecimal(29, retailCustomersConnectRecord.getOtcLast28DaysStakeAmt());
            preparedStatement.setObject(30, retailCustomersConnectRecord.getOtcLast28daysPlayerDays());
            preparedStatement.setBigDecimal(31, retailCustomersConnectRecord.getOtcTotalBonusAmtRedeemed());
            preparedStatement.setBigDecimal(32, retailCustomersConnectRecord.getBipTotalStakeAmt());
            preparedStatement.setObject(33, retailCustomersConnectRecord.getBipPlayerDays());
            preparedStatement.setString(34, retailCustomersConnectRecord.getFirstRetailSSBTBetDate());
            preparedStatement.setString(35, retailCustomersConnectRecord.getLatestRetailSSBTBetDate());
            preparedStatement.setBigDecimal(36, retailCustomersConnectRecord.getSsbtTotalStakeAmt());
            preparedStatement.setObject(37, retailCustomersConnectRecord.getSsbtPlayerDays());
            preparedStatement.setString(38, retailCustomersConnectRecord.getLatestRetailVIPBetDate());
            preparedStatement.setBigDecimal(39, retailCustomersConnectRecord.getRetailVIPTotalStakes());
            preparedStatement.setObject(40, retailCustomersConnectRecord.getRetailVIPTotalPlayerDays());
            preparedStatement.setBigDecimal(41, retailCustomersConnectRecord.getRetailVIPTotalStakesOver50());
            preparedStatement.setBigDecimal(42, retailCustomersConnectRecord.getRetailVIPLast28DayStakes());
            preparedStatement.setObject(43, retailCustomersConnectRecord.getRetailVIPLast28DayPlayerDays());
            preparedStatement.setBigDecimal(44, retailCustomersConnectRecord.getRetailIVIPLast28DayStakesOvr50());
            preparedStatement.setString(45, retailCustomersConnectRecord.getBonusAbuserFOBT());
            preparedStatement.setString(46, retailCustomersConnectRecord.getBonusAbuserOTC());
            preparedStatement.setString(47, retailCustomersConnectRecord.getRetailFallowCell());
            preparedStatement.setString(48, retailCustomersConnectRecord.getRetailOTCCRMStatus());
            preparedStatement.setString(49, retailCustomersConnectRecord.getCurrentRetailVIP());
            preparedStatement.setString(50, retailCustomersConnectRecord.getAcctVRFlag());
            preparedStatement.setString(51, retailCustomersConnectRecord.getRetailCustomerType());
            preparedStatement.setString(52, retailCustomersConnectRecord.getRetailFOBTCrmStatus());
            preparedStatement.setString(53, retailCustomersConnectRecord.getHasBetOTCRetail());
            preparedStatement.setObject(54, retailCustomersConnectRecord.getProcessingDateId());

            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting retail player record into the database" + e.getMessage());
        }
    }
}

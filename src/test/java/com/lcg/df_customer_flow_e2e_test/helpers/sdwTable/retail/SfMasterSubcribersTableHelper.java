package com.lcg.df_customer_flow_e2e_test.helpers.sdwTable.retail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.helpers.BrandMap;
import com.lcg.df_customer_flow_e2e_test.pojos.dpl.SfMasterSubcribersRecord;

public class SfMasterSubcribersTableHelper extends RetailBaseTableHelper {

    public void insertPlayerRecord(SfMasterSubcribersRecord sfMasterSubcribersRecord) {
        String stm = "INSERT INTO DW.SF_MASTER_SUBCRIBERS (PlayerId, FirstRetailBIPBetDate, FirstRetailFOBTBetDate, FirstRetailOTCBetDate, LastRetailBIPBetDate, LastRetailFOBTBetDate, LastRetailOTCBetDate," +
                "CUSTOMER_SEQ, EMAILADDRESS, USERNAME, FIRSTNAME, LASTNAME, ADDRESS1, CITY, POSTCODE, BRAND, COUNTRY, GENDER, DOB, BETTINGCURRENCY, REGDATETIME, DAYSSINCEREG, ACCOUNTTYPE, MACROSEGMNT, " +
                "VIPLEVEL, LASTLOGINDATE, DAYSSINCELASTLOGIN, CURRENTBALANCE, BONUSBALANCE, CASHBALANCE, ACCOUNTSTATUS, OPTINEMAIL, OPTINSMS, OPTINPUSH, OPTINDIRECTMAIL, OPTINOBTM, SELFEXCLUDE, AUTOSUPPRESS, LOAD_DATE_ID)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        loadOracleDbDriver();
        try (
                Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PWD);
                PreparedStatement preparedStatement = connection.prepareStatement(stm)
        ) {
            connection.setAutoCommit(false);
            preparedStatement.setString(1, String.valueOf(sfMasterSubcribersRecord.getCUSTOMER_ID()));
            preparedStatement.setDate(2, sfMasterSubcribersRecord.getFirstRetailBIPBetDate());
            preparedStatement.setDate(3, sfMasterSubcribersRecord.getFirstRetailFOBTBetDate());
            preparedStatement.setDate(4, sfMasterSubcribersRecord.getFirstRetailOTCBetDate());
            preparedStatement.setDate(5, sfMasterSubcribersRecord.getLastRetailBIPBetDate());
            preparedStatement.setDate(6, sfMasterSubcribersRecord.getLastRetailFOBTBetDate());
            preparedStatement.setDate(7, sfMasterSubcribersRecord.getLastRetailOTCBetDate());

            // Not nullable fields and not in scope for testing from SDW MSF table as these fields are extracting from DPL
            preparedStatement.setInt(8, sfMasterSubcribersRecord.getCUSTOMER_SEQ());
            preparedStatement.setString(9, sfMasterSubcribersRecord.getEmailAddress());
            preparedStatement.setString(10, sfMasterSubcribersRecord.getUsername());
            preparedStatement.setString(11, sfMasterSubcribersRecord.getFirstName());
            preparedStatement.setString(12, sfMasterSubcribersRecord.getLastName());
            preparedStatement.setString(13, sfMasterSubcribersRecord.getAddress1());
            preparedStatement.setString(14, sfMasterSubcribersRecord.getCity());
            preparedStatement.setString(15, sfMasterSubcribersRecord.getPostcode());
            preparedStatement.setString(16, BrandMap.getBrand(sfMasterSubcribersRecord.getBRAND_SEQ()));
            preparedStatement.setString(17, sfMasterSubcribersRecord.getCountry());
            preparedStatement.setString(18, sfMasterSubcribersRecord.getGender());
            preparedStatement.setDate(19, sfMasterSubcribersRecord.getDob());
            preparedStatement.setString(20, sfMasterSubcribersRecord.getBettingCurrency());
            preparedStatement.setDate(21, sfMasterSubcribersRecord.getRegDateTime());
            preparedStatement.setInt(22, sfMasterSubcribersRecord.getDaysSinceReg());
            preparedStatement.setString(23, sfMasterSubcribersRecord.getAccountType());
            preparedStatement.setString(24, sfMasterSubcribersRecord.getMacroSegmnt());
            preparedStatement.setInt(25, sfMasterSubcribersRecord.getVipLevel());
            preparedStatement.setDate(26, sfMasterSubcribersRecord.getLastLoginDate());
            preparedStatement.setInt(27, sfMasterSubcribersRecord.getDaysSinceLastLogin());
            preparedStatement.setDouble(28, sfMasterSubcribersRecord.getCurrentBalance());
            preparedStatement.setDouble(29, sfMasterSubcribersRecord.getBonusBalance());
            preparedStatement.setDouble(30, sfMasterSubcribersRecord.getCashBalance());
            preparedStatement.setString(31, sfMasterSubcribersRecord.getAccountStatus());
            preparedStatement.setString(32, sfMasterSubcribersRecord.getOptInEmail());
            preparedStatement.setString(33, sfMasterSubcribersRecord.getOptInSMS());
            preparedStatement.setString(34, sfMasterSubcribersRecord.getOptInPush());
            preparedStatement.setString(35, sfMasterSubcribersRecord.getOptInDirectMail());
            preparedStatement.setString(36, sfMasterSubcribersRecord.getOptInOBTM());
            preparedStatement.setString(37, sfMasterSubcribersRecord.getSelfExclude());
            preparedStatement.setString(38, sfMasterSubcribersRecord.getAutosuppress());
            preparedStatement.setInt(39, sfMasterSubcribersRecord.getLoadDateId());

            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting retail player record into the database" + e.getMessage());
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.sdwTable.retail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.config.EnvConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RetailBaseTableHelper {

    protected static final String CONNECTION_URL = EnvConfig.getProperty("sdw.retail.db.url");
    protected static final String USER = EnvConfig.getProperty("sdw.retail.db.user");
    protected static final String PWD = EnvConfig.getProperty("sdw.retail.db.password");

    private final List<String> retailTables;

    public RetailBaseTableHelper() {
        retailTables = new ArrayList<>();
        retailTables.add("ST.DIM_RTL_CUSTOMERS_CONNECT");
        retailTables.add("DW.SF_MASTER_SUBCRIBERS");
    }

    public void loadOracleDbDriver() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Error: " + e.getMessage(), e);
        }
    }

    public void deletePlayerRecord(int playerId) {
        loadOracleDbDriver();
        try (
                Connection connection = DriverManager.getConnection(CONNECTION_URL, USER, PWD);
        ) {
            for (String table : retailTables) {
                checkIfWeCanDeleteRecordsFromThisTable(table);
                String stm = String.format("DELETE FROM %s WHERE PlayerId = ?", table);
                PreparedStatement preparedStatement = connection.prepareStatement(stm);
                connection.setAutoCommit(false);
                preparedStatement.setString(1, String.valueOf(playerId));
                preparedStatement.executeUpdate();
                connection.commit();
                preparedStatement.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting retail player record from the database" + e.getMessage());
        }
        log.info("Player deleted from SDW Retail DB");
    }

    private void checkIfWeCanDeleteRecordsFromThisTable(String table) {
        if (!retailTables.contains(table.toUpperCase())) {
            String message = String.format("Tables from where we can delete records are %s.\n", retailTables.toString());
            throw new RuntimeException(message);
        }
    }

}

package com.lcg.df_customer_flow_e2e_test.helpers.aws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClientBuilder;
import com.amazonaws.services.elasticmapreduce.model.ClusterState;
import com.amazonaws.services.elasticmapreduce.model.ClusterSummary;
import com.amazonaws.services.elasticmapreduce.model.ClusterTimeline;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterResult;
import com.amazonaws.services.elasticmapreduce.model.InternalServerErrorException;
import com.amazonaws.services.elasticmapreduce.model.ListClustersRequest;
import com.amazonaws.services.elasticmapreduce.model.ListClustersResult;
import com.amazonaws.services.elasticmapreduce.model.ListStepsRequest;
import com.amazonaws.services.elasticmapreduce.model.ListStepsResult;
import com.amazonaws.services.elasticmapreduce.model.StepState;
import com.amazonaws.services.elasticmapreduce.model.StepStatus;
import com.amazonaws.services.elasticmapreduce.model.StepSummary;
import com.amazonaws.services.elasticmapreduce.model.StepTimeline;
import com.amazonaws.services.elasticmapreduce.model.TerminateJobFlowsRequest;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.Environment;
import com.amazonaws.services.lambda.model.EnvironmentResponse;
import com.amazonaws.services.lambda.model.GetFunctionConfigurationRequest;
import com.amazonaws.services.lambda.model.GetFunctionConfigurationResult;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.UpdateFunctionConfigurationRequest;
import com.amazonaws.services.lambda.model.UpdateFunctionConfigurationResult;
import com.lcg.df_customer_flow_e2e_test.helpers.dbHelper.EmrCluster;
import com.lcg.df_customer_flow_e2e_test.helpers.dbHelper.EmrClusterRepository;

import junit.framework.Assert;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AWSServiceHelper {
	
	private final  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
	private AmazonElasticMapReduce emr = null;
	
	public void updateLambdaDeadLine(int hour) {
		
		AWSLambda awsLambda = AWSLambdaClientBuilder.defaultClient();

		GetFunctionConfigurationResult response = awsLambda.getFunctionConfiguration(new GetFunctionConfigurationRequest().withFunctionName("df-customer-dpl-extractor"));

		EnvironmentResponse envResponse = response.getEnvironment(); 
		
		java.util.Map<String, String> updateVariable = envResponse.getVariables();
		String timeStr = getTimeStringInHrMm(hour);
		updateVariable.put("DPL_DEADLINE", getTimeStringInHrMm(hour));
		
		
		 UpdateFunctionConfigurationRequest expected = new UpdateFunctionConfigurationRequest()
	                .withFunctionName(response.getFunctionName())
	                .withEnvironment(new Environment().withVariables(updateVariable));
		
		 awsLambda.updateFunctionConfiguration(expected);
		
		log.info("------ DPL_DEADLINE set to {}",timeStr);
		awsLambda.shutdown();
	}
	
	public void triggerLambda(String type) {
		
		String payload = "";
		String functionName = "";
		
		if("diff".equals(type)) {
			payload = "{}";
			functionName = "df-customer-dpl-extractor";
		}else if("migration".equals(type)){
			payload = "{\"extractionType\": \"MIGRATION\"}";
			functionName = "df-customer-dpl-extractor";
		}
		triggerLambda(functionName,payload,type);
	}
	
	private void triggerLambda(String functionName,String payload,String type) {
		AWSLambda awsLambda = AWSLambdaClientBuilder.defaultClient();
		
		InvokeRequest invokeRequest = new InvokeRequest()
                .withFunctionName(functionName)
                .withPayload(payload)
                .withInvocationType(InvocationType.Event);
		
		InvokeResult result = awsLambda.invoke(invokeRequest);
		assertEquals("Status code for successful Lamdba triggrt event type is 202",result.getStatusCode(), Integer.valueOf(202));
		
		log.info("Successfully triggered event for {} , functionName: {} , payload: {}",type,functionName,payload);
		awsLambda.shutdown();
	}
	
	private String getTimeStringInHrMm(int hour) {
		LocalTime lTime = LocalTime.now();
		lTime = lTime.plusHours(hour);
		
		return lTime.format(formatter);
	}
	
	public boolean readyToQueryCluster(ClusterSummary clusterSummary) {
		
		DescribeClusterResult result = null;
		DescribeClusterRequest describeClusterRequest = new DescribeClusterRequest().withClusterId(clusterSummary.getId());
		
		int retryTime = 0;
		int maxRetry = 5;
		
		
		while(retryTime < maxRetry) {
			
			result = emr.describeCluster(describeClusterRequest);
			if (result != null) {
				String state = result.getCluster().getStatus().getState();
				if (state.equalsIgnoreCase("RUNNING") ||state.equalsIgnoreCase("WAITING"))	{
					log.info("The cluster with id " + clusterSummary.getId() + " exists and is " + state);   
					return true;
				}
			}
			
			retryTime++;
			waitForBeforeResumeTask(2 ,"wait For cluster state - RUNNING or WAITING");
		}
		
		log.info("The cluster with id " + clusterSummary.getId() + ", steps not yet ready for query. ");
		return false;
		
	}
	
	public void waitForEMRClusterFinish(String runType,String stepName, Date beforeTime, int initWaitMins) {
		
		//EmrClusterRepository emrRepository = new EmrClusterRepository();
		boolean retry = true;
		int retryTime = 0;
		int maxRetry = 5;
		
		boolean foundCluster = false;
		ClusterSummary activeCluster = null;
//		AmazonElasticMapReduce emr = AmazonElasticMapReduceClientBuilder.defaultClient();
		
		while(retryTime < maxRetry) {
			
			try {
				if(!foundCluster) {
					
					activeCluster = getActiveCluster(emr,runType);
					
					if(activeCluster != null) {
						foundCluster = true;
						
						log.info("Found cluster with id - {}, name - {}, status - {}",activeCluster.getId(),
								activeCluster.getName(),activeCluster.getStatus().getState());
						
						boolean flag = false;
						if(readyToQueryCluster(activeCluster)) {
							flag = waitTillStepCompletion(emr,activeCluster,stepName,runType, beforeTime);
						}

						if(!flag) {
							String msg = "------ EMR operation not completed successfully for cluster id - {" + activeCluster.getId() + "}";
							fail(msg);
						}
						return;
						
					}else {
						continue;
					}
				}
			}catch(Exception e) {
				log.error("Exception caught while querring EMR, will wait and try again",e);
			}
				
			
			
			retryTime++;
			waitForBeforeResumeTask(initWaitMins * retryTime,"waitForEMRClusterFinish - "+ stepName);
		
		}
		

		String msg = "------ EMR operation did not find running cluster";
		fail(msg);

	}
	
	private boolean waitTillStepCompletion(AmazonElasticMapReduce emr,ClusterSummary activeCluster,String stepName,String runType, Date beforeTime) {
		
		log.info("Check step completion status in clusterId: {} , for - {} ",activeCluster.getId(),stepName);
		

		int retry = 0;
		int maxRetry = 5;

		while(retry < maxRetry) {
			retry++;
			ListStepsRequest listReq = buildListStepRequest(activeCluster);
			ListStepsResult stepResult = emr.listSteps(listReq);
			
			List<StepSummary> stepSums =  stepResult.getSteps().stream().filter(ss -> ss.getName().equals(stepName) && 
					activeSteps(ss.getStatus(),beforeTime))
				.collect(Collectors.toList());
			
			if(!stepSums.isEmpty()) {
				
				if(isStepsCompleted(stepSums,runType, stepName)) {
					return true;
				}else if(!hasRunningSteps(stepSums,runType, stepName)) {
					
					log.info("{} may have been terminated in EMR for {} ",stepName,runType);
					return false;
				}

			}
			waitForBeforeResumeTask(1 ,"waitTillStepCompletion - " + stepName);
		}
		
		return false;
		
	}
	
	private boolean activeSteps(StepStatus stepStatus,Date beforeTime) {
		boolean valid = false;
		StepTimeline timeline = stepStatus.getTimeline();
		if(timeline != null) {
			valid = (timeline.getStartDateTime() == null) || (timeline.getStartDateTime().after(beforeTime));
		}
		
		return valid;
	}
	
	private ClusterSummary getActiveCluster(AmazonElasticMapReduce emr,String runType) {
		
		ListClustersRequest clusterRequest = new ListClustersRequest();
		
		clusterRequest.withClusterStates(ClusterState.WAITING,ClusterState.BOOTSTRAPPING,ClusterState.STARTING,ClusterState.RUNNING);
		
		ListClustersResult result = emr.listClusters(clusterRequest);
		
		List<ClusterSummary> clusterList = result.getClusters();
		
		return getActiveCluster(clusterList,runType);
	}
	
	public void simulateTriggerlambda(String runType) {
		SimulateLambdaTrigger simulateTrigger;
		if("diff".equals(runType)) {
			simulateTrigger = new DiffSimulate();
			simulateTrigger.simulateTrigger(runType);
		}else {
			simulateTrigger = new MigrateSimulate();
			simulateTrigger.simulateTrigger(runType);
		}

	}
	
	public void shutDownIdealClusters(String runType) {
		AmazonElasticMapReduce emr = AmazonElasticMapReduceClientBuilder.defaultClient();
		ListClustersRequest clusterRequest = new ListClustersRequest();
		
		clusterRequest.withClusterStates(ClusterState.WAITING);
		
		ListClustersResult result = emr.listClusters(clusterRequest);
		
		List<ClusterSummary> clusterList = result.getClusters();
		ClusterSummary clusterSummary = getActiveCluster(clusterList,runType);
		
		TerminateJobFlowsRequest terminateReq = new TerminateJobFlowsRequest(Arrays.asList(clusterSummary.getId()));
		
		try {
		emr.terminateJobFlows(terminateReq);
		
			log.info("------ Requested to Terminate cluster-id: {}",clusterSummary.getId());
		}catch(InternalServerErrorException ie) {
			log.info("------ Failed to terminate cluster-id: {} because of exception - {}",clusterSummary.getId(),ie.getErrorMessage());
		}
		
		emr.shutdown();
	}
	
	private boolean hasRunningSteps(List<StepSummary> stepSummary,String runType,String stepName) {
		
		int b = (int)stepSummary.stream().filter(ss -> ss.getStatus().getState().equals(StepState.RUNNING.toString()) 
				|| ss.getStatus().getState().equals(StepState.PENDING.toString())).count();
		if (b > 0) {
			log.info("{} still running in EMR for {} ",stepName,runType);
			return true;
		}
		
		return false;
	}
	
	private boolean isStepsCompleted(List<StepSummary> stepSummary,String runType,String stepName) {
		
		int b = (int)stepSummary.stream().filter(ss -> ss.getStatus().getState().equals(StepState.COMPLETED.toString())).count();
		if(stepSummary.size() == b) {

			log.info("{} completed in EMR for {} ",stepName,runType);
			return true;
		}
		
		return false;
	}
	
	private ListStepsRequest buildListStepRequest(ClusterSummary activeCluster) {
		
		ListStepsRequest requestSteps = new ListStepsRequest();
		requestSteps.withClusterId(activeCluster.getId());
		
		return requestSteps;
		
	}
	
	private  ClusterSummary getActiveCluster(List<ClusterSummary> clusterList, String runLabel) {
		
		EMRRunType runType = EMRRunType.findType(runLabel);
		

		List<ClusterSummary> result = clusterList.stream().filter(cs -> cs.getName().equals(runType.desc)).collect(Collectors.toList());
		if(!result.isEmpty()) {
			result.sort(Comparator.comparing(cs -> getClusterCreationTime(cs),Comparator.nullsFirst(Comparator.reverseOrder())));
			
			log.debug("Returning first from list of clusters matching run type - {}", runLabel);
			ClusterSummary cs;
			for(int i =0; i < result.size(); i++) {
				cs = result.get(i);
				log.debug("Found at index {}, Cluster id - {}, Cluster Name - {}",i,cs.getId(),cs.getName());
			}
			
			return result.get(0);
		}

		
		return null;
	}
	
	private Date getClusterCreationTime(ClusterSummary cs) {
		
		Date created = null;
		
		Optional<ClusterTimeline> optTimeline = Optional.of(cs.getStatus().getTimeline());
		if(optTimeline.isPresent()) {
			created = optTimeline.get().getCreationDateTime();
		}
		
		return created;
	}
	
	private void waitForBeforeResumeTask(int mins, String msg) {
		if(msg.isEmpty()) {
			log.info("Waiting for {} mins before resume job...", mins);
		}else {
			log.info("Waiting for {} mins before resume task - {}", mins,msg);
		}
		
        try {
            Thread.sleep(mins * 60000);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
	}
	
	public class DiffSimulate implements SimulateLambdaTrigger {
		
		@Override
		public void simulateTrigger(String runType) {
			String extractName = "Extraction Step";
			String joinName = "Joining Step";
			Date before = null;
			
			emr = AmazonElasticMapReduceClientBuilder.defaultClient();
			LocalTime startTime = LocalTime.now();
			
			triggerLambda(runType);
			log.info("------ Triggered lambda for extraction");
			
			before = new Date();
			
			
			
			waitForEMRClusterFinish(runType,extractName,before,3);
			
			triggerLambda(runType);
			log.info("------ Triggered lambda for joining");
			waitForEMRClusterFinish(runType,joinName,before,3);
			
			LocalTime endTime = LocalTime.now();
			log.info("------ Trigger completed in {} mins", endTime.getMinute() - startTime.getMinute());
			
			emr.shutdown();
		}
		
	}
	
	public class MigrateSimulate implements SimulateLambdaTrigger {
		
		@Override
		public void simulateTrigger(String runType) {
			String extractName = "Extraction Step";
			String joinName = "Joining Step";
			Date before = null;
			
			emr = AmazonElasticMapReduceClientBuilder.defaultClient();
			LocalTime startTime = LocalTime.now();
			
			triggerLambda(runType);
			log.info("------ Triggered lambda for {}",runType);
			
			before = new Date();
			
			waitForEMRClusterFinish(runType,extractName,before,3);
			

			waitForEMRClusterFinish(runType,joinName,before,1);
			
			LocalTime endTime = LocalTime.now();
			log.info("------ Trigger completed in {} mins", endTime.getMinute() - startTime.getMinute());
			
			emr.shutdown();
		}
		
	}

}

package com.lcg.df_customer_flow_e2e_test.helpers.aws;

public enum EMRRunType {
	
	DIFF("diff","DPL Diff Cluster"),MIGRATE("migration","DPL Migration Cluster");
	
	public String label;
	public String desc;
	
	 EMRRunType(String label, String desc) {
		this.label = label;
		this.desc= desc;
	}
	
	public static EMRRunType findType(String label) {
		
		if(DIFF.label.equals(label)) {
			return DIFF;
		}
		
		return MIGRATE;
	}

}

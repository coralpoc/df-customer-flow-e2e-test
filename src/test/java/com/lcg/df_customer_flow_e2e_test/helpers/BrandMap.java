package com.lcg.df_customer_flow_e2e_test.helpers;

import java.util.HashMap;
import java.util.Map;

public class BrandMap {

    private static final Map<Integer, String> TARGET_BRAND_MAP;
    private static final Map<Integer, String> BRAND_MAP;

    static {
        TARGET_BRAND_MAP = new HashMap<>();
        TARGET_BRAND_MAP.put(2, "Coral");
        TARGET_BRAND_MAP.put(3, "Coral");
        TARGET_BRAND_MAP.put(4, "Coral");
        TARGET_BRAND_MAP.put(12, "Ladbrokes");
        TARGET_BRAND_MAP.put(14, "Ladbrokes");

        BRAND_MAP = new HashMap<>();
        BRAND_MAP.put(2, "Gala Casino");
        BRAND_MAP.put(3, "Gala Bingo");
        BRAND_MAP.put(4, "Coral");
        BRAND_MAP.put(12, "Ladbrokes");
        BRAND_MAP.put(14, "Ladbrokes");
    }

    public static String getTargetBrand(int brandSeq) {
        return TARGET_BRAND_MAP.get(brandSeq);
    }

    public static String getBrand(int brandSeq) {
        return BRAND_MAP.get(brandSeq);
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public abstract class AbstractDbHelper {

    protected static Connection connection = null;

    protected void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                log.debug("Closing db connecton");
            } catch (SQLException e) {
                throw new RuntimeException("Error closing db connecton: " + e.getMessage());
            }
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor
public class EmrCluster {

    private String clusterId;
    private Instant startTime;

}

package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import java.time.Instant;
import java.util.Iterator;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.lcg.df_customer_flow_e2e_test.config.ConfigDetector;
import com.lcg.df_customer_flow_e2e_test.config.EnvConfig;

public class DdbDERExtractorState {

    private static final String DDB_SERVICE_ENDPOINT = EnvConfig.getProperty("ddb.url");
    private static final String EXTRACTOR_DER_DIFFSTATE_TABLE_NAME = EnvConfig.getProperty("ddb.extractorderdiffstate.table");
    private static final String SIGNING_REGION = "eu-west-1";

    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
            new AwsClientBuilder.EndpointConfiguration(DDB_SERVICE_ENDPOINT, SIGNING_REGION))
            .build();
    private static DynamoDB dynamoDB = new DynamoDB(client);
    private static Table table = dynamoDB.getTable(EXTRACTOR_DER_DIFFSTATE_TABLE_NAME);

    public static Instant findLastDerExtractionTime() {
        String dataBase = ConfigDetector.getDerDataBase();
        Instant instant;
        QuerySpec spec = new QuerySpec();
        spec.withMaxResultSize(1).withScanIndexForward(false).withHashKey("extractorName", dataBase);

        ItemCollection<QueryOutcome> items = table.query(spec);
        Iterator<Item> iterator = items.iterator();
        Item item = null;
        while (iterator.hasNext()) {
            item = iterator.next();
        }
        if (item != null) {
            instant = Instant.ofEpochMilli(item.getLong("lastUpdateTime"));
        } else {
            String message = String.format("Last der extraction time not found for %s", dataBase);
            throw new RuntimeException(message);
        }
        return instant;
    }
}


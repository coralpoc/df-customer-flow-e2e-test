package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "dpl-extraction-status")
public class DplExtractionStatusTable {
    private String sourceAvailDate;
    private String tableName;

    @DynamoDBHashKey
    public String getSourceAvailDate() {
        return sourceAvailDate;
    }

    public void setSourceAvailDate(String sourceAvailDate) {
        this.sourceAvailDate = sourceAvailDate;
    }

    @DynamoDBRangeKey
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}

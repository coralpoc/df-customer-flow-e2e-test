package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class DplExtractionStatusRepository {

    private static final String DDB_SERVICE_ENDPOINT = "dynamodb.eu-west-1.amazonaws.com";
    private static final String SIGNING_REGION = "eu-west-1";

    private static final String EXTRACTION_STATUS_TABLE_NAME = "dpl-extraction-status";
    private static final String SOURCE_AVAIL_DATE_COL = "sourceAvailDate";
    private static final String TABLE_NAME_COL = "tableName";

    private static String sourceAvailableDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());


    private static final AmazonDynamoDB DB_CLIENT = AmazonDynamoDBClientBuilder.defaultClient();
    		/*AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
            new AwsClientBuilder.EndpointConfiguration(DDB_SERVICE_ENDPOINT, SIGNING_REGION))
            .build();*/

    public void deleteAllItems() {
        DynamoDBMapper dbMapper = new DynamoDBMapper(DB_CLIENT);
        DynamoDBTableMapper<DplExtractionStatusTable, String, String> tableMapper = dbMapper.newTableMapper(DplExtractionStatusTable.class);
        PaginatedScanList<DplExtractionStatusTable> allItems = tableMapper.scan(new DynamoDBScanExpression());
        tableMapper.batchDelete(allItems);
    }

    public void deleteItem(List<String> tables) {
        String sourceAvailableDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        DynamoDB dynamoDB = new DynamoDB(DB_CLIENT);
        Table ddbTable = dynamoDB.getTable(EXTRACTION_STATUS_TABLE_NAME);
        tables.forEach(table -> {
            if (table.equals("DIM_RTL_CUSTOMERS_CONNECT")) {
                table = "ST.DIM_RTL_CUSTOMERS_CONNECT";
            } else if (table.equals("SF_MASTER_SUBCRIBERS")) {
                table = "DW.SF_MASTER_SUBCRIBERS";
            }
            DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
                    .withPrimaryKey("sourceAvailDate", sourceAvailableDate, "tableName", table);
            ddbTable.deleteItem(deleteItemSpec);
        });
    }

    public void setJoiningEndTime(String table, Long minutes) {
        DB_CLIENT.updateItem(getUpdateItemRequestToUpdateJoiningEndTime(table, minutes));
        log.info("{} table joining end time set to {} minutes ago", table, minutes);
    }

    public void setExtractionStatus(String table, String status) {
        DB_CLIENT.updateItem(getUpdateItemRequestToUpdateExtractionStatus(table, status));
        log.info("{} table status set to {}", table, status);
    }

    private UpdateItemRequest getUpdateItemRequestToUpdateJoiningEndTime(String table, Long minutes) {
        Instant endTime = Instant.now();
        Map<String, AttributeValue> attributeValues = new HashMap<>();
        AttributeValue joiningEndTime = buildTimeAttribute(endTime, minutes);
        String updateExpression = "SET joiningEndTime = :joiningEndTime";
        attributeValues.put(":joiningEndTime", joiningEndTime);
        log.info("Setting joiningEndTime to {}", joiningEndTime.getS());

        return new UpdateItemRequest()
                .withTableName(EXTRACTION_STATUS_TABLE_NAME)
                .withKey(
                        new AbstractMap.SimpleEntry("sourceAvailDate", new AttributeValue()
                                .withS(sourceAvailableDate)),
                        new AbstractMap.SimpleEntry("tableName", new AttributeValue()
                                .withS(table))
                )
                .withUpdateExpression(updateExpression)
                .withExpressionAttributeValues(attributeValues);
    }

    public UpdateItemRequest getUpdateItemRequestToUpdateExtractionStatus(String table, String status) {
        Instant endTime = Instant.now();
        log.info("Updating view {} to status {}", table, status);
        Map<String, AttributeValue> attributeValues = new HashMap<>();
        attributeValues.put(":extractionStatus", new AttributeValue().withS(status));

        String endTimeColumnName;
        String updateExpression = "";
        if (status.equalsIgnoreCase("EXTRACTED")) {
            endTimeColumnName = "extractionEndTime";
            AttributeValue extractionEndTime = buildTimeAttribute(endTime);
            updateExpression = "SET extractionStatus = :extractionStatus, " + endTimeColumnName + " = :" + endTimeColumnName;
            attributeValues.put(":" + endTimeColumnName, extractionEndTime);
            log.info("Setting extractionEndTime to {}", extractionEndTime.getS());
        } else if (status.equalsIgnoreCase("SENT_TO_KAFKA")) {
            endTimeColumnName = "joiningEndTime";
            AttributeValue joiningEndTime = buildTimeAttribute(endTime);
            updateExpression = "SET extractionStatus = :extractionStatus, " + endTimeColumnName + " = :" + endTimeColumnName;
            attributeValues.put(":" + endTimeColumnName, joiningEndTime);
            log.info("Setting joiningEndTime to {}", joiningEndTime.getS());
        } else {
            updateExpression = "SET extractionStatus = :extractionStatus";
        }

        UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName(EXTRACTION_STATUS_TABLE_NAME)
                .withKey(
                        new AbstractMap.SimpleEntry("sourceAvailDate", new AttributeValue()
                                .withS(sourceAvailableDate)),
                        new AbstractMap.SimpleEntry("tableName", new AttributeValue()
                                .withS(table))
                )
                .withUpdateExpression(updateExpression)
                .withExpressionAttributeValues(attributeValues);
        return updateItemRequest;
    }

    public String getValueOfColumnInStatusTable(String tableName, String columnName) {
        QueryRequest queryRequest = new QueryRequest();
        queryRequest.setTableName(EXTRACTION_STATUS_TABLE_NAME);
        ArrayList attributesToGet = new ArrayList();
        attributesToGet.add(columnName);
        queryRequest.setAttributesToGet(attributesToGet);

        Map<String, Condition> keyConditions = new HashMap<>();
        Condition hashKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(sourceAvailableDate));
        keyConditions.put(SOURCE_AVAIL_DATE_COL, hashKeyCondition);

        Condition rangeKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(tableName));

        keyConditions.put(TABLE_NAME_COL, rangeKeyCondition);
        queryRequest.setKeyConditions(keyConditions);

        QueryResult queryResult = DB_CLIENT.query(queryRequest);

        List<Map<String, AttributeValue>> rows = queryResult.getItems();
        if (rows.size() == 0) {
            String errorMessage = String.format("No entry found in the ddb status table for view %s", tableName);
            log.error(errorMessage);
            throw new RuntimeException(errorMessage);
        } else if (rows.size() > 1) {
            String errorMessage = String.format("More than 1 entry found in the ddb status table for view %s", tableName);
            log.error(errorMessage);
            throw new RuntimeException(errorMessage);
        }
        return rows.get(0).get(columnName).getS();
    }

    public boolean checkColumnExistInStatusTable(String tableName, String columnName) {
    	
    	boolean columnExist = true;
        QueryRequest queryRequest = new QueryRequest();
        queryRequest.setTableName(EXTRACTION_STATUS_TABLE_NAME);
        ArrayList attributesToGet = new ArrayList();
        attributesToGet.add(columnName);
        queryRequest.setAttributesToGet(attributesToGet);

        Map<String, Condition> keyConditions = new HashMap<>();
        Condition hashKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(sourceAvailableDate));
        keyConditions.put(SOURCE_AVAIL_DATE_COL, hashKeyCondition);

        Condition rangeKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(tableName));

        keyConditions.put(TABLE_NAME_COL, rangeKeyCondition);
        queryRequest.setKeyConditions(keyConditions);

        QueryResult queryResult = DB_CLIENT.query(queryRequest);

        List<Map<String, AttributeValue>> rows = queryResult.getItems();
        if (rows.size() == 0) {
        	columnExist = false;
        }
        return columnExist;
    }
    
    private AttributeValue buildTimeAttribute(Instant instant, Long minutes) {
        AttributeValue attributeValue = new AttributeValue();
        if (instant == null) {
            attributeValue.withNULL(true);
        } else {
            attributeValue.withS(instant.atZone(ZoneOffset.UTC).minusMinutes(minutes).format(DateTimeFormatter.ISO_DATE_TIME));
        }
        return attributeValue;
    }

    private AttributeValue buildTimeAttribute(Instant instant) {
        AttributeValue attributeValue = new AttributeValue();
        if (instant == null) {
            attributeValue.withNULL(true);
        } else {
            attributeValue.withS(instant.atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME));
        }
        return attributeValue;
    }

    private static void createExtractionStatusTable() {
        DB_CLIENT.createTable(new CreateTableRequest()
                .withTableName(EXTRACTION_STATUS_TABLE_NAME)
                .withKeySchema(
                        new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName(SOURCE_AVAIL_DATE_COL),
                        new KeySchemaElement().withKeyType(KeyType.RANGE).withAttributeName(TABLE_NAME_COL)
                )
                .withAttributeDefinitions(
                        new AttributeDefinition()
                                .withAttributeName(SOURCE_AVAIL_DATE_COL)
                                .withAttributeType(ScalarAttributeType.S),
                        new AttributeDefinition()
                                .withAttributeName(TABLE_NAME_COL)
                                .withAttributeType(ScalarAttributeType.S)
                )
                .withProvisionedThroughput(new ProvisionedThroughput()
                        .withReadCapacityUnits(2L)
                        .withWriteCapacityUnits(2L)
                )
        );
        log.info("Created DyanamoDB table {}", EXTRACTION_STATUS_TABLE_NAME);
    }

    public void insertViewEntry(String tableName) {
        PutItemRequest putItemRequest = new PutItemRequest();

        putItemRequest.setTableName(EXTRACTION_STATUS_TABLE_NAME);
        putItemRequest.addItemEntry("sourceAvailDate", new AttributeValue(sourceAvailableDate))
                .addItemEntry("tableName", new AttributeValue(tableName));

        log.info("Inserting record into DDB: {}:{}:{}", sourceAvailableDate, tableName);

        PutItemResult result = DB_CLIENT.putItem(putItemRequest);
        int statusCode = result.getSdkHttpMetadata().getHttpStatusCode();
        checkStatusCode(statusCode);
    }

    private static void checkStatusCode(int statusCode) {
        if (statusCode != 200) {
            String errorMessage = String.format("Got unexpected status code %d from DDB API.", statusCode);
            throw new RuntimeException(errorMessage);
        }
    }

}

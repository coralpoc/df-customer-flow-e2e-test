package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PreDestroy;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmrClusterRepository {

    private static final String TABLE_NAME = "dpl-emr-clusters";

    private static final String START_DATE_COL = "startDate";
    private static final String CLUSTER_ID_COL = "clusterId";
    private static final String START_TIME_COL = "startTime";

    private final AmazonDynamoDBAsync dynamoDB;

    public EmrClusterRepository() {
        this.dynamoDB = AmazonDynamoDBAsyncClientBuilder.standard().build();
    }

    @PreDestroy
    public void shutdown() {
        this.dynamoDB.shutdown();
    }


    public List<EmrCluster> findByDates(LocalDate... dates) {
        List<Future<QueryResult>> futures = Arrays.stream(dates)
                .map(this::queryByDate)
                .collect(Collectors.toList());
        return futures.stream()
                .flatMap(future -> {
                    try {
                        List<Map<String, AttributeValue>> items = future.get(60, TimeUnit.SECONDS).getItems();
                        return items != null ? items.stream() : Stream.empty();
                    } catch (RuntimeException e) {
                        throw e;
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                })
                .map(this::toEmrCluster)
                .sorted((EmrCluster e1,EmrCluster e2 ) -> e1.getStartTime().compareTo(e2.getStartTime()))
                .collect(Collectors.toList());
    }

    private Future<QueryResult> queryByDate(LocalDate date) {
        return dynamoDB
                .queryAsync(new QueryRequest()
                        .withTableName(TABLE_NAME)
                        .withKeyConditionExpression(START_DATE_COL + " = :startDate")
                        .addExpressionAttributeValuesEntry(":startDate", new AttributeValue()
                                .withS(date.format(ISO_LOCAL_DATE)))
                );
    }

    private EmrCluster toEmrCluster(Map<String, AttributeValue> record) {
        return new EmrCluster(
                record.get(CLUSTER_ID_COL).getS(),
                toInstant(record.get(START_TIME_COL))
        );
    }

    private Instant toInstant(AttributeValue attributeValue) {
        if (attributeValue == null ||
                (attributeValue.isNULL() != null && attributeValue.isNULL())) {
            return null;
        } else {
            return Optional.ofNullable(attributeValue.getS())
                    .map(s -> ZonedDateTime.parse(s, DateTimeFormatter.ISO_DATE_TIME).toInstant())
                    .orElse(null);
        }
    }

    public void delete(EmrCluster emrCluster) {
        dynamoDB.deleteItem(new DeleteItemRequest()
            .withTableName(TABLE_NAME)
            .withKey(
                    new AbstractMap.SimpleEntry<>(
                            START_DATE_COL, new AttributeValue().withS(toDateAsString(emrCluster.getStartTime()))
                    ),
                    new AbstractMap.SimpleEntry<>(
                            CLUSTER_ID_COL, new AttributeValue().withS(emrCluster.getClusterId())
                    )
            )
        );
    }

    private String toDateAsString(Instant instant) {
        return instant
                .atZone(ZoneOffset.UTC)
                .toLocalDate()
                .format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

}

package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.googlecode.junittoolbox.PollingWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.TimeUnit.SECONDS;

public class DdbCustomerLookUp {

    private static final String DDB_SERVICE_ENDPOINT = "dynamodb.eu-west-1.amazonaws.com";
    private static final String CUSTOMER_LOOKUP_TABLE_NAME = "Customer-Lookup";
    private static final String SIGNING_REGION = "eu-west-1";

    private static Logger log = LoggerFactory.getLogger(DdbCustomerLookUp.class);

    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
            new AwsClientBuilder.EndpointConfiguration(DDB_SERVICE_ENDPOINT, SIGNING_REGION))
            .build();
    private static DynamoDB dynamoDB = new DynamoDB(client);
    private static Table table = dynamoDB.getTable(CUSTOMER_LOOKUP_TABLE_NAME);

    public static Item getPlayerFromDdb(String username, String brand, long epochTimeAtStartOfTheTest) {
        PollingWait wait = new PollingWait().timeoutAfter(20, SECONDS).pollEvery(1, SECONDS);
        wait.until(() -> {
            Item item = getItem(username, brand);
            boolean itemFound = item != null;
            if (itemFound && checkItemCreatedAtValue(item, epochTimeAtStartOfTheTest)) {
                log.info("Player found in DynamoDB.");
                itemFound = true;
            } else {
                log.info("Player not found in DynamoDB.");
                itemFound = false;
            }
            return itemFound;
        });
        Item item = getItem(username, brand);
        log.info("Item details from DynamoDB: {}\n", item.toJSONPretty());
        return item;
    }

    // We don't know what the value will be. Therefore we check if the value matches the estimated timeframe we would expect.
    private static boolean checkItemCreatedAtValue(Item item, long earliestExpectedItemCreation) {

        long itemCreatedAt = Long.valueOf(item.getString("CreatedAt"));

        String itemCreatedAtAsString = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(itemCreatedAt));
        log.info("Actual item CreatedAt time: {}", itemCreatedAtAsString);

        String earliestExpectedItemCreatedAtAsString = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new java.util.Date(earliestExpectedItemCreation));
        log.info("Polling for CreatedAt not earlier than: {}", earliestExpectedItemCreatedAtAsString);

        return itemCreatedAt >= earliestExpectedItemCreation;
    }

    public static boolean isPlayerInDdb(String username, String brand) {
        return getItem(username, brand) != null;
    }

    private static Item getItem(String username, String brand) {
        log.info("Calling DynamoDB {}:{}", username, brand);
        return table.getItem("UserName", username, "Brand", brand);
    }

    public static void deleteItem(String userName, String brand) {
        DeleteItemSpec deleteItemSpec = new DeleteItemSpec()
                .withPrimaryKey("UserName", userName, "Brand", brand);
        table.deleteItem(deleteItemSpec);
        log.info("User {} deleted from DDB", userName);
    }
}

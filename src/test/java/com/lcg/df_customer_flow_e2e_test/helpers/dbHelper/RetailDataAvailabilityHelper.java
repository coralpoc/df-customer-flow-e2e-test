package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.config.EnvConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RetailDataAvailabilityHelper extends AbstractDbHelper {

    private static final String CONNECTION_URL = EnvConfig.getProperty("sdw.retail.db.url");
    private static final String USER = EnvConfig.getProperty("sdw.retail.db.user");
    private static final String PWD = EnvConfig.getProperty("sdw.retail.db.password");
    private static Connection connection = null;

    public RetailDataAvailabilityHelper() {
        loadOracleDbDriver();
    }

    private boolean isRetailView(String tableName) {
        return ("SF_MASTER_SUBCRIBERS".equalsIgnoreCase(tableName) ||
                "DIM_RTL_CUSTOMERS_CONNECT".equalsIgnoreCase(tableName));
    }

    private void loadOracleDbDriver() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Error: " + e.getMessage(), e);
        }
    }

    private void createDbConnection() {
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PWD);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new RuntimeException("Error connecting to the database: " + e.getMessage(), e);
        }
        log.debug("Connected to db");
    }

    public void setDataAvailabilityToTrue(List<String> tables) {
        createDbConnection();
        try {
            for (String table : tables) {
                if (isRetailView(table))
                    setDataAvailableForView(table);
            }
            // commit also once, for better performance
            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException("Error creating statement: " + e.getMessage());
        } catch (RuntimeException e) {
            throw e;
        } finally {
            closeConnection();
        }
    }

    public void doNotSetDataAvailabilityToToday() {
        createDbConnection();
        String query = "UPDATE odi_work.snp_session SET sess_beg = TRUNC(SYSDATE), sess_end = TRUNC(SYSDATE - 1) WHERE sess_name in ('PKG_SALESFORCE_CORAL_RETAIL', 'PRC_GENERATE_SF_MASTERSUBCRIPTION_ARCHIVE')";
        executeQuery(query);
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error creating statement: " + e.getMessage());
        } finally {
            closeConnection();
        }
    }

    private void setDataAvailableForView(String table) {
        log.info("Setting {} table data available for diff ...", table);
        String query = null;
        if (table.contains("DIM_RTL_CUSTOMERS_CONNECT")) {
            query = "UPDATE odi_work.snp_session SET sess_beg = TRUNC(SYSDATE), sess_end = TRUNC(SYSDATE) WHERE sess_name = 'PKG_SALESFORCE_CORAL_RETAIL'";
        } else if (table.contains("SF_MASTER_SUBCRIBERS")) {
            query = "UPDATE odi_work.snp_session SET sess_beg = TRUNC(SYSDATE), sess_end = TRUNC(SYSDATE) WHERE sess_name = 'PRC_GENERATE_SF_MASTERSUBCRIPTION_ARCHIVE'";
        }
        executeQuery(query);
    }

    private void executeQuery(String query) {
        try (Statement stmt = connection.createStatement()) {
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            throw new RuntimeException("Error executing query: " + query + " Error message: " + e.getMessage());
        }
    }
}

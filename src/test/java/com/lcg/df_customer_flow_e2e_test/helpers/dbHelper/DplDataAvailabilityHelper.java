package com.lcg.df_customer_flow_e2e_test.helpers.dbHelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.config.EnvConfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DplDataAvailabilityHelper extends AbstractDbHelper {

    private static final String CONNECTION_URL = EnvConfig.getProperty("db.url");
    private static final String USER = EnvConfig.getProperty("db.user");
    private static final String PWD = EnvConfig.getProperty("db.password");

    private static Connection connection = null;

    private List<String> dplViews() {
        List<String> dplViews = new ArrayList<>();
        dplViews.add("SFMC_BALANCE");
        dplViews.add("SFMC_BET_EARLY");
        dplViews.add("SFMC_BET_LIFE");
        dplViews.add("SFMC_CUSTOMER_INFO_EARLY");
        dplViews.add("SFMC_CUSTOMER_INFO_LIFE");
        dplViews.add("SFMC_STATUS");
        dplViews.add("SFMC_DEPOSIT_EARLY");
        dplViews.add("SFMC_DEPOSIT_LIFE");
        dplViews.add("SFMC_LOGIN");
        dplViews.add("SFMC_DEPOSIT_RELATIVE");
        dplViews.add("SFMC_PREDICTIONS");
        dplViews.add("SFMC_SEGMENTATIONS");
        return dplViews;
    }

    private void createDbConnection() {
    	log.debug("Attempting to connect to dpl db with url {}",CONNECTION_URL);
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PWD);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new RuntimeException("Error connecting to the database: " + e.getMessage(), e);
        }
        log.debug("Connected to db");
    }

    public void setDataAvailabilityToTrue(List<String> tables, int mins) {
        createDbConnection();
        try {
            tables.forEach(table -> {
                if (dplViews().contains(table)) {
                    if (table.equals("SFMC_BALANCE")) {
                        for (int i = 0; i < 4; i++) {
                            setDataAvailableForView(table, mins);
                        }
                    } else {
                        setDataAvailableForView(table, mins);
                    }
                }
            });

            // commit also once, for better performance
            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException("Error creating statement: " + e.getMessage());
        } catch (RuntimeException e) {
            throw e;
        } finally {
            closeConnection();
        }
    }

    public void cleanupExistingAvailabilityInfo() {
        createDbConnection();
        String query = "DELETE FROM DPL.SFMC_LOAD_STATUS WHERE ViewName IN ('SFMC_BALANCE', 'SFMC_BET_EARLY', 'SFMC_BET_LIFE', 'SFMC_CUSTOMER_INFO_EARLY', 'SFMC_CUSTOMER_INFO_LIFE', 'SFMC_DEPOSIT_EARLY', 'SFMC_DEPOSIT_LIFE', 'SFMC_DEPOSIT_RELATIVE', 'SFMC_LOGIN', 'SFMC_PREDICTIONS', 'SFMC_SEGMENTATIONS', 'SFMC_STATUS') AND CAST(EndDateTime AS DATE) = CAST(GETDATE() AS DATE) AND CAST(StartDateTime AS DATE) = CAST(GETDATE() AS DATE)";
        executeQuery(query);
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error creating statement: " + e.getMessage());
        } finally {
            closeConnection();
        }
    }
    
    public void clearDPLLoadStatusTableForViews(List<String> views) {
    	
    	String deleteViewStr = "DELETE FROM DPL.SFMC_LOAD_STATUS WHERE ViewName IN " + createInQueryPlaceholder(views.size()) + 
    			" AND CAST(EndDateTime AS DATE) = CAST(GETDATE() AS DATE) AND CAST(StartDateTime AS DATE) = CAST(GETDATE() AS DATE)";
    	
        createDbConnection();

        try {
        	PreparedStatement ps = connection.prepareStatement(deleteViewStr);
        	
        	for(int i=1;i<views.size(); i++) {
        		ps.setString(i, views.get(i-1));
        	}
        	ps.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException("clearDPLLoadStatusTableForViews: Error Exceuting SQL statement: " + e.getMessage());
        } finally {
            closeConnection();
        }
    }
    
    private static String createInQueryPlaceholder(int count) {
    	StringBuilder builder = new StringBuilder();
    	builder.append("(");
    	for(int i=0; i < count; i++) {
    		builder.append(" ?,");
    	}
    	builder.deleteCharAt(builder.length() -1);
    	builder.append(")");
    	
    	return builder.toString();
    }

    private void setDataAvailableForView(String viewName, int mins) {
        log.debug("Setting {} table data available for diff ...", viewName);
        String rawQuery = "INSERT INTO DPL.SFMC_LOAD_STATUS(ViewName, StartDateTime, EndDateTime, ProcessName, ParameterStartDateID) VALUES('%s', DATEADD(MINUTE, -%s, CURRENT_TIMESTAMP), CURRENT_TIMESTAMP, SUBSTRING(CONVERT(VARCHAR(40), NEWID()),0,9), CONVERT(VARCHAR(8), DATEADD(DAY, -1, CURRENT_TIMESTAMP), 112))";
        String processedQuery = String.format(rawQuery, viewName, mins);
        executeQuery(processedQuery);
    }

    private void executeQuery(String query) {
        try (Statement stmt = connection.createStatement()) {
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            throw new RuntimeException("Error executing query: " + query + " Error message: " + e.getMessage());
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.trigger;

public class DplExtractorConfig {

    private String clusterId;
//    private int partitionCount = 300;
    private String extractionType;
    private String kafkaConsumerName;

//    public int getPartitionCount() {
//        return partitionCount;
//    }

//    public void setPartitionCount(int partitionCount) {
//        this.partitionCount = partitionCount;
//    }

    public String getExtractionType() {
        return extractionType;
    }

    public String getClusterId() {
        return clusterId;
    }

    public String getKafkaConsumerName() {
        return kafkaConsumerName;
    }

    public void setExtractionType(String extractionType) {
        this.extractionType = extractionType;
    }

    public void setKafkaConsumerName(String kafkaConsumerName) {
        this.kafkaConsumerName = kafkaConsumerName;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }
}

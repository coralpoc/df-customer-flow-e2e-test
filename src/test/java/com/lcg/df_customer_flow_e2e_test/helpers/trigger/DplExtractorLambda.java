package com.lcg.df_customer_flow_e2e_test.helpers.trigger;

import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DplExtractorLambda {

    private DplExtractorService dplExtractorService = LambdaInvokerFactory.builder()
            .lambdaClient(AWSLambdaClientBuilder.standard().withRegion("eu-west-1").build())
            .build(DplExtractorService.class);

    public void invokeMigration() {
        log.info("Starting migration...");
        dplExtractorService.startExtraction(getConfig("MIGRATION"));
        log.info("Lambda function invoked successfully");
    }

    public void invokeDiff() {
        log.info("Starting diff...");
        dplExtractorService.startExtraction(getConfig("DIFF"));
        log.info("Lambda function invoked successfully");
    }

    private DplExtractorConfig getConfig(String extrationType) {
        DplExtractorConfig config = new DplExtractorConfig();
//        if (extrationType.equals("MIGRATION")) {
//            String emrClusterId = EnvConfig.getProperty("migration.emr.cluster.id");
//            config.setClusterId(emrClusterId);
//        }
        config.setExtractionType(extrationType);
        return config;
    }

    public void invokeDmpFullLoad() {
        log.info("Starting Full load...");
        DplExtractorConfig fullLoadConfig = new DplExtractorConfig();
        fullLoadConfig.setExtractionType("MIGRATION");
        fullLoadConfig.setKafkaConsumerName("DMP");
        dplExtractorService.startExtraction(fullLoadConfig);
        log.info("Lambda function invoked successfully");
    }
}

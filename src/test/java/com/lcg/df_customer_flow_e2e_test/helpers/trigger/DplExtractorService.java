package com.lcg.df_customer_flow_e2e_test.helpers.trigger;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface DplExtractorService {

    @LambdaFunction(functionName = "df-customer-dpl-extractor")
    void startExtraction(DplExtractorConfig config);
}

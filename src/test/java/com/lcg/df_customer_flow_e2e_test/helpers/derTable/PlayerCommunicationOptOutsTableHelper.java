package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getPasswordFor;
import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getUrlFor;
import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getUserFor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.config.ConfigDetector;
import com.lcg.df_customer_flow_e2e_test.pojos.der.PlayerCommunicationOptOutsRecord;

public class PlayerCommunicationOptOutsTableHelper {

    static final String dataBase = ConfigDetector.getDerDataBase();

    public void insertPlayerRecord(PlayerCommunicationOptOutsRecord record) {

        String stm = "INSERT INTO PLAYERCOMMUNICATIONOPTOUTS (PLAYERCODE, CASINONAME, CHANNELTYPE, OPTOUTTYPE, SYNCDATE, der_update_time) VALUES" +
                "(?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getPlayerCode());
            pst.setString(2, record.getCasinoName());
            pst.setString(3, record.getChannelType());
            pst.setString(4, record.getOptOutType());
            pst.setDate(5, record.getSyncDate());
            pst.setString(6, record.getDerUpdateTime());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }

    public void deletePlayerRecord(int playerCode) {

        String stm = "DELETE FROM PLAYERCOMMUNICATIONOPTOUTS WHERE playercode = ?";

        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);
            pst.setInt(1, playerCode);
            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting record from the database", e);
        }
    }

    public void updatePlayerRecord(String column, Object value, PlayerCommunicationOptOutsRecord record ) {
        String stm = String.format("UPDATE PLAYERCOMMUNICATIONOPTOUTS SET %s = ? WHERE playercode = ?", column);
        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setObject(1, value);
            pst.setInt(2, record.getPlayerCode());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import com.lcg.df_customer_flow_e2e_test.pojos.der.PlayerRecord;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class PlayerTableHelper {

    private final PlayerRecord playerRecord;

    private PlayerAccountTableHelper playerAccountTableHelper = new PlayerAccountTableHelper();
    private PlayerPersonalTableHelper playerPersonalTableHelper = new PlayerPersonalTableHelper();
    private PlayerVerificationTableHelper playerVerificationTableHelper = new PlayerVerificationTableHelper();
    private PlayerNetworkTableHelper playerNetworkTableHelper = new PlayerNetworkTableHelper();
    private PlayerSignupTableHelper playerSignupTableHelper = new PlayerSignupTableHelper();
    private PlayerSelfExclusionsTableHelper playerSelfExclusionsTableHelper = new PlayerSelfExclusionsTableHelper();
    private PlayerCommunicationOptOutsTableHelper playerCommunicationOptOutsTableHelper = new PlayerCommunicationOptOutsTableHelper();

    public PlayerTableHelper( PlayerRecord playerRecord) {
        this.playerRecord = playerRecord;
    }

    public void insertAllPlayerRecords() {
        log.info("Inserting player into der DB");
        playerAccountTableHelper.insertPlayerRecord(playerRecord.getPlayerAccountRecord());
        playerPersonalTableHelper.insertPlayerRecord(playerRecord.getPlayerPersonalRecord());
        playerVerificationTableHelper.insertPlayerRecord(playerRecord.getPlayerVerificationRecord());
        playerNetworkTableHelper.insertPlayerRecord(playerRecord.getPlayerNetworkRecord());
        playerSignupTableHelper.insertPlayerRecord( playerRecord.getPlayerSignUpRecord());
        playerSelfExclusionsTableHelper.insertPlayerRecord(playerRecord.getPlayerSelfExclusionsRecord());
        playerCommunicationOptOutsTableHelper.insertPlayerRecord(playerRecord.getPlayerCommunicationOptOutsRecord());
        log.info("Player inserted into der DB");
    }

    public void deletePlayerFromAllTables(int playerId) {
        log.info("Deleting player from der DB");
        playerAccountTableHelper.deletePlayerRecord(playerId);
        playerPersonalTableHelper.deletePlayerRecord(playerId);
        playerVerificationTableHelper.deletePlayerRecord(playerId);
        playerNetworkTableHelper.deletePlayerRecord(playerId);
        playerSignupTableHelper.deletePlayerRecord(playerId);
        playerSelfExclusionsTableHelper.deletePlayerRecord(playerId);
        playerCommunicationOptOutsTableHelper.deletePlayerRecord(playerId);
        log.info("Player deleted from der DB\n");
    }

    public void updatePlayerRecord(String table, String column, Object value) {
        if(value.toString().equals("null")) {
            value = null;
        }
        switch (table) {
            case "player_account":
                playerAccountTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerAccountRecord());
                break;
            case "player_personal":
                playerPersonalTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerPersonalRecord());
                break;
            case "player_verification":
                playerVerificationTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerVerificationRecord());
                break;
            case "player_network":
                playerNetworkTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerNetworkRecord());
                break;
            case "player_signup":
                playerSignupTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerSignUpRecord());
                break;
            case "playerselfexclusions":
                playerSelfExclusionsTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerSelfExclusionsRecord());
                break;
            case "playercommunicationoptouts":
                playerCommunicationOptOutsTableHelper.updatePlayerRecord(column,value,playerRecord.getPlayerCommunicationOptOutsRecord());
                break;
            default:
                String errorMessage = String.format("Unknown table name: %s", table);
                throw new RuntimeException(errorMessage);
        }
    }
}

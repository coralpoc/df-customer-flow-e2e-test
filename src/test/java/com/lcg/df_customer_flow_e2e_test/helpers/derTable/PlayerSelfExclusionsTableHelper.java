package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.der.PlayerSelfExclusionsRecord;

public class PlayerSelfExclusionsTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(PlayerSelfExclusionsRecord record) {

        String stm = "INSERT INTO PLAYERSELFEXCLUSIONS (CODE, STARTDATE, ENDDATE, der_update_time, TYPE) VALUES" +
                "(?, ?, ?, ?, ?)";
        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCode());
            pst.setDate(2, record.getStartDate());
            pst.setDate(3, record.getEndDate());
            pst.setString(4, record.getDerUpdateTime());
            pst.setString(5, record.getType());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }

    public void deletePlayerRecord(int playerId) {
        deletePlayerRecord("PLAYERSELFEXCLUSIONS", playerId);
    }

    public void updatePlayerRecord(String column, Object value, PlayerSelfExclusionsRecord record) {
        updatePlayerRecord("PLAYERSELFEXCLUSIONS",column, value, record);
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getPasswordFor;
import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getUrlFor;
import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getUserFor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.config.ConfigDetector;
import com.lcg.df_customer_flow_e2e_test.pojos.der.BaseRecord;

class BaseTableHelper {

    static final String dataBase = ConfigDetector.getDerDataBase();

     public void deletePlayerRecord(String table, int code) {
        checkIfWeCanDeleteRecordsFromThisTable(table);
        checkIfWeCanDeleteRecordsInThisEnv();

        String stm = String.format("DELETE FROM %s WHERE code = ?", table);

        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);
            pst.setInt(1, code);
            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting record from the database", e);
        }
    }

    public void updatePlayerRecord(String table, String column, Object value, BaseRecord record ) {
        String stm = String.format("UPDATE %s SET %s = ? WHERE code = ?", table, column);
        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setObject(1, value);
            pst.setInt(2, record.getCode());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error updating record in the database", e);
        }
    }

    private void checkIfWeCanDeleteRecordsFromThisTable(String table) {
        List<String> whitelistedTables = new ArrayList<>();
        whitelistedTables.add("PLAYER_ACCOUNT");
        whitelistedTables.add("PLAYER_NETWORK");
        whitelistedTables.add("PLAYER_PERSONAL");
        whitelistedTables.add("PLAYERSELFEXCLUSIONS");
        whitelistedTables.add("PLAYER_SIGNUP");
        whitelistedTables.add("PLAYER_VERIFICATION");

        if (!whitelistedTables.contains(table.toUpperCase())) {
            String message = String.format("Tables from where we can delete records are %s.\n", whitelistedTables.toString());
            throw new RuntimeException(message);
        }
    }

    private static void checkIfWeCanDeleteRecordsInThisEnv() {
        String env = ConfigDetector.getEnv();
        if (!Boolean.parseBoolean(System.getProperty("skip.env.check"))) {
            List<String> whitelistedEnvs = new ArrayList<>();
            whitelistedEnvs.add("inmemory");
            whitelistedEnvs.add("dev");
            whitelistedEnvs.add("sit");

            if (!whitelistedEnvs.contains(env)) {
                String message = String.format("Envs where we can always run these tests are %s.\n" +
                        "If you are sure you want to run this test in '%s' env, please specify '-Dskip.env.check=true' option.", whitelistedEnvs.toString(), env);
                throw new RuntimeException(message);
            }
        }
    }
}

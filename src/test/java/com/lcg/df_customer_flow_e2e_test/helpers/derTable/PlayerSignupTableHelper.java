package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.der.PlayerSignUpRecord;

public class PlayerSignupTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(PlayerSignUpRecord record) {

        String stm = "INSERT INTO PLAYER_SIGNUP (CODE, SIGNUPDATE, der_update_time) VALUES" +
                "(?, ?, ?)";
        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCode());
            pst.setTimestamp(2, record.getSignUpDate());
            pst.setString(3, record.getDerUpdateTime());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }

    public void deletePlayerRecord(int playerId) {
        deletePlayerRecord("PLAYER_SIGNUP", playerId);
    }

    public void updatePlayerRecord(String column, Object value, PlayerSignUpRecord record) {
        updatePlayerRecord("PLAYER_SIGNUP",column, value, record);
    }
}

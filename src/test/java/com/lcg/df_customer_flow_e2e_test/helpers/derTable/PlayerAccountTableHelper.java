package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getPasswordFor;
import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getUrlFor;
import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.getUserFor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.der.PlayerAccountRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlayerAccountTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(PlayerAccountRecord record) {

        String stm = "INSERT INTO PLAYER_ACCOUNT (ACCOUNTBUSINESSPHASE, CASINONAME, CODE, CURRENCYCODE, der_update_time, FROZEN, LASTLOGINDATE, PROFILEID, TOKENCODE, VERIFICATIONANSWER, VERIFICATIONQUESTION, VIPLEVEL) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String dbUrl = getUrlFor(dataBase);
        String dbUser = getUserFor(dataBase);
        log.info("Database details: url={} user={}", dbUrl, dbUser);
        try (
                Connection conn = DriverManager.getConnection(dbUrl, dbUser, getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setString(1, record.getAccountBusinessPhase());
            pst.setString(2, record.getCasinoName());
            pst.setObject(3, record.getCode());
            pst.setString(4, record.getCurrencyCode());
            pst.setString(5, record.getDerUpdateTime());
            pst.setObject(6, record.getFrozen());
            pst.setTimestamp(7, record.getLastLoginDate());
            pst.setString(8, record.getProfileId());
            pst.setString(9, record.getTokenCode());
            pst.setString(10, record.getVerificationAnswer());
            pst.setString(11, record.getVerificationQuestion());
            pst.setObject(12, record.getVipLevel());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }

    public void deletePlayerRecord(int playerId) {
        deletePlayerRecord("PLAYER_ACCOUNT", playerId);
    }

    public void updatePlayerRecord(String column, Object value, PlayerAccountRecord record) {
        updatePlayerRecord("PLAYER_ACCOUNT",column, value, record);
    }
}

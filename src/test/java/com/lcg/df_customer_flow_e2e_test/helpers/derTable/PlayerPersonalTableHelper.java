package com.lcg.df_customer_flow_e2e_test.helpers.derTable;

import static com.lcg.df_customer_flow_e2e_test.config.DerDbConfig.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.der.PlayerPersonalRecord;

public class PlayerPersonalTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(PlayerPersonalRecord record) {

        String stm = "INSERT INTO PLAYER_PERSONAL (CODE, ADDRESS, BIRTHDATE, CELLPHONE, CITY, COUNTRYNAME, der_update_time, EMAIL, FIRSTNAME, GENDER, LASTNAME, PHONE, STATE, TITLE, USERNAME, ZIP) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (
                Connection conn = DriverManager.getConnection(getUrlFor(dataBase), getUserFor(dataBase), getPasswordFor(dataBase));
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCode());
            pst.setString(2, record.getAddress());
            pst.setDate(3, record.getBirthDate());
            pst.setString(4, record.getCellPhone());
            pst.setString(5, record.getCity());
            pst.setString(6, record.getCountryName());
            pst.setString(7, record.getDerUpdateTime());
            pst.setString(8, record.getEmail());
            pst.setString(9, record.getFirstName());
            pst.setString(10, record.getGender());
            pst.setString(11, record.getLastName());
            pst.setString(12, record.getPhone());
            pst.setString(13, record.getState());
            pst.setString(14, record.getTitle());
            pst.setString(15, record.getUserName());
            pst.setString(16, record.getZip());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }

    public void deletePlayerRecord(int playerId) {
        deletePlayerRecord("PLAYER_PERSONAL", playerId);
    }

    public void updatePlayerRecord(String column, Object value, PlayerPersonalRecord record) {
        updatePlayerRecord("PLAYER_PERSONAL",column, value, record);
    }
}

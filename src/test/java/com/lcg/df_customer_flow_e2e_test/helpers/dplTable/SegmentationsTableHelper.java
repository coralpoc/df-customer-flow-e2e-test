package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.SegmentationsRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SegmentationsTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(SegmentationsRecord record) {

        String stm = "INSERT INTO DPL.SFMC_SEGMENTATIONS(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, CUSTPRODSEGMNTTOPLEVEL, CUSTPRODSEGMNTMIDLEVEL, " +
                "CUSTPRODSEGMNTLOWLEVEL, BRANDBEHAVSUPERSEGMNT, BRANDBEHAVSUBSEGMNT, BRANDVALUESUPERSEGMNT, BRANDVALUESUBSEGMNT, SOURCEOFPLAYSEGMNT, " +
                "CUSTCHANNELSEGMNT, SHOPCARDMGMNTSEGMNT, LIFECYCLESEGMNT, CUSTSPORTSBETSEGMNT, CUSTSPORTSMARKETSEGMNT, CUSTSPORTSTIMESEGMNT, " +
                "CUSTBONUSSEGMNT, PREFERREDDEPOSITINGQOM, PREFERREDDEPOSITINGDOW, PAYMENTPATTERN, ETL_UPDATEDATE, CUSTCOHORTPERSONA) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setString(4, record.getCustProdSegmntTopLevel());
            pst.setString(5, record.getCustProdSegmntMidLevel());
            pst.setString(6, record.getCustProdSegmntLowLevel());
            pst.setString(7, record.getBrandBehavSuperSegmnt());
            pst.setObject(8, record.getBrandBehavSubSegmnt());
            pst.setString(9, record.getBrandValueSuperSegmnt());
            pst.setObject(10, record.getBrandValueSubSegmnt());
            pst.setString(11, record.getSourceOfPlaySegmnt());
            pst.setString(12, record.getCustChannelSegmnt());
            pst.setString(13, record.getShopCardMgmntSegmnt());
            pst.setString(14, record.getLifecycleSegmnt());
            pst.setString(15, record.getCustSportsBetSegmnt());
            pst.setString(16, record.getCustSportsMarketSegmnt());
            pst.setString(17, record.getCustSportsTimeSegmnt());
            pst.setString(18, record.getCustBonusSegmnt());
            pst.setString(19, record.getPreferredDepositingQOM());
            pst.setString(20, record.getPreferredDepositingDOW());
            pst.setString(21, record.getPaymentPattern());
            pst.setTimestamp(22, record.getETL_UPDATEDATE());
            pst.setString(23, record.getCustCohortPersona());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

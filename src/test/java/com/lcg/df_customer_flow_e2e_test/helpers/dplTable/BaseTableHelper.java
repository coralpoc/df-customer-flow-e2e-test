package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.config.EnvConfig;

public class BaseTableHelper {

	private final String CONNECTION_URL = EnvConfig.getProperty("db.url");
	private final String USER = EnvConfig.getProperty("db.user");
	private final String PWD = EnvConfig.getProperty("db.password");
	
	public final String SFMC_BALANCE = "DPL.SFMC_BALANCE";
	public final String SFMC_BET_EARLY = "DPL.SFMC_BET_EARLY";
	public final String SFMC_BET_LIFE = "DPL.SFMC_BET_LIFE";
	public final String SFMC_CUSTOMER_INFO_EARLY = "DPL.SFMC_CUSTOMER_INFO_EARLY";
	public final String SFMC_CUSTOMER_INFO_LIFE = "DPL.SFMC_CUSTOMER_INFO_LIFE";
	public final String SFMC_DEPOSIT_EARLY = "DPL.SFMC_DEPOSIT_EARLY";
	public final String SFMC_DEPOSIT_LIFE = "DPL.SFMC_DEPOSIT_LIFE";
	public final String SFMC_DEPOSIT_RELATIVE = "DPL.SFMC_DEPOSIT_RELATIVE";
	public final String SFMC_LOGIN = "DPL.SFMC_LOGIN";
	public final String SFMC_STATUS = "DPL.SFMC_STATUS";
	public final String SFMC_SEGMENTATIONS = "DPL.SFMC_SEGMENTATIONS";
	public final String SFMC_PREDICTIONS = "DPL.SFMC_PREDICTIONS";
	

    private final List<String> dplTables;

    public BaseTableHelper() {
        dplTables = new ArrayList<>();
        dplTables.add(SFMC_BALANCE);
        dplTables.add(SFMC_BET_EARLY);
        dplTables.add(SFMC_BET_LIFE);
        dplTables.add(SFMC_CUSTOMER_INFO_EARLY);
        dplTables.add(SFMC_CUSTOMER_INFO_LIFE);
        dplTables.add(SFMC_DEPOSIT_EARLY);
        dplTables.add(SFMC_DEPOSIT_LIFE);
        dplTables.add(SFMC_DEPOSIT_RELATIVE);
        dplTables.add(SFMC_LOGIN);
        dplTables.add(SFMC_STATUS);
        dplTables.add(SFMC_SEGMENTATIONS);
        dplTables.add(SFMC_PREDICTIONS);
    }
    
    protected Connection getDbConnection() throws SQLException {
    	return DriverManager.getConnection(CONNECTION_URL, USER, PWD);
    }

    public void deleteAllPlayerRecords(int customerSeq) {
        for (String table : dplTables) {
            checkIfWeCanDeleteRecordsFromThisTable(table);

            try (
                    Connection conn = getDbConnection();
            ) {
                String stm = String.format("DELETE FROM %s WHERE CUSTOMER_ID = ?", table);
                PreparedStatement pst = conn.prepareStatement(stm);
                conn.setAutoCommit(false);
                pst.setInt(1, customerSeq);
                pst.executeUpdate();
                conn.commit();
                pst.close();
            } catch (SQLException e) {
                throw new RuntimeException("Error deleting record from the database", e);
            }
        }
    }

    private void checkIfWeCanDeleteRecordsFromThisTable(String table) {
        if (!dplTables.contains(table.toUpperCase())) {
            String message = String.format("Tables from where we can delete records are %s.\n", dplTables.toString());
            throw new RuntimeException(message);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.BetLifeRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BetLifeTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(BetLifeRecord record) {

        String stm = "INSERT INTO DPL.SFMC_BET_LIFE(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, LASTBETDATETIME, LASTBETPRODTYPE, LASTBETAMT, " +
                "LASTBETOUTCOMEWINLOSE, TOTALNUMBETS, TOTALSTAKEAMT, TOTALWINAMT, TOTALLOSSAMT, TOTALGROSSWINAMT, TOTALNGR, NUMACTIVEDAYS, " +
                "LASTUNSETTLEDBETDATE, PREFERREDBINGOROOM, MOBILEPLAYER, CASHOUTCUST, PARTIALCASHOUTCUST, HASBETONCASINO, HASBETINPLAY, " +
                "SPORTSLASTBETDATE, SPORTSTOTALSTAKEAMT, SPORTSTOTALNGRAMT, CASINOLASTBETDATE, CASINOTOTALSTAKEAMT, CASINOTOTALNGRAMT, " +
                "BINGOLASTBETDATE, BINGOTOTALSTAKEAMT, BINGOTOTALNGRAMT, POKERLASTBETDATE, POKERTOTALSTAKEAMT, POKERTOTALNGRAMT, BETONLINE, " +
                "SECTOLASTBETDATE, LASTPLAYEDSLOTSDATE, LASTUSEDMAINAPP, LASTUSEDSLOTSGAMESAPP, ETL_UPDATE_DATE_BET_LIFE, " +
                "SPORTS3MNTHAVGSTAKEAMT,CASINO3MNTHAVGSTAKEAMT,BINGO3MNTHAVGSTAKEAMT,LIVECASINO3MNTHAVGSTAKEAMT) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setTimestamp(4, record.getLastBetDateTime());
            pst.setString(5, record.getLastBetProdType());
            pst.setBigDecimal(6, record.getLastBetAmt());
            pst.setString(7, record.getLastBetOutcomeWinLose());
            pst.setBigDecimal(8, record.getTotalNumBets());
            pst.setBigDecimal(9, record.getTotalStakeAmt());
            pst.setBigDecimal(10, record.getTotalWinAmt());
            pst.setBigDecimal(11, record.getTotalLossAmt());
            pst.setBigDecimal(12, record.getTotalGrossWinAmt());
            pst.setBigDecimal(13, record.getTotalNGR());
            pst.setInt(14, record.getNumActiveDays());
            pst.setTimestamp(15, record.getLastUnsettledBetDate());
            pst.setString(16, record.getPreferredBingoRoom());
            pst.setString(17, record.getMobilePlayer());
            pst.setString(18, record.getCashoutCust());
            pst.setString(19, record.getPartialCashoutCust());
            pst.setString(20, record.getHasBetOnCasino());
            pst.setString(21, record.getHasBetInPlay());
            pst.setTimestamp(22, record.getSportsLastBetDate());
            pst.setBigDecimal(23, record.getSportsTotalStakeAmt());
            pst.setBigDecimal(24, record.getSportsTotalNGRAmt());
            pst.setTimestamp(25, record.getCasinoLastBetDate());
            pst.setBigDecimal(26, record.getCasinoTotalStakeAmt());
            pst.setBigDecimal(27, record.getCasinoTotalNGRAmt());
            pst.setTimestamp(28, record.getBingoLastBetDate());
            pst.setBigDecimal(29, record.getBingoTotalStakeAmt());
            pst.setBigDecimal(30, record.getBingoTotalNGRAmt());
            pst.setTimestamp(31, record.getPokerLastBetDate());
            pst.setBigDecimal(32, record.getPokerTotalStakeAmt());
            pst.setBigDecimal(33, record.getPokerTotalNGRAmt());
            pst.setString(34, record.getBetOnline());
            pst.setTimestamp(35, record.getSecToLastBetDate());
            pst.setTimestamp(36, record.getLastPlayedSlotsDate());
            pst.setTimestamp(37, record.getLastUsedMainApp());
            pst.setTimestamp(38, record.getLastUsedSlotsGamesApp());
            pst.setTimestamp(39, record.getETL_UPDATE_DATE_BET_LIFE());
            pst.setBigDecimal(40, record.getSports3mnthAvgStakeAmt());
            pst.setBigDecimal(41, record.getCasino3mnthAvgStakeAmt());
            pst.setBigDecimal(42, record.getBingo3mnthAvgStakeAmt());
            pst.setBigDecimal(43, record.getLiveCasino3mnthAvgStakeAmt());


            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

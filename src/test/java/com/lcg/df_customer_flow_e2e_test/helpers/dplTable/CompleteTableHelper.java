package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.util.Arrays;
import java.util.List;

import com.lcg.df_customer_flow_e2e_test.helpers.sdwTable.retail.RetailBaseTableHelper;
import com.lcg.df_customer_flow_e2e_test.helpers.sdwTable.retail.RetailCustomersConnectTableHelper;
import com.lcg.df_customer_flow_e2e_test.helpers.sdwTable.retail.SfMasterSubcribersTableHelper;
import com.lcg.df_customer_flow_e2e_test.pojos.CommonState;
import com.lcg.df_customer_flow_e2e_test.pojos.dpl.CompletePlayerRecord;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
public class CompleteTableHelper {

    private final CompletePlayerRecord playerRecord;

    private BalanceTableHelper balanceTableHelper = new BalanceTableHelper();
    private BetEarlyTableHelper betEarlyTableHelper = new BetEarlyTableHelper();
    private BetLifeTableHelper betLifeTableHelper = new BetLifeTableHelper();
    private CustomerInfoEarlyTableHelper customerInfoEarlyTableHelper = new CustomerInfoEarlyTableHelper();
    private CustomerInfoLifeTableHelper customerInfoLifeTableHelper = new CustomerInfoLifeTableHelper();
    private DepositEarlyTableHelper depositEarlyTableHelper = new DepositEarlyTableHelper();
    private DepositLifeTableHelper depositLifeTableHelper = new DepositLifeTableHelper();
    private DepositRelativeTableHelper depositRelativeTableHelper = new DepositRelativeTableHelper();
    private LoginTableHelper loginTableHelper = new LoginTableHelper();
    private PredictionsTableHelper predictionsTableHelper = new PredictionsTableHelper();
    private SegmentationsTableHelper segmentationsTableHelper = new SegmentationsTableHelper();
    private StatusTableHelper statusTableHelper = new StatusTableHelper();
    private BaseTableHelper baseTableHelper = new BaseTableHelper();
    private RetailCustomersConnectTableHelper retailCustomersConnectTableHelper = new RetailCustomersConnectTableHelper();
    private SfMasterSubcribersTableHelper sfMasterSubcribersTableHelper = new SfMasterSubcribersTableHelper();
    private RetailBaseTableHelper retailBaseTableHelper = new RetailBaseTableHelper();

    public CompleteTableHelper(CompletePlayerRecord playerRecord) {
        this.playerRecord = playerRecord;
    }

    public void insertAllPlayerRecords() {
        insertAllPlayerRecords(CommonState.getCommonState().getTables());
    }

    public void insertAllPlayerRecords(List<String> tables) {
        if (tables.contains("SFMC_LOGIN")) {
            loginTableHelper.insertPlayerRecord(playerRecord.getLoginRecord());
        }
        if (tables.contains("SFMC_BALANCE")) {
            balanceTableHelper.insertPlayerRecord(playerRecord.getBalanceRecord());
        }
        if (tables.contains("SFMC_BET_EARLY")) {
            betEarlyTableHelper.insertPlayerRecord(playerRecord.getBetEarlyRecord());
        }
        if (tables.contains("SFMC_BET_LIFE")) {
            betLifeTableHelper.insertPlayerRecord(playerRecord.getBetLifeRecord());
        }
        if (tables.contains("SFMC_CUSTOMER_INFO_EARLY")) {
            customerInfoEarlyTableHelper.insertPlayerRecord(playerRecord.getCustomerInfoEarlyRecord());
        }
        if (tables.contains("SFMC_CUSTOMER_INFO_LIFE")) {
            customerInfoLifeTableHelper.insertPlayerRecord(playerRecord.getCustomerInfoLifeRecord());
        }
        if (tables.contains("SFMC_DEPOSIT_EARLY")) {
            depositEarlyTableHelper.insertPlayerRecord(playerRecord.getDepositEarlyRecord());
        }
        if (tables.contains("SFMC_DEPOSIT_LIFE")) {
            depositLifeTableHelper.insertPlayerRecord(playerRecord.getDepositLifeRecord());
        }
        if (tables.contains("SFMC_DEPOSIT_RELATIVE")) {
            depositRelativeTableHelper.insertPlayerRecord(playerRecord.getDepositRelativeRecord());
        }
        if (tables.contains("SFMC_PREDICTIONS")) {
            predictionsTableHelper.insertPlayerRecord(playerRecord.getPredictionsRecord());
        }
        if (tables.contains("SFMC_SEGMENTATIONS")) {
            segmentationsTableHelper.insertPlayerRecord(playerRecord.getSegmentationsRecord());
        }
        if (tables.contains("SFMC_STATUS")) {
            statusTableHelper.insertPlayerRecord(playerRecord.getStatusRecord());
        }
        if (tables.contains("DIM_RTL_CUSTOMERS_CONNECT") && playerRecord.getRetailCustomersConnectRecord().getBRAND_SEQ() == 4) {
            retailCustomersConnectTableHelper.insertPlayerRecord(playerRecord.getRetailCustomersConnectRecord());
        }
        if (tables.contains("SF_MASTER_SUBCRIBERS") && Arrays.asList(2, 3, 4).contains(playerRecord.getSfMasterSubcribersRecord().getBRAND_SEQ())) {
            sfMasterSubcribersTableHelper.insertPlayerRecord(playerRecord.getSfMasterSubcribersRecord());
        }
    }

    public void deletePlayerFromAllDPLTables(int customerId) {

        baseTableHelper.deleteAllPlayerRecords(customerId);
        log.info("Player {} deleted from DPL DB", customerId);

    }
    
    public void deletePlayerFromAllSDWTables(int customerId) {
        
        retailBaseTableHelper.deletePlayerRecord(customerId);
        log.info("Player {} deleted from SDW DB", customerId);
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.BetEarlyRecord;

@Slf4j
public class BetEarlyTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(BetEarlyRecord record) {

        String stm = "INSERT INTO DPL.SFMC_BET_EARLY(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, FIRSTBETDATETIME, FIRSTBETPRODTYPE, " +
                "FIRSTBETAMT, FIRSTBETOUTCOMEWINLOSE, SECONDBETTYPEPROD, SECONDBETAMTPROD, SECONDBETOUTCOME, THIRDBETTYPEPROD, " +
                "THIRDBETAMTPROD, THIRDBETOUTCOME, SPORTSFIRSTBETDATE, CASINOFIRSTBETDATE, BINGOFIRSTBETDATE, POKERFIRSTBETDATE, " +
                "ETL_UPDATE_DATE_BET_EARLY) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setTimestamp(4, record.getFirstBetDateTime());
            pst.setString(5, record.getFirstBetProdType());
            pst.setBigDecimal(6, record.getFirstBetAmt());
            pst.setString(7, record.getFirstBetOutcomeWinLose());
            pst.setString(8, record.getSecondBetTypeProd());
            pst.setBigDecimal(9, record.getSecondBetAmtProd());
            pst.setString(10, record.getSecondBetOutcome());
            pst.setString(11, record.getThirdBetTypeProd());
            pst.setBigDecimal(12, record.getThirdBetAmtProd());
            pst.setString(13, record.getThirdBetOutcome());
            pst.setTimestamp(14, record.getSportsFirstBetDate());
            pst.setTimestamp(15, record.getCasinoFirstBetDate());
            pst.setTimestamp(16, record.getBingoFirstBetDate());
            pst.setTimestamp(17, record.getPokerFirstBetDate());
            pst.setTimestamp(18, record.getETL_UPDATE_DATE_BET_EARLY());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.DepositRelativeRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DepositRelativeTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(DepositRelativeRecord record) {

        String stm = "INSERT INTO DPL.SFMC_DEPOSIT_RELATIVE(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, NUMDEPOSITLAST7DAYS, TOTALAMTDEPOSITLAST7DAYS, " +
                "NUMDEPOSITLAST28DAYS, TOTALAMTDEPOSITLAST28DAYS, NUMDEPOSITPREVPERIOD, TOTALAMTDEPOSITPREVPERIOD, NUMDEPOSITCURRPERIOD, " +
                "TOTALAMTDEPOSITCURRPERIOD, RETAILAMTDEPOSITLAST7DAYS, RETAILAMTDEPOSITLAST28DAYS, RETAILAMTWITHDRAWLAST7DAYS, " +
                "RETAILAMTWITHDRAWLAST28DAYS, ETL_UPDATE_DATE_DEPOSIT_RELATIVE) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setInt(4, record.getNumDepositLast7Days());
            pst.setBigDecimal(5, record.getTotalAmtDepositLast7Days());
            pst.setInt(6, record.getNumDepositLast28Days());
            pst.setBigDecimal(7, record.getTotalAmtDepositLast28Days());
            pst.setInt(8, record.getNumDepositPrevPeriod());
            pst.setBigDecimal(9, record.getTotalAmtDepositPrevPeriod());
            pst.setInt(10, record.getNumDepositCurrPeriod());
            pst.setBigDecimal(11, record.getTotalAmtDepositCurrPeriod());
            pst.setBigDecimal(12, record.getRetailAmtDepositLast7Days());
            pst.setBigDecimal(13, record.getRetailAmtDepositLast28Days());
            pst.setBigDecimal(14, record.getRetailAmtWithdrawLast7Days());
            pst.setBigDecimal(15, record.getRetailAmtWithdrawLast28Days());
            pst.setTimestamp(16, record.getETL_UPDATE_DATE_DEPOSIT_RELATIVE());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

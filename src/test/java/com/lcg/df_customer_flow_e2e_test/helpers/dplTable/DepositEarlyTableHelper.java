package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.DepositEarlyRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DepositEarlyTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(DepositEarlyRecord record) {

        String stm = "INSERT INTO DPL.SFMC_DEPOSIT_EARLY(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, FIRSTDEPOSITDATETIME, SECONDDEPOSITDATETIME, " +
                "THIRDDEPOSITDATETIME, FIRSTDEPOSITAMT, SECONDDEPOSITAMT, THIRDDEPOSITAMT, FIRSTDECLINEDDEPOSITDATE, FIRSTSHOPDEPOSITDATE, " +
                "FIRSTSHOPDEPOSITAMT, ETL_UPDATE_DATE_DEPOSIT_EARLY) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setTimestamp(4, record.getFirstDepositDateTime());
            pst.setTimestamp(5, record.getSecondDepositDateTime());
            pst.setTimestamp(6, record.getThirdDepositDateTime());
            pst.setBigDecimal(7, record.getFirstDepositAmt());
            pst.setBigDecimal(8, record.getSecondDepositAmt());
            pst.setBigDecimal(9, record.getThirdDepositAmt());
            pst.setTimestamp(10, record.getFirstDeclinedDepositDate());
            pst.setTimestamp(11, record.getFirstShopDepositDate());
            pst.setBigDecimal(12, record.getFirstShopDepositAmt());
            pst.setTimestamp(13, record.getETL_UPDATE_DATE_DEPOSIT_EARLY());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

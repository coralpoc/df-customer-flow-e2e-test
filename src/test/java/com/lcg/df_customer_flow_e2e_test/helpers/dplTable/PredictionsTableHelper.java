package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.PredictionsRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PredictionsTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(PredictionsRecord record) {

        String stm = "INSERT INTO DPL.SFMC_PREDICTIONS(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, PVIPFLAG, PVIPSCORE, PREDLTV, PREDCHURNSCORE, " +
                "PREDWISEGUYFLAG, PREDWISEGUYSCORE, FEATURESPACESCORE, FEATURESPACEREASON, BONUSUPLIFTPREDICTION, PHARMSCORE, PHARMREASON, " +
                "ETL_UPDATE_DATE_PREDICTIONS, PREDCHURNFLAG) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setString(4, record.getPvipFlag());
            pst.setBigDecimal(5, record.getPvipScore());
            pst.setBigDecimal(6, record.getPredLtv());
            pst.setBigDecimal(7, record.getPredChurnScore());
            pst.setString(8, record.getPredWiseGuyFlag());
            pst.setBigDecimal(9, record.getPredWiseGuyScore());
            pst.setObject(10, record.getFeaturespaceScore());
            pst.setObject(11, record.getFeaturespaceReason());
            pst.setObject(12, record.getBonusUpliftPrediction());
            pst.setBigDecimal(13, record.getPharmScore());
            pst.setObject(14, record.getPharmReason());
            pst.setTimestamp(15, record.getETL_UPDATE_DATE_PREDICTIONS());
            pst.setString(16, record.getPredChurnFlag());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

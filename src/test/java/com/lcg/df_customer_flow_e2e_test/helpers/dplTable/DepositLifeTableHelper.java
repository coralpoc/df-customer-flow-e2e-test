package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.DepositLifeRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DepositLifeTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(DepositLifeRecord record) {

        String stm = "INSERT INTO DPL.SFMC_DEPOSIT_LIFE(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, TOTALNUMDEPOSIT, TOTALAMTDEPOSITED, LASTDEPOSITDATE, " +
                "LASTDEPOSITAMT, TOTALNUMDEPOSITDAYS, LASTDECLINEDDEPOSITDATE, LASTDEPOSITDAYAMT, NUMFAILEDDEPOSIT, RETAILLASTSHOPDEPOSITAMT, " +
                "RETAILTOTALAMTDEPOSITED, RETAILLASTWITHDRAWDATE, RETAILLASTWITHDRAWAMT, TOTALRETAILAMTWITHDRAW, ETL_UPDATE_DATE_DEPOSIT_LIFE, " +
                "RETAILLASTSHOPDEPSITDATE, LASTWITHDRAWALDATE) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setBigDecimal(4, record.getTotalNumDeposit());
            pst.setBigDecimal(5, record.getTotalAmtDeposited());
            pst.setTimestamp(6, record.getLastDepositDate());
            pst.setBigDecimal(7, record.getLastDepositAmt());
            pst.setObject(8, record.getTotalNumDepositDays());
            pst.setTimestamp(9, record.getLastDeclinedDepositDate());
            pst.setBigDecimal(10, record.getLastDepositDayAmt());
            pst.setObject(11, record.getNumFailedDeposit());
            pst.setBigDecimal(12, record.getRetailLastShopDepositAmt());
            pst.setBigDecimal(13, record.getRetailTotalAmtDeposited());
            pst.setTimestamp(14, record.getRetailLastWithdrawDate());
            pst.setBigDecimal(15, record.getRetailLastWithdrawAmt());
            pst.setBigDecimal(16, record.getTotalRetailAmtWithdraw());
            pst.setTimestamp(17, record.getETL_UPDATE_DATE_DEPOSIT_LIFE());
            pst.setTimestamp(18, record.getRetailLastShopDepsitDate());
            pst.setTimestamp(19, record.getLastWithdrawalDate());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.CustomerInfoLifeRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerInfoLifeTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(CustomerInfoLifeRecord record) {

        String stm = "INSERT INTO DPL.SFMC_CUSTOMER_INFO_LIFE(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, ACCOUNTTYPE, VIPLEVEL, VIPLEVELCHANGEDATE, " +
                "CREDITCARDEXPIRYDATE, LAST4CARDDIGTS, SPORTSBOOKSTAKEFACTOR, BONUSABUSE, SHOPCARDCUST, SHOPCARDDATETIME, SHOPCARDREGSTORE, " +
                "SPORTSSTAKEFACCHANGEDATE, ETL_UPDATEDATE_CUST_INFO_LIFE, HASLADBROKESACCOUNT, HASCORALACCOUNT, HASGALABINGOACCOUNT, " +
                "HASGALACASINOACCOUNT, SALUTATION, DONOTCALL, PROMOEMAIL, PROMOSMS, PROMODIRECTMAIL, PROMOPHONE," +
                "ACCOUNTSTATUSIMS, VERIFICATIONSTATUS, SELFEXCLUSIONENDDATE, SELFEXCLUSIONSTARTDATE) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setString(4, record.getAccountType());
            pst.setString(5, record.getVIPLevel());
            pst.setTimestamp(6, record.getVIPLevelChangeDate());
            pst.setObject(7, record.getCreditCardExpiryDate());
            pst.setObject(8, record.getLast4CardDigts());
            pst.setString(9, record.getSportsbookStakeFactor());
            pst.setInt(10, record.getBonusAbuse());
            pst.setString(11, record.getShopCardCust());
            pst.setTimestamp(12, record.getShopCardDateTime());
            pst.setString(13, record.getShopCardRegStore());
            pst.setTimestamp(14, record.getSportsStakeFacChangeDate());
            pst.setTimestamp(15, record.getETL_UPDATEDATE_CUST_INFO_LIFE());
            pst.setString(16, record.getHasLadbrokesAccount());
            pst.setString(17, record.getHasCoralAccount());
            pst.setString(18, record.getHasGalaBingoAccount());
            pst.setString(19, record.getHasGalaCasinoAccount());
            pst.setString(20, record.getSalutation());
            pst.setInt(21, record.getDoNotCall());
            pst.setInt(22, record.getPromoEmail());
            pst.setInt(23, record.getPromoSMS());
            pst.setInt(24, record.getPromoDirectMail());
            pst.setInt(25, record.getPromoPhone());
            pst.setString(26, record.getAccountStatusIMS());
            pst.setString(27, record.getVerificationStatus());
            pst.setTimestamp(28, record.getSelfExclusionEndDate());
            pst.setTimestamp(29, record.getSelfExclusionStartDate());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

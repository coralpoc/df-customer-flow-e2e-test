package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.StatusRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StatusTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(StatusRecord record) {

        String stm = "INSERT INTO DPL.SFMC_STATUS(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, CUSTPREVIOUSVIP, CUSTSPREVIOUSVIPLEVEL, ACCOUNTSTATUS, " +
                "OPTINEMAIL, OPTINSMS, OPTINPUSH, OPTINDIRECTMAIL, OPTINOBTM, POKERCRMSTATUS, SPORTSCRMSTATUS, CASINOCRMSTATUS, BINGOCRMSTATUS, " +
                "SELFEXCLUDE, NEARESTRETAILSHOP, SHOPCARDACCOUNTTYPE, DISTANCETOSHOP, REFERREDAFRIEND, SPORTSSTAKEFACTORPREVALUE, AUTOSUPPRESS, " +
                "RETAILDISTANCETOPREFERREDSHOP, RETAILNEARESTSHOPNAME, ETL_UPDATE_DATE_STATUS, CRMACCOUNTSTATUSLASTUPDATED, AUTOSUPPRESSLASTUPDATED, CONSENTEXPIRYDATE) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setString(4, record.getCustPreviousVIP());
            pst.setString(5, record.getCustsPreviousVIPLevel());
            pst.setString(6, record.getAccountStatus());
            pst.setString(7, record.getOptinEmail());
            pst.setString(8, record.getOptinSMS());
            pst.setObject(9, record.getOptinPush());
            pst.setString(10, record.getOptinDirectMail());
            pst.setString(11, record.getOptinOBTM());
            pst.setString(12, record.getPokerCRMStatus());
            pst.setString(13, record.getSportsCRMStatus());
            pst.setString(14, record.getCasinoCRMStatus());
            pst.setString(15, record.getBingoCRMStatus());
            pst.setString(16, record.getSelfExclude());
            pst.setString(17, record.getNearestRetailShop());
            pst.setString(18, record.getShopCardAccountType());
            pst.setBigDecimal(19, record.getDistanceToShop());
            pst.setString(20, record.getReferredAFriend());
            pst.setString(21, record.getSportsStakeFactorPreValue());
            pst.setString(22, record.getAutoSuppress());
            pst.setBigDecimal(23, record.getRetailDistanceToPreferredShop());
            pst.setString(24, record.getRetailNearestShopName());
            pst.setTimestamp(25, record.getETL_UPDATE_DATE_STATUS());
            pst.setTimestamp(26, record.getCrmAccountStatusLastUpdated());
            pst.setTimestamp(27, record.getAutoSuppressLastUpdated());
            pst.setTimestamp(28, record.getConsentExpiryDate());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.LoginRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(LoginRecord record) {

        String stm = "INSERT INTO DPL.SFMC_LOGIN(CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, LASTLOGINDATE, DATELASTFAILEDLOGIN, ETL_UPDATE_DATE_WEBSITE_LIFE) VALUES" +
                "(?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setTimestamp(4, record.getLastLoginDate());
            pst.setTimestamp(5, record.getDateLastFailedLogin());
            pst.setTimestamp(6, record.getETL_UPDATE_DATE_WEBSITE_LIFE());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.CustomerInfoEarlyRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerInfoEarlyTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(CustomerInfoEarlyRecord record) {

        String stm = "INSERT INTO DPL.SFMC_CUSTOMER_INFO_EARLY(CUSTOMERSEQ, BRAND_SEQ, CUSTOMER_ID, EMAILADDRESS, PLAYERID, " +
                "SPORTSBOOKCUSTID, BINGOVFUSERNAME, USERNAME, MOBILENUMBER, HOMENUMBER, FIRSTNAME, LASTNAME, ADDRESS1, CITY, POSTCODE, " +
                "BRAND, COUNTRY, GENDER, DOB, BETTINGCURRENCY, IMSPROFILEID, SIGNUPPRODVERTICAL, REGDATETIME, UKCOUNTRY, " +
                "ETL_UPDATEDATE_CUST_INFO_EARLY) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setString(4, record.getEmailAddress());
            // do not set player Id here. according to CT-3451 it shouldn't be needed.
            pst.setObject(5, null);
            pst.setString(6, record.getSportsbookCustId());
            pst.setString(7, record.getBingoVFUsername());
            pst.setString(8, record.getUsername());
            pst.setString(9, record.getMobileNumber());
            pst.setString(10, record.getHomeNumber());
            pst.setString(11, record.getFirstName());
            pst.setString(12, record.getLastName());
            pst.setString(13, record.getAddress1());
            pst.setString(14, record.getCity());
            pst.setString(15, record.getPostcode());
            // do not set brand here. according to CT-3451 it shouldn't be needed.
            pst.setString(16, null);
            pst.setString(17, record.getCountry());
            pst.setString(18, record.getGender());
            pst.setTimestamp(19, record.getDOB());
            pst.setString(20, record.getBettingCurrency());
            pst.setString(21, record.getIMSProfileId());
            pst.setString(22, record.getSignupProdVertical());
            pst.setTimestamp(23, record.getRegDateTime());
            pst.setString(24, record.getUkCountry());
            pst.setTimestamp(25, record.getETL_UPDATEDATE_CUST_INFO_EARLY());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

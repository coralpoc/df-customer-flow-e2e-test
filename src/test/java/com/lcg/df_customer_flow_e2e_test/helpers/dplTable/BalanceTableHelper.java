package com.lcg.df_customer_flow_e2e_test.helpers.dplTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.lcg.df_customer_flow_e2e_test.pojos.dpl.BalanceRecord;

public class BalanceTableHelper extends BaseTableHelper {

    public void insertPlayerRecord(BalanceRecord record) {

        String stm = "INSERT INTO DPL.SFMC_BALANCE (CUSTOMER_SEQ, BRAND_SEQ, CUSTOMER_ID, BONUSBALANCE, CASHBALANCE, BALANCEUPDATETIME," +
                " COMPPOINTS, BUZZPOINTSBALANCE, ETL_UPDATE_DATE) VALUES" +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (
                Connection conn = getDbConnection();
                PreparedStatement pst = conn.prepareStatement(stm)
        ) {
            conn.setAutoCommit(false);

            pst.setInt(1, record.getCUSTOMER_SEQ());
            pst.setInt(2, record.getBRAND_SEQ());
            pst.setInt(3, record.getCUSTOMER_ID());
            pst.setBigDecimal(4, record.getBonusBalance());
            pst.setBigDecimal(5, record.getCashBalance());
            pst.setTimestamp(6, record.getBalanceUpdateTime());
            pst.setBigDecimal(7, record.getCompPoints());
            pst.setBigDecimal(8, record.getBuzzPointsBalance());
            pst.setTimestamp(9, record.getETL_UPDATE_DATE());

            pst.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException("Error inserting record into the database", e);
        }
    }
}

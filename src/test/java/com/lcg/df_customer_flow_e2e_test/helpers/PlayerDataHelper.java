package com.lcg.df_customer_flow_e2e_test.helpers;

import static com.lcg.df_customer_flow_e2e_test.helpers.filehelper.FileHelper.*;
import static com.lcg.df_customer_flow_e2e_test.utils.CommonUtil.getBrandSeq;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.lcg.df_customer_flow_e2e_test.helpers.dplTable.CompleteTableHelper;
import com.lcg.df_customer_flow_e2e_test.pojos.Player;
import com.lcg.df_customer_flow_e2e_test.pojos.dpl.CompletePlayerRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlayerDataHelper {

    public static final String BRAND_SEQ_COLUMN = "brand_seq";
    private static final String TABLE_COLUMN_NAME = "table";
    private static final String COLUMN_COLUMN_NAME = "column";
    private static final String VALUE_COLUMN_NAME = "value";

    private CompleteTableHelper completeTableHelper;

    public static List<Player> loadAllPlayerDataForFeature(String featureName) throws IOException {
        String featureRelativePath = gettDataPathForFeature(featureName);
        PathMatchingResourcePatternResolver loader = new PathMatchingResourcePatternResolver();
        Resource playerDirs[] = loader.getResources(getPlayerDirsResourceFilter(featureRelativePath));
        List<Player> players = Arrays.stream(playerDirs).map(r -> getPlayerData(loader, featureRelativePath, r)).collect(Collectors.toList());
        return players;
    }

    public void insertListOfPlayers(List<Player> listOfPlayers) {
        listOfPlayers.stream().forEach(this::insertPlayerDataRows);
    }

    public void insertPlayerDataRows(Player player) {
        Integer brandSeq = null;
        CompletePlayerRecord completePlayerRecord = null;
        for (Map<String, String> data : player.getDataToInsert()) {
            brandSeq = getBrandSeq(data);

            if (data.get("column").equals("crmAccountStatusLastUpdated") && data.get("value") != null && data.get("value").startsWith("NOW-")) {
                int dateOffset = Integer.valueOf(data.get("value").substring(data.get("value").indexOf("-")));
                data.put("value", getTimestampMinusDays(-dateOffset));
            }

            if (data.get("column").equals("autoSuppressLastUpdated") && data.get("value") != null && data.get("value").startsWith("NOW-")) {
                int dateOffset = Integer.valueOf(data.get("value").substring(data.get("value").indexOf("-")));
                data.put("value", getTimestampMinusDays(-dateOffset));
            }

            if (completePlayerRecord == null) {
                completePlayerRecord = new CompletePlayerRecord(player.getCustomerSeq(), player.getCustomerId(), brandSeq);
            }
            completePlayerRecord.setPlayerData(data.get(TABLE_COLUMN_NAME), data.get(COLUMN_COLUMN_NAME), data.get(VALUE_COLUMN_NAME));
        }
        completeTableHelper = new CompleteTableHelper(completePlayerRecord);
        log.debug("Inserting {} player into DB", player.getCustomerId());
        completeTableHelper.insertAllPlayerRecords();
        log.info("Player {} from folder - {} inserted into DB", player.getCustomerId(),player.getFolderName());

    }

    public void deletePlayerFromAllTables(Integer customerId) {
        completeTableHelper.deletePlayerFromAllDPLTables(customerId);
        //completeTableHelper.deletePlayerFromAllSDWTables(customerId);
    }

    private String getTimestampMinusDays(int days) {
        return String.valueOf(new Timestamp(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * days)));
    }
}

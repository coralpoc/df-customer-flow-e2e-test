package com.lcg.df_customer_flow_e2e_test.helpers.filehelper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.lcg.df_customer_flow_e2e_test.pojos.Player;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileHelper {

    private static final String PLAYER_DATA_ROOT_PATH = "player_data";

    public static String gettDataPathForFeature(String featureName) {
        return PLAYER_DATA_ROOT_PATH + "/" + featureName;
    }

    public static Player getPlayerData(PathMatchingResourcePatternResolver loader, String featureRelativePath, Resource playerDir) {

        Player player = new Player();
        try {
        	player.setFolderName(playerDir.getFile().getName());
        	
            String playerRelativePathWithinFeature = featureRelativePath + "/" + playerDir.getFile().getName();
            Resource playerInsertionsResourcesWithinFeature[] = loader.getResources(getInsertionsResourceFilter(playerRelativePathWithinFeature));
            Arrays.stream(playerInsertionsResourcesWithinFeature).forEach(r -> loadInsertionsIntoPlayer(player, r));

            Resource playerAssertionsResourcesWithinFeature[] = loader.getResources(getAssertionsResourceFilter(playerRelativePathWithinFeature));
            Arrays.stream(playerAssertionsResourcesWithinFeature).forEach(r -> loadAssertionsIntoPlayer(player, r));
        } catch(IOException e) {
            log.error(e.getMessage());
        }
        log.debug("Player details in folder - {} is now assigned customer id - {}",player.getFolderName(), player.getCustomerId());
        
        return player;
    }

    public static String getPlayerDirsResourceFilter(String classpathRelativePath) {
        return "classpath:/" + classpathRelativePath + File.separator + "player*";
    }

    private static String getAssertionsResourceFilter(String classpathRelativePath) {
        return "classpath:/" + classpathRelativePath + File.separator + "*assertions.txt";
    }

    private static String getInsertionsResourceFilter(String classpathRelativePath) {
        return "classpath:/" + classpathRelativePath + File.separator + "*insertions.txt";
    }

    private static Player loadAssertionsIntoPlayer(Player player, Resource resource) {
        logResource("Loading assertions from {}", resource);
        List<Map<String,String>> dataToAssert = getRowMapsFromResource(resource);
        player.setDataToAssert(dataToAssert);
        return player;
    }

    private static Player loadInsertionsIntoPlayer(Player player, Resource resource) {
        logResource("Loading insertions from {}", resource);
        List<Map<String,String>> dataToInsert = getRowMapsFromResource(resource);
        player.setDataToInsert(dataToInsert);
        return player;
    }

    private static List<Map<String,String>> getRowMapsFromResource(Resource resource) {
        List<Map<String,String>> rowMaps = new ArrayList<>();

        try {
            List<String> dataLines = getDataLines(resource);
            List<String> headings = getColumnHeadings(dataLines);

            getDataLinesWithoutHeadings(dataLines).stream()
                    .filter(l -> l.trim().length()>0)
                    .forEach(dataLine -> {
                        rowMaps.add(getRowMap(dataLine, headings));
                    });
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return rowMaps;
    }

    private static List<String> getDataLinesWithoutHeadings(List<String> dataLines) {
        return dataLines.subList(1,dataLines.size());
    }

    private static List<String> getColumnHeadings(List<String> dataLines) {
        String[] headings = dataLines.get(0).trim().split("\\|");
        List<String> sanitisedHeadings = Arrays.stream(headings).map(h -> sanitised(h)).collect(Collectors.toList());
        log.debug(sanitisedHeadings.stream().collect(Collectors.joining(", ")));
        return sanitisedHeadings;
    }

    private static Map<String, String> getRowMap(String dataLine, List<String> headings) {
        Map<String, String> rowMap = new HashMap<>();
        String columns[] = dataLine.split("\\|");
        IntStream.range(0, headings.size()).boxed().forEach(i -> rowMap.put(headings.get(i), sanitised(columns[i])));
        return rowMap;
    }

    private static List<String> getDataLines(Resource resource) throws IOException {
        List<String> dataLines = Files.readAllLines(Paths.get(resource.getURI()), StandardCharsets.UTF_8)
                .stream().map(l -> stripEndPipes(l)).map(l -> l.trim()).collect(Collectors.toList());
        log.debug("Read {} lines", dataLines.size());
        return dataLines;
    }

    private static String stripEndPipes(String cucumberFormatDataLine) {
        return cucumberFormatDataLine.substring(cucumberFormatDataLine.indexOf("|")+1,
                cucumberFormatDataLine.lastIndexOf("|"));
    }

    private static String sanitised(String value) {
        return value == null ? null : value.trim();
    }

    private static void logResource(String messageTemplate, Resource resource) {
        try {
            String resourcePath = resource.getFile().getAbsolutePath();
            log.debug(messageTemplate, resourcePath);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}

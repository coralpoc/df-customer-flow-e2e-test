package com.lcg.df_customer_flow_e2e_test.config;


public class DerDbConfig {

    public static String getUrlFor(String database) {
        String configEntry = String.format("der.%s.db.url", database);
        return EnvConfig.getProperty(configEntry);
    }

    public static String getUserFor(String database) {
        String configEntry = String.format("der.%s.db.user", database);
        return EnvConfig.getProperty(configEntry);
    }

    public static String getPasswordFor(String database) {
        String configEntry = String.format("der.%s.db.password", database);
        return EnvConfig.getProperty(configEntry);
    }
}

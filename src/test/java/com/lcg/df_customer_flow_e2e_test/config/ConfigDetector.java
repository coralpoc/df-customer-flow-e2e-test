package com.lcg.df_customer_flow_e2e_test.config;

import java.util.ArrayList;
import java.util.List;

public class ConfigDetector {

    public static String getEnv() {
        String env = ((null != System.getProperty("env.name")) ? System.getProperty("env.name") : "dev").toLowerCase();
        checkIfWeCanRunTestsInThisEnv(env);
        return env;
    }

    public static String getFrom() {
        return ((null != System.getProperty("from")) ? System.getProperty("from") : "local").toLowerCase();
    }

    public static String getDerDataBase() {
        return ((null != System.getProperty("database")) ? System.getProperty("database") : "coral").toLowerCase();
    }

    public static void checkIfWeCanRunTestsInThisEnv(String env) {
        if (!Boolean.parseBoolean(System.getProperty("skip.env.check"))) {
            List<String> whitelistedEnvs = new ArrayList<>();
            whitelistedEnvs.add("inmemory");
            whitelistedEnvs.add("dev");
            whitelistedEnvs.add("sit");

            if (!whitelistedEnvs.contains(env)) {
                String message = String.format("Envs where we can always run these tests are %s.\n" +
                        "If you are sure you want to run this test in '%s' env, please specify '-Dskip.env.check=true' option.", whitelistedEnvs.toString(), env);
                throw new RuntimeException(message);
            }
        }
    }
}

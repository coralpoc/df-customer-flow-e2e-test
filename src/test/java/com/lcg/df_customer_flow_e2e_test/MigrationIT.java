package com.lcg.df_customer_flow_e2e_test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/com/lcg/df_customer_flow_e2e_test/features/dpl",
		tags = "@migration",
		plugin = {"pretty","html:target/cucumber", "json:target/cucumber-migration.json","junit:target/cucumber-migration.xml" }
		)
public class MigrationIT {
}
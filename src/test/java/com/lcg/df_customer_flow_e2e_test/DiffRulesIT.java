package com.lcg.df_customer_flow_e2e_test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/com/lcg/df_customer_flow_e2e_test/features/dpl",
		tags = "@diff",
		plugin = {"pretty","html:target/cucumber", "json:target/cucumber-diff-rule.json","junit:target/cucumber-diff-rule.xml" }
		)
public class DiffRulesIT {
}

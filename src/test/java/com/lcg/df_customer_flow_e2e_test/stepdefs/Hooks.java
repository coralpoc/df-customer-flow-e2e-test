package com.lcg.df_customer_flow_e2e_test.stepdefs;

import static com.lcg.df_customer_flow_e2e_test.utils.CommonUtil.getSalesforceHandler;

import com.lcg.df_customer_flow_e2e_test.helpers.PlayerDataHelper;
import com.lcg.df_customer_flow_e2e_test.pojos.CommonState;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Hooks {
	
	
	@Before(order = 0)
	public void beforeScenarioStart(Scenario scenario) {

		log.info("---------- Start of new Scenarion: " + scenario.getName() + "----------");
	}
	
	@After(order = 0)
	public void afterScenarioStart(Scenario scenario) {
		log.info("---------- End of Scenarion: Run Status - " + scenario.getStatus().name());
	}
	
	@After(order = 1)
    public void logSfResponseOnFailure(Scenario scenario) {
        if (scenario.isFailed()) {
            getSalesforceHandler().logResponseOnFailure();
        }
    }

    /*@After(order = 1,value="@delete-from-db" )
    public void afterIterationDeleteFromDb() {
        CommonState.getCommonState().getPlayers().forEach(p -> playerDataHelper.deletePlayerFromAllTables(p.getCustomerId()));
    }*/

    @After(order = 2,value ="@delete-from-sf")
    public void afterIterationDeleteFromSf() {
        getSalesforceHandler().deleteSalesforcePlayers();
    }

}

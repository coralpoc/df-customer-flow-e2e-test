package com.lcg.df_customer_flow_e2e_test.stepdefs;

import static com.lcg.df_customer_flow_e2e_test.helpers.PlayerDataHelper.loadAllPlayerDataForFeature;
import static com.lcg.df_customer_flow_e2e_test.utils.CommonUtil.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.lcg.df_customer_flow_e2e_test.helpers.PlayerDataHelper;
import com.lcg.df_customer_flow_e2e_test.helpers.aws.AWSServiceHelper;
import com.lcg.df_customer_flow_e2e_test.helpers.dbHelper.DplDataAvailabilityHelper;
import com.lcg.df_customer_flow_e2e_test.helpers.dbHelper.DplExtractionStatusRepository;
import com.lcg.df_customer_flow_e2e_test.helpers.dbHelper.RetailDataAvailabilityHelper;
import com.lcg.df_customer_flow_e2e_test.helpers.trigger.DplExtractorLambda;
import com.lcg.df_customer_flow_e2e_test.pojos.CommonState;
import com.lcg.df_customer_flow_e2e_test.pojos.Player;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DPLStepdefs{
	
	private PlayerDataHelper playerDataHelper;
    private DplDataAvailabilityHelper dplDataAvailabilityHelper;
    private RetailDataAvailabilityHelper retailDataAvailabilityHelper;
    private DplExtractionStatusRepository dplExtractionStatusRepository;
    private List<String> criticalViews = Arrays.asList("SFMC_CUSTOMER_INFO_EARLY", "SFMC_CUSTOMER_INFO_LIFE", "SFMC_STATUS");
    private List<String> nonCriticalViews = Arrays.asList("SFMC_PREDICTIONS", "SFMC_SEGMENTATIONS");
    private List<String> medCriticalViews = Arrays.asList("SFMC_BALANCE", "SFMC_DEPOSIT_EARLY", "SFMC_DEPOSIT_LIFE", "SFMC_LOGIN", "SFMC_DEPOSIT_RELATIVE");
    private List<String> lowCriticalViews = Arrays.asList("SFMC_BET_EARLY", "SFMC_BET_LIFE");
    private AWSServiceHelper aswService;
    
    public DPLStepdefs() {
    	
    	CommonState.getCommonState().resetSalesforceHandler();

        playerDataHelper = new PlayerDataHelper();
        dplDataAvailabilityHelper = new DplDataAvailabilityHelper();
        retailDataAvailabilityHelper = new RetailDataAvailabilityHelper();
        dplExtractionStatusRepository = new DplExtractionStatusRepository();
        aswService = new AWSServiceHelper();
    
    }
    
    @Given("we insert the data only into the following tables")
    public void we_insert_data_into_tables(List<String> tables){
        resetTables();
        for(String table: tables) {
        	addTable(table);
        }

    }
    
    @Given("players are inserted into database for feature {string}")
    public void players_are_inserted_into_db_for_feature(String featureName)  {
        try {
            List<Player> players = loadAllPlayerDataForFeature(featureName);
            CommonState.getCommonState().setPlayers(players);
            playerDataHelper.insertListOfPlayers(players);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }
    
    @When("the migration is kicked off")
    public void when_migration_kicked_off() {
    	new DplExtractorLambda().invokeMigration();
    }
    
    @When("we wait {int} minutes")
    public void wait_for_some_time(Integer waitTime) {
        int minutesToWait = waitTime * 60;
        waitForEmrClusterToExtractTheData(minutesToWait);
    }
    
    @Then("the players are available in SF for {string}")
    public void players_available_in_SF(String extractionType)  {

        CommonState.getCommonState().resetSalesforceHandler();
        CommonState.getCommonState().getPlayers().forEach(p -> {
            if (p.getDataToAssert().size() > 0) {
                verifyExpectedPlayerDataIsInSalesforce(p, extractionType, null);
            }
        });
    }

    @Given("clean the existing data availability info")
    public void clean_existing_availability_in_DB(){
        dplDataAvailabilityHelper.cleanupExistingAvailabilityInfo();
    }
    
    @Given("dynamo db status table cleaned up")
    public void dynamodb_status_table_clear() {
    	cleanupInternalDynamoDbTable();
    }
    
    @Given("dpl load status is set to available to start from {int} min ago")
    public void dpl_data_available_for_diff_start(Integer mins,List<String> tables) {
    	setDplDataToAvailable(mins,tables);
    }
    
    @And("table names should {string} in view status in DDB")
    public void check_view_exist_in_status_table(String condition,List<String> tableNames) {
    	boolean existFlag = true;
    	if(condition.startsWith("NOT")) {
    		existFlag = false;
    	}
    	
    	
    	for(String table: tableNames) {
    		boolean flag = dplExtractionStatusRepository.checkColumnExistInStatusTable(table,"extractionStatus");
    		String errorMsg = String.format("Expects %s for table name - %s in status table",condition,table);
    		if(existFlag != flag) {
    			assertThat(errorMsg,existFlag == flag);
    		}
    	}
    	
    }
    
    
    @Then("all {string} views status should be {string} in DDB")
    public void confirm_view_status_in_DDB(String viewsType, String status) {
        if (viewsType.equals("critical")) {
            criticalViews.forEach(view -> {
                String actualStatus = dplExtractionStatusRepository.getValueOfColumnInStatusTable(view, "extractionStatus");
                String errorMessage = String.format("Unexpected status %s for view %s", actualStatus, view);
                assertThat(errorMessage, actualStatus.equals(status));
            });
        }
        if (viewsType.equals("non critical")) {
            nonCriticalViews.forEach(view -> {
                String actualStatus = dplExtractionStatusRepository.getValueOfColumnInStatusTable(view, "extractionStatus");
                String errorMessage = String.format("Unexpected status %s for view %s", actualStatus, view);
                assertThat(errorMessage, actualStatus.equals(status));
            });
        }
        if (viewsType.equals("medium critical")) {
            medCriticalViews.forEach(view -> {
                String actualStatus = dplExtractionStatusRepository.getValueOfColumnInStatusTable(view, "extractionStatus");
                String errorMessage = String.format("Unexpected status %s for view %s", actualStatus, view);
                assertThat(errorMessage, actualStatus.equals(status));
            });
        }
        if (viewsType.equals("low critical")) {
            lowCriticalViews.forEach(view -> {
                String actualStatus = dplExtractionStatusRepository.getValueOfColumnInStatusTable(view, "extractionStatus");
                String errorMessage = String.format("Unexpected status %s for view %s", actualStatus, view);
                assertThat(errorMessage, actualStatus.equals(status));
            });
        }
    }
    
    @Then("the players are available in SF for {string} and batch number {string}")
    public void confirm_players_available_in_SF_and_batch_number_correct (String extractionType, String batchNumber) {
        CommonState.getCommonState().resetSalesforceHandler();
        CommonState.getCommonState().getPlayers().forEach(p -> {
            if (p.getDataToAssert().size() > 0) {
                verifyExpectedPlayerDataIsInSalesforce(p, extractionType, batchNumber);
            }
        });
    }

    /*@Then("the players are available in SF for {string}")
    public void confirm_players_available_in_SF(String extractionType) {
        CommonState.getCommonState().resetSalesforceHandler();
        CommonState.getCommonState().getPlayers().forEach(p -> {
            if (p.getDataToAssert().size() > 0) {
                verifyExpectedPlayerDataIsInSalesforce(p, extractionType, null);
            }
        });
    }*/
    
    @And("the joining end time set to {int} minutes ago for tables:")
    public void set_joining_end_time_for_tables(int minutes, List<String> tables){
    	for(String table: tables) {
    		dplExtractionStatusRepository.setJoiningEndTime(table, (long)minutes);
    	}
    }
    
    @And("set dpl_deadline to {int} hour ahead")
    public void set_dpl_deadline_ahead(int hour) {
    	
    	aswService.updateLambdaDeadLine(hour);
    }
    
    @And("trigger lambda for {string}")
    public void trigger_labda_for_operation(String name) {
    	aswService.simulateTriggerlambda(name);
    }
    
    private void setDplDataToAvailable(Integer mins,List<String> tables) {
        dplDataAvailabilityHelper.setDataAvailabilityToTrue(tables, mins);
    }
    
    private void cleanupInternalDynamoDbTable() {
        DplExtractionStatusRepository dplExtractionStatusRepository = new DplExtractionStatusRepository();
        dplExtractionStatusRepository.deleteAllItems();
    }
    
    private void addTable(String tableName) {
        CommonState.getCommonState().addTable(tableName);
    }
    
    private void resetTables() {
        CommonState.getCommonState().resetTables();
    }
    
    //@After
    public void logSfResponseOnFailure(Scenario scenario) {
        if (scenario.isFailed()) {
            getSalesforceHandler().logResponseOnFailure();
        }
    }

    //@After("@delete-from-db")
    public void afterIterationDeleteFromDb() {
        CommonState.getCommonState().getPlayers().forEach(p -> playerDataHelper.deletePlayerFromAllTables(p.getCustomerId()));
    }

    //@After("@delete-from-sf")
    public void afterIterationDeleteFromSf() {
        getSalesforceHandler().deleteSalesforcePlayers();
    }
   

}

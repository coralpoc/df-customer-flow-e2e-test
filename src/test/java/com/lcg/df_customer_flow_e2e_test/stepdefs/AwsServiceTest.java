package com.lcg.df_customer_flow_e2e_test.stepdefs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.Environment;
import com.amazonaws.services.lambda.model.EnvironmentResponse;
import com.amazonaws.services.lambda.model.GetFunctionConfigurationRequest;
import com.amazonaws.services.lambda.model.GetFunctionConfigurationResult;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.UpdateFunctionConfigurationRequest;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClientBuilder;
import com.amazonaws.services.elasticmapreduce.model.ClusterState;
import com.amazonaws.services.elasticmapreduce.model.ClusterSummary;
import com.amazonaws.services.elasticmapreduce.model.ListClustersRequest;
import com.amazonaws.services.elasticmapreduce.model.ListClustersResult;

import junit.framework.Assert;
@Ignore
public class AwsServiceTest {
	
	@Test
	public void triggerAws() {
		
		AWSLambdaClientBuilder awsClientBuilder = AWSLambdaClientBuilder.standard();
		AWSLambda awsLambda = awsClientBuilder.defaultClient();

		GetFunctionConfigurationResult response = awsLambda.getFunctionConfiguration(new GetFunctionConfigurationRequest().withFunctionName("df-customer-dpl-extractor"));

		EnvironmentResponse envResponse = response.getEnvironment(); //addVariablesEntry("DPL_DEADLINE", "16:30");
		
		java.util.Map<String, String> updateVariable = envResponse.getVariables();
		updateVariable.put("DPL_DEADLINE", "17:00");
		
		//envResponse.setVariables(updateVariable);
		
		 UpdateFunctionConfigurationRequest expected = new UpdateFunctionConfigurationRequest()
	                .withFunctionName(response.getFunctionName())
	                .withEnvironment(new Environment().withVariables(updateVariable));
		
		awsLambda.updateFunctionConfiguration(expected);
		

		InvokeRequest invokeRequest = new InvokeRequest()
                .withFunctionName("df-customer-dpl-extractor")
                //.withPayload("{}")
                .withPayload("{\"extractionType\":\"MIGRATION\"}")
                .withInvocationType(InvocationType.Event);
		
		InvokeResult result = awsLambda.invoke(invokeRequest);
		
		assertNotNull(result);
		assertEquals("Status code for successful event type is 202",result.getStatusCode(), Integer.valueOf(202));
		
	}
	
	//@Test
	public void checkEmrInstance() {
		
		AmazonElasticMapReduceClientBuilder amazonEMRBuilder = AmazonElasticMapReduceClientBuilder.standard();
		AmazonElasticMapReduce emr = AmazonElasticMapReduceClientBuilder.defaultClient();
		
		ListClustersRequest clusterRequest = new ListClustersRequest();
		Date now = new Date();
		System.out.println("Get cluster from before date - " + now);
		//clusterRequest.setCreatedBefore(now );
		clusterRequest.withClusterStates(ClusterState.WAITING,ClusterState.BOOTSTRAPPING,ClusterState.STARTING);
		
		ListClustersResult result = emr.listClusters(clusterRequest);
		
		List<ClusterSummary> clusterList = result.getClusters();
		
		assertNotNull(result);

		
	}

}

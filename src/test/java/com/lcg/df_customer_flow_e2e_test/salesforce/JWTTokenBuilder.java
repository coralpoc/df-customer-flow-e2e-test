package com.lcg.df_customer_flow_e2e_test.salesforce;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.MessageFormat;
import java.util.Base64;

/**
 * Builder class to create a JWTToken based on the provided params
 */
public class JWTTokenBuilder {

    private static final String HEADER = "{\"alg\":\"RS256\"}";
    private static final String CLAIM_TEMPLATE = "'{'\"iss\": \"{0}\", \"sub\": \"{1}\", \"aud\": \"{2}\", \"exp\": \"{3}\"'}'";

    private String clientId;
    private String username;
    private String url;
    private String keystorePass;
    private String keystorePath;
    private String keystoreAlias;

    /**
     * Creates a new JWTTokenBuilder Object
     *
     * @return new empty JWTTokenBuilder
     */
    public static JWTTokenBuilder create() {
        return new JWTTokenBuilder();
    }

    /**
     * Add a client id to the builder
     *
     * @param clientId of the salesforce instance
     * @return JWTTokenBuilder with populated client Id
     */
    public JWTTokenBuilder clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    /**
     * Add an username to the builder
     *
     * @param username of the user
     * @return JWTTokenBuilder with populated client Id
     */
    public JWTTokenBuilder username(String username) {
        this.username = username;
        return this;
    }

    public JWTTokenBuilder url(String url) {
        this.url = url;
        return this;
    }

    public JWTTokenBuilder keystorePass(String keystorePass) {
        this.keystorePass = keystorePass;
        return this;
    }

    public JWTTokenBuilder keystorePath(String keystorePath) {
        this.keystorePath = keystorePath;
        return this;
    }

    public JWTTokenBuilder keystoreAlias(String keystoreAlias) {
        this.keystoreAlias = keystoreAlias;
        return this;
    }

    public String build() throws
            IOException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnrecoverableKeyException,
            CertificateException, KeyStoreException {

        StringBuffer token = new StringBuffer();
        token.append(Base64.getUrlEncoder().encodeToString(HEADER.getBytes()));
        token.append(".");

        String[] claimArray = new String[4];
        claimArray[0] = clientId;
        claimArray[1] = username;
        claimArray[2] = url;
        claimArray[3] = Long.toString((System.currentTimeMillis() / 1000) + 300);

        MessageFormat claims;
        claims = new MessageFormat(CLAIM_TEMPLATE);
        String payload = claims.format(claimArray);

        token.append(Base64.getUrlEncoder().encodeToString(payload.getBytes("UTF-8")));

        KeyStore keystore = KeyStore.getInstance("JKS");
        keystore.load(new FileInputStream(keystorePath), keystorePass.toCharArray());

        PrivateKey privateKey = (PrivateKey) keystore.getKey(keystoreAlias, keystorePass.toCharArray());

        //Sign the JWT Header + "." + JWT Claims Object
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        signature.update(token.toString().getBytes("UTF-8"));

        String signedPayload = Base64.getUrlEncoder().encodeToString(signature.sign());

        //Separate with a period
        token.append(".");

        //Add the encoded signature
        token.append(signedPayload);

        return token.toString();
    }
}

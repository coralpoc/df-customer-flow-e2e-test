package com.lcg.df_customer_flow_e2e_test.salesforce;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

@Slf4j
public class SalesforceService {

    @Getter
    private String accessToken;

    @Getter
    private String instanceURL;

    private String url;
    private String jwtToken;

    public static SalesforceService create() {
        return new SalesforceService();
    }

    public SalesforceService url(String url) {
        this.url = url;
        return this;
    }

    public SalesforceService jwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
        return this;
    }

    public SalesforceService authenticate() {
        HttpURLConnection connection = null;

        try {
            //Create connection

            String data =
                    "grant_type=" + URLEncoder.encode("urn:ietf:params:oauth:grant-type:jwt-bearer", "UTF-8") +
                            "&assertion=" + jwtToken;

            URL url = new URL(this.url + "/services/oauth2/token");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(data.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(data);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuffer response = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            JSONObject json = new JSONObject(response.toString());
            this.accessToken = (String) json.get("access_token");
            this.instanceURL = (String) json.get("instance_url");
            log.info("SF instance: {}\n", instanceURL);

        } catch (Exception e) {
            e.printStackTrace();
            closeConnection(connection);
            String errorMessage = String.format("Error calling SF: %s", e.getMessage());
            throw new RuntimeException(errorMessage);
        } finally {
            closeConnection(connection);
        }

        return this;
    }

    private void closeConnection(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }
}

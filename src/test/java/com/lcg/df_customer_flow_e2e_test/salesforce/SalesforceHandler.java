package com.lcg.df_customer_flow_e2e_test.salesforce;

import static com.lcg.df_customer_flow_e2e_test.utils.CommonUtil.getSalesforceClient;
import static java.util.Objects.isNull;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.googlecode.junittoolbox.PollingWait;

import io.restassured.response.Response;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class SalesforceHandler {

    private static final int MIN_CALLS_TO_SALES_BEFORE_CONFIDENT_ALL_DATA_RETRIEVED = 15;
    private static final String SALESFOCE_RESPONSE_COLUMN_ID = "Id";
    private static final String SALESFORCE_RESPONSE_COLUMN_PLAYER_ID = "Player_Id__c";
    private static final int SALEFORCE_REQUEST_MAX_WAIT_TIME = 120;

    private Integer statusCode;
    private JSONObject json;
    private Set<String> internalSfPlayerIds;
    private int callsToSalesforce = 0;

    public SalesforceHandler() {
        internalSfPlayerIds = new HashSet<>();
    }

    public void resetPolling() {
        callsToSalesforce = 0;
    }

    public boolean isPlayerInSf() {
        return statusCode == 200;
    }

    public void verifyExpectedDataIsInSalesforce(Integer customerId, String targetCustomerIdentifier, Map<String, String> apiNamesAndValues) {
        String apiName = apiNamesAndValues.get("ApiName");
        String expected = apiNamesAndValues.get("Value");
        if (callsToSalesforce < MIN_CALLS_TO_SALES_BEFORE_CONFIDENT_ALL_DATA_RETRIEVED) {
            log.info("Polling salesforce for latest data on targetCustomerIdentifier {}", targetCustomerIdentifier);
            checkPlayerInSFByPolling(customerId, targetCustomerIdentifier, apiName, expected);
            callsToSalesforce++;
        } else {
            log.info("Checking against locally cached SF response for customerId {} apiName {}, expected {}",
                    customerId, apiName, expected);
            checkValue(customerId, apiName, expected);
        }
    }

    public void checkPlayerInSFByPolling(Integer customerId, String targetCustomerIdentifier, String apiName, String expected) {
        PollingWait wait = new PollingWait().timeoutAfter(SALEFORCE_REQUEST_MAX_WAIT_TIME, SECONDS).pollEvery(2, SECONDS);
        wait.until(() -> {
            log.info("Fetching user from SF API: {}", targetCustomerIdentifier);

            Response resp = getSalesforceClient().getPlayer(targetCustomerIdentifier);
            statusCode = resp.statusCode();
            if (isPlayerInSf()) {
                json = new JSONObject(resp.getBody().asString());
            }

            if (!isPlayerInSf()) {
                log.info("Player not found in SF for targetPlayerId: {}", targetCustomerIdentifier);
                return false;
            } else if (isPlayerInSf() && !isExpectedValueInSf(customerId, apiName, expected)) {
                log.info("Player found in SF, field: {} with value: {} but expected: {} for customerId: {} with targetPlayerId: {}",
                		apiName,getJsonValue(customerId, apiName), expected, customerId, targetCustomerIdentifier);
                return false;
            } else {
                return true;
            }
        });
        checkPlayerId(customerId);
        checkValue(customerId, apiName, expected);
    }

    public void logResponseOnFailure() {
        if (json != null) {
            log.error("JSON for the failed scenario:{}", json.toString(4));
        } else {
            log.error("Player not found in SF for the failed scenario");
        }
    }

    public void deleteSalesforcePlayers() {
        internalSfPlayerIds.stream().forEach(p -> {
            Response resp = getSalesforceClient().deletePlayer(p);
            log.info("Player with internalSalesforceId {} deleted from SF", p);
            log.info("SF deletion status code: {}", resp.statusCode());
            log.info("SF deletion response: {}", resp.getBody().asString());
        });
    }

    private boolean isExpectedValueInSf(Integer customerId, String apiName, String expected) {
        String JsonValue = getJsonValue(customerId, apiName);
        if (apiName.equalsIgnoreCase("DF_ExtractionDate__c")) {
            String dfExractionDateValue = JsonValue.substring(0, JsonValue.indexOf("T"));
            return dfExractionDateValue.equals(expected);
        } else {
            return JsonValue.equals(expected);
        }
    }

    private void checkPlayerId(Integer customerId) {
        log.debug("Checking Player ID");
        assertThat(getJsonValue(customerId, SALESFORCE_RESPONSE_COLUMN_PLAYER_ID), is(String.valueOf(customerId)));
    }

    private void checkValue(Integer customerId, String apiName, String expected) {
        String internalSfPlayerId = getJsonValue(customerId, SALESFOCE_RESPONSE_COLUMN_ID);
        internalSfPlayerIds.add(internalSfPlayerId);

        log.debug("Response has internalSfPlayerId {}", internalSfPlayerId);
        log.info("API name: {}", apiName);
        log.info("Expected value: {}", expected);

        String apiValue = getJsonValue(customerId, apiName);
        log.info("Actual value: {}", apiValue);
        if (apiName.equalsIgnoreCase("DF_ExtractionDate__c") && !isNull(apiValue)) {
            apiValue = apiValue.substring(0, apiValue.indexOf("T"));
            // Convert exponential values (target value) in a String into a decimal representation (source value) without exponential notation
        } else if (apiName.equalsIgnoreCase("pHarmScore__c") && !isNull(apiValue) && apiValue.contains("E")) {
            String patternStr = "[0-9.-]*E[0-9.-]*";
            String ExponentialTargetValue = apiValue;
            Pattern pattern = Pattern.compile(patternStr);
            Matcher matcher = pattern.matcher(ExponentialTargetValue);
            if (matcher.find()) {
                Double d = Double.valueOf(matcher.group());
                apiValue = ExponentialTargetValue.replaceAll(patternStr, BigDecimal.valueOf(d).toPlainString());
            } else {
                String errorMessage = String.format("No match find for the value %s with search pattern %s", ExponentialTargetValue, patternStr);
                throw new RuntimeException(errorMessage);
            }
        }
        assertThat(apiValue, equalTo(expected));
    }

    private String getJsonValue(Integer customerId, String apiName) {
        Object obj = json.get(apiName);
        if (obj instanceof Double) {
            // if it's an int, return an int
            if (((Double) obj % 1) == 0) {
                return Integer.toString(((Double) obj).intValue());
            } else {
                // otherwise return double
                return Double.toString((Double) obj);
            }
        } else if (obj instanceof String || isNull(apiName) || obj instanceof Boolean || obj.toString().equals("null")) {
            return obj.toString();
        } else {
            String errorMessage = String.format("Unexpected data type returned by SF API for PlayerId %s, field %s", customerId, apiName);
            throw new RuntimeException(errorMessage);
        }
    }
}

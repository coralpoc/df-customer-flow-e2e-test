package com.lcg.df_customer_flow_e2e_test.salesforce;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import com.lcg.df_customer_flow_e2e_test.config.EnvConfig;

import static io.restassured.RestAssured.given;

@Slf4j
public class SalesforceClient {

    private static final String CONSUMER_KEY = EnvConfig.getProperty("sf.api.key");
    private static final String SALESFORCE_URL = EnvConfig.getProperty("sf.api.url");
    private static final String CLAIM_EMAIL = EnvConfig.getProperty("sf.api.user");

    private static final String KEYSTORE_PASS = EnvConfig.getProperty("sf.api.keystore.password");
    private static final String KEYSTORE_PATH = EnvConfig.getProperty("sf.api.keystore.path");
    private static final String KEYSTORE_ALIAS = EnvConfig.getProperty("sf.api.keystore.alias");

    private static final String TOKEN;
    private static final SalesforceService SERVICE;

    private static final RequestSpecification reqSpec;

    static {
        try {
            TOKEN = JWTTokenBuilder
                    .create()
                    .clientId(CONSUMER_KEY)
                    .url(SALESFORCE_URL)
                    .username(CLAIM_EMAIL)
                    .keystorePass(KEYSTORE_PASS)
                    .keystorePath(KEYSTORE_PATH)
                    .keystoreAlias(KEYSTORE_ALIAS)
                    .build();

            SERVICE = SalesforceService
                    .create()
                    .url(SALESFORCE_URL)
                    .jwtToken(TOKEN)
                    .authenticate();

            log.info("**** Generated Token for SF connection -> " + TOKEN);
            
            reqSpec = new RequestSpecBuilder().setBaseUri(SERVICE.getInstanceURL()).build();

        } catch (Exception e) {
            throw new RuntimeException("Error creating SalesforceService", e);
        }
    }

    public Response upsertPlayer(String customerIdentifier, Map playerRecord) {
        String url = String.format("/services/data/v39.0/sobjects/Account/IMS_Customer_Identifier__c/%s", customerIdentifier);
        log.debug("SF upsert url: {}", url);

        Response resp = given()
                .spec(reqSpec)
                .and().header("Authorization", "Bearer " + SERVICE.getAccessToken())
                .and().contentType(ContentType.JSON)
                .and().body(playerRecord)
                .when()
                .patch(url)
                .then().extract().response();

        return resp;
    }

    public Response getPlayer(String customerIdentifier) {
        String url = String.format("/services/data/v39.0/sobjects/Account/IMS_Customer_Identifier__c/%s", customerIdentifier);
        log.debug("SF get url: {}", url);

        Response resp = given()
                .spec(reqSpec)
                .and().header("Authorization", "Bearer " + SERVICE.getAccessToken())
                .when()
                .get(url)
                .then().extract().response();

        return resp;
    }

    public Response deletePlayer(String internalSfId) {
        String url = String.format("/services/data/v39.0/sobjects/Account/%s", internalSfId);
        log.debug("SF delete url: {}", url);

        Response resp = given()
                .spec(reqSpec)
                .and().header("Authorization", "Bearer " + SERVICE.getAccessToken())
                .when()
                .delete(url)
                .then().extract().response();

        return resp;
    }
}

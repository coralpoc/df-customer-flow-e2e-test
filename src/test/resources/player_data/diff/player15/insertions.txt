| brand_seq  | table         | column             | value |
| 4          | SFMC_CUSTOMER_INFO_EARLY | lastName           | Hopkins |
| 4          | SFMC_CUSTOMER_INFO_EARLY | gender             | X       | Removed  |
| 4          | SFMC_CUSTOMER_INFO_EARLY | bettingCurrency    | USD     |
| 4          | SFMC_CUSTOMER_INFO_EARLY | signupProdVertical | Poker   |
| 4          | SFMC_CUSTOMER_INFO_EARLY | regDateTime        | 2017-10-16 12:10:05.000000000    |
| 4          | SFMC_CUSTOMER_INFO_LIFE | accountType  | grid  |
| 4          | SFMC_CUSTOMER_INFO_LIFE | shopCardCust | null |
| 4          | SFMC_CUSTOMER_INFO_LIFE | salutation               | null                                                |
| 4          | SFMC_CUSTOMER_INFO_LIFE | verificationStatus | null                                       |
| 4          | SFMC_CUSTOMER_INFO_LIFE | accountStatusIMS | Active                                       |
| 4          | SFMC_SEGMENTATIONS | custProdSegmntTopLevel | New   |
| 4          | SFMC_STATUS | selfExclude     | false  |
| 4          | SFMC_STATUS | accountStatus                 | Active                                             |
| 4          | SFMC_STATUS | crmAccountStatusLastUpdated   | NOW-6                                              |
| 4          | SFMC_STATUS | autoSuppress                  | false                                              |
| 4          | SFMC_STATUS | autoSuppressLastUpdated       | NOW-6                                              |
| 4          | SFMC_STATUS | consentExpiryDate             | null                      |
| 4          | SFMC_BET_LIFE | lastBetDateTime       | null   |
| 4          | SFMC_DEPOSIT_LIFE | lastDepositDate          | null |
| 4          | SFMC_DEPOSIT_LIFE | lastWithdrawalDate       | null |
|  4          | SFMC_BET_LIFE | sports3mnthAvgStakeAmt     | 112.21546                                |
|  4          | SFMC_BET_LIFE | casino3mnthAvgStakeAmt   | 2533.21546                                       |
|  4          | SFMC_BET_LIFE | bingo3mnthAvgStakeAmt     | 2808.21345                                       |
|  4          | SFMC_BET_LIFE | liveCasino3mnthAvgStakeAmt      | 523356.21546                                |
|  brand_seq  | table         | column                | value                                              |
|  4          | SFMC_BET_LIFE | lastBetDateTime       | 2018-01-19 01:10:05                                |
|  4          | SFMC_BET_LIFE | lastBetProdType       | AbcdefGHijklmn Opr ! 12345678901234567890123456789 |
|  4          | SFMC_BET_LIFE | lastBetAmt            | 123456.99987                                       |
|  4          | SFMC_BET_LIFE | lastBetOutcomeWinLose | lOse                                               |
|  4          | SFMC_BET_LIFE | totalNumBets          | 223456.98                                          |
|  4          | SFMC_BET_LIFE | totalStakeAmt         | 323456.97                                          |
|  4          | SFMC_BET_LIFE | totalWinAmt           | 423456.96                                          |
|  4          | SFMC_BET_LIFE | totalLossAmt          | 523456.95                                          |
|  4          | SFMC_BET_LIFE | totalGrossWinAmt      | 623456.94                                          |
|  4          | SFMC_BET_LIFE | totalNGR              | 723456.93                                          |
|  4          | SFMC_BET_LIFE | numActiveDays         | 4001                                               |
|  4          | SFMC_BET_LIFE | lastUnsettledBetDate  | 2017-06-01 01:10:00                                |
|  4          | SFMC_BET_LIFE | preferredBingoRoom    | AbcdefGHijklmn Rom ! 12345678901234567890123456789 |
|  4          | SFMC_BET_LIFE | mobilePlayer          | y                                                  |
|  4          | SFMC_BET_LIFE | cashoutCust           | Y                                                  | Removed |
|  4          | SFMC_BET_LIFE | partialCashoutCust    | 1                                                  | Removed |
|  4          | SFMC_BET_LIFE | hasBetOnCasino        | 1                                                  | Removed |
|  4          | SFMC_BET_LIFE | hasBetInPlay          | Y                                                  |
|  4          | SFMC_BET_LIFE | sportsLastBetDate     | 2017-06-02 01:10:02                                |
|  4          | SFMC_BET_LIFE | sportsTotalStakeAmt   | 523156.21654                                       |
|  4          | SFMC_BET_LIFE | sportsTotalNGRAmt     | 523256.21576                                       |
|  4          | SFMC_BET_LIFE | casinoLastBetDate     | 2017-06-03 01:10:03                                |
|  4          | SFMC_BET_LIFE | casinoTotalStakeAmt   | 523356.21546                                       |
|  4          | SFMC_BET_LIFE | casinoTotalNGRAmt     | 523456.21345                                       |
|  4          | SFMC_BET_LIFE | bingoLastBetDate      | 2017-06-04 01:10:08                                |
|  4          | SFMC_BET_LIFE | bingoTotalStakeAmt    | 529456.21469                                       | Removed      |
|  4          | SFMC_BET_LIFE | bingoTotalNGRAmt      | 523556.2146                                        | Removed      |
|  4          | SFMC_BET_LIFE | pokerLastBetDate      | 2017-06-05 01:10:09                                | Removed      |
|  4          | SFMC_BET_LIFE | pokerTotalStakeAmt    | 523656.211                                         | Removed      |
|  4          | SFMC_BET_LIFE | pokerTotalNGRAmt      | 523756.9901                                        | Removed      |
|  4          | SFMC_BET_LIFE | betOnline             | N                                                  |
|  4          | SFMC_BET_LIFE | secToLastBetDate      | 2017-06-06 01:10:07                                |
|  4          | SFMC_BET_LIFE | lastPlayedSlotsDate   | 2017-06-07 01:10:01                                |
|  4          | SFMC_BET_LIFE | lastUsedMainApp       | 2017-06-08 01:10:02                                |
|  4          | SFMC_BET_LIFE | lastUsedSlotsGamesApp | 2017-06-09 01:10:03                                |
|  4          | SFMC_BET_LIFE | sports3mnthAvgStakeAmt     | 112.21546                                |
|  4          | SFMC_BET_LIFE | casino3mnthAvgStakeAmt   | 2533.21546                                       |
|  4          | SFMC_BET_LIFE | bingo3mnthAvgStakeAmt     | 2808.21345                                       |
|  4          | SFMC_BET_LIFE | liveCasino3mnthAvgStakeAmt      | 523356.21546                                |
|  4          | SFMC_LOGIN    | lastLoginDate         | 2015-10-16 12:10:05.000000000                      |
|  4          | SFMC_LOGIN    | dateLastFailedLogin   | 2015-09-17 12:15:05.000000000                      |
|  4          | SFMC_BALANCE  | bonusBalance          | 123456.9876                                        |
|  4          | SFMC_BALANCE  | cashBalance           | 223456.9845                                        |
|  4          | SFMC_BALANCE  | balanceUpdateTime     | 2015-10-16 12:10:05.000000000                      |
|  4          | SFMC_BALANCE  | compPoints            | 0.97                                               |
|  4          | SFMC_BALANCE  | buzzPointsBalance     | 423456.9850                                          |
|  4          | SFMC_BET_EARLY | firstBetDateTime       | 2018-01-19 01:10:05.000000000                      |
|  4          | SFMC_BET_EARLY | firstBetProdType       | FVv0lTSNsAGhIXBDbnbFWEKksrRUKGyOjugNYsjzZIO54m1yuV |
|  4          | SFMC_BET_EARLY | firstBetAmt            | 945367.1255                                        |
|  4          | SFMC_BET_EARLY | firstBetOutcomeWinLose | lose                                               |
|  4          | SFMC_BET_EARLY | secondBetTypeProd      | ubixcGRxQLAT7X3Go7JqrveKGfgZA3PkTh7zzkyZRrQrxgBQuR |
|  4          | SFMC_BET_EARLY | secondBetAmtProd       | 7647454.6712                                       |
|  4          | SFMC_BET_EARLY | secondBetOutcome       | win                                                |
|  4          | SFMC_BET_EARLY | thirdBetTypeProd       | wgCQcIxGV1ChgF4ZZa7Z62hFMvlHRzDfVGj7RfLg0UiDW6FLly | Removed  |
|  4          | SFMC_BET_EARLY | thirdBetAmtProd        | 3435635.66                                         |
|  4          | SFMC_BET_EARLY | thirdBetOutcome        | lose                                               |
|  4          | SFMC_BET_EARLY | sportsFirstBetDate     | 2018-01-19 02:10:05.000000000                      |
|  4          | SFMC_BET_EARLY | casinoFirstBetDate     | 2018-01-20 03:10:05.000000000                      |
|  4          | SFMC_BET_EARLY | bingoFirstBetDate      | 2018-01-21 04:10:05.000000000                      |
|  4          | SFMC_BET_EARLY | pokerFirstBetDate      | 2018-01-22 05:10:05.000000000                      |
|  4          | SFMC_DEPOSIT_EARLY | firstDepositDateTime     | 2015-12-06 00:00:00 |
|  4          | SFMC_DEPOSIT_EARLY | secondDepositDateTime    | 2015-12-07 00:00:00 |
|  4          | SFMC_DEPOSIT_EARLY | thirdDepositDateTime     | 2015-12-08 00:00:00 |
|  4          | SFMC_DEPOSIT_EARLY | firstDepositAmt          | 463728.91234        |
|  4          | SFMC_DEPOSIT_EARLY | secondDepositAmt         | 263728.91768        |
|  4          | SFMC_DEPOSIT_EARLY | thirdDepositAmt          | 663728.91555        | Removed |
|  4          | SFMC_DEPOSIT_EARLY | firstDeclinedDepositDate | 2015-12-09 00:00:00 |
|  4          | SFMC_DEPOSIT_EARLY | firstShopDepositDate     | 2015-12-10 00:00:00 |
|  4          | SFMC_DEPOSIT_EARLY | firstShopDepositAmt      | 863728.91111        | Removed |
|  4          | SFMC_DEPOSIT_LIFE | totalAmtDeposited        | 223456.09909                  |
|  4          | SFMC_DEPOSIT_LIFE | lastDepositDate          | 2017-12-27 15:35:41.000000000 |
|  4          | SFMC_DEPOSIT_LIFE | lastDepositAmt           | 323456.12345                  | Removed  |
|  4          | SFMC_DEPOSIT_LIFE | totalNumDepositDays      | 4004                          | Removed  |
|  4          | SFMC_DEPOSIT_LIFE | lastDeclinedDepositDate  | 2017-12-28 15:35:41.000000000 |
|  4          | SFMC_DEPOSIT_LIFE | lastDepositDayAmt        | 423456.12678                  | Removed  |
|  4          | SFMC_DEPOSIT_LIFE | numFailedDeposit         | 323                           |
|  4          | SFMC_DEPOSIT_LIFE | retailLastShopDepositAmt | 523456.999                    |
|  4          | SFMC_DEPOSIT_LIFE | retailTotalAmtDeposited  | 523456.33123                  |
|  4          | SFMC_DEPOSIT_LIFE | retailLastWithdrawDate   | 2017-12-29 15:35:41.000000000 | Removed  |
|  4          | SFMC_DEPOSIT_LIFE | retailLastWithdrawAmt    | 623456.09123                  | Removed  |
|  4          | SFMC_DEPOSIT_LIFE | totalRetailAmtWithdraw   | 723456.10504                  |
|  4          | SFMC_DEPOSIT_LIFE | retailLastShopDepsitDate | 2017-12-30 15:35:41.000000000 |
|  4          | SFMC_DEPOSIT_LIFE | totalNumDeposit          | 123456.45746                     |
|  4          | SFMC_DEPOSIT_LIFE | lastWithdrawalDate       | 2018-02-20 01:00:00.000000000                      |
|  4          | SFMC_DEPOSIT_RELATIVE | numDepositLast7Days         | 36354636      | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | totalAmtDepositLast7Days    | 2635353.87    | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | numDepositLast28Days        | 47647474      | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | totalAmtDepositLast28Days   | 3436726.67467 |              |
|  4          | SFMC_DEPOSIT_RELATIVE | numDepositPrevPeriod        | 76765454      | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | totalAmtDepositPrevPeriod   | 4764646.56    | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | numDepositCurrPeriod        | 38633766      | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | totalAmtDepositCurrPeriod   | 8463737.76    | out of scope |
|  4          | SFMC_DEPOSIT_RELATIVE | retailAmtDepositLast7Days   | 9837647.12622 | Removed      |
|  4          | SFMC_DEPOSIT_RELATIVE | retailAmtDepositLast28Days  | 1233536.89500 | Removed      |
|  4          | SFMC_DEPOSIT_RELATIVE | retailAmtWithdrawLast7Days  | 8763893.10111 | Removed      |
|  4          | SFMC_DEPOSIT_RELATIVE | retailAmtWithdrawLast28Days | 2345678.99000 | Removed      |
|  4          | SFMC_PREDICTIONS | pvipFlag              | True         |
|  4          | SFMC_PREDICTIONS | pvipScore             | 143895.91578 |
|  4          | SFMC_PREDICTIONS | predLtv               | 143893.91500 |
|  4          | SFMC_PREDICTIONS | predChurnScore        | 143892.416   |
|  4          | SFMC_PREDICTIONS | predWiseGuyScore      | 143894.4101  |
|  4          | SFMC_PREDICTIONS | predWiseGuyFlag       | False        |
|  4          | SFMC_PREDICTIONS | predChurnFlag         | TRUE         |
|  4          | SFMC_PREDICTIONS | featurespaceScore     | 53432        | Removed but not in scope already |
|  4          | SFMC_PREDICTIONS | featurespaceReason    | 123          | Removed but not in scope already |
|  4          | SFMC_PREDICTIONS | bonusUpliftPrediction | 4321         | Removed but not in scope already |
|  4          | SFMC_PREDICTIONS | pharmScore            | 432377.99999 |
|  4          | SFMC_PREDICTIONS | pharmReason           | 1820         |
|  4          | SFMC_CUSTOMER_INFO_EARLY | lastName           | Bloggs-Smyth 4567890123456789012345678901234567890 |
|  4          | SFMC_CUSTOMER_INFO_EARLY | emailAddress       | longfirstname.longlastname@test.ladbrokescoral.com |
|  4          | SFMC_CUSTOMER_INFO_EARLY | sportsbookCustId   | SbCI123456                                         |
|  4          | SFMC_CUSTOMER_INFO_EARLY | bingoVFUsername    | coral:1130000012345678901234                       |
|  4          | SFMC_CUSTOMER_INFO_EARLY | username           | ladblokeJoe234567890123456789012                   |
|  4          | SFMC_CUSTOMER_INFO_EARLY | mobileNumber       | 07000123456                                        |
|  4          | SFMC_CUSTOMER_INFO_EARLY | homeNumber         | +442000123456                                      |
|  4          | SFMC_CUSTOMER_INFO_EARLY | firstName          | Joe 567890123456789012345678901234567890           |
|  4          | SFMC_CUSTOMER_INFO_EARLY | address1           | Adanfairpwllgwyngyllgogerychwyrndrobwllllantysilio |
|  4          | SFMC_CUSTOMER_INFO_EARLY | city               | Cianfairpwllgwyngyllgogerychwyrndrobwlll           |
|  4          | SFMC_CUSTOMER_INFO_EARLY | postcode           | SW12 1AA89                                         |
|  4          | SFMC_CUSTOMER_INFO_EARLY | country            | Coanfairpwllgwyngyllgogerychwyrndrobwllllantysilio |
|  4          | SFMC_CUSTOMER_INFO_EARLY | gender             | M                                                  | Removed |
|  4          | SFMC_CUSTOMER_INFO_EARLY | DOB                | 1982-12-06 00:00:00.000000000                      |
|  4          | SFMC_CUSTOMER_INFO_EARLY | bettingCurrency    | EUR                                                |
|  4          | SFMC_CUSTOMER_INFO_EARLY | IMSProfileId       | Some string 345678901234567890                     |
|  4          | SFMC_CUSTOMER_INFO_EARLY | signupProdVertical | Sports                                             |
|  4          | SFMC_CUSTOMER_INFO_EARLY | regDateTime        | 2005-10-16 12:10:05.000000000                      |
|  4          | SFMC_CUSTOMER_INFO_EARLY | ukCountry          | Northern Ireland                                   |
|  4          | SFMC_CUSTOMER_INFO_LIFE | accountType              | multi-channel                                      |
|  4          | SFMC_CUSTOMER_INFO_LIFE | VIPLevel                 | 156                                                |
|  4          | SFMC_CUSTOMER_INFO_LIFE | VIPLevelChangeDate       | 2018-01-19 01:10:05.000000000                      |
|  4          | SFMC_CUSTOMER_INFO_LIFE | creditCardExpiryDate     | 1234567289                                         |
|  4          | SFMC_CUSTOMER_INFO_LIFE | last4CardDigts           | 8822                                               |
|  4          | SFMC_CUSTOMER_INFO_LIFE | sportsbookStakeFactor    | 4863737.4778                                       |
|  4          | SFMC_CUSTOMER_INFO_LIFE | bonusAbuse               | 1                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | shopCardCust             | True                                               |
|  4          | SFMC_CUSTOMER_INFO_LIFE | shopCardDateTime         | 2018-01-19 03:10:05.000000000                      |
|  4          | SFMC_CUSTOMER_INFO_LIFE | shopCardRegStore         | qFcyptjP2rYjznL1XbC9cYuVrFxmDjp610o5V8YbZUYSoIjFhn |
|  4          | SFMC_CUSTOMER_INFO_LIFE | sportsStakeFacChangeDate | 2018-01-19 04:10:05.000000000                      | Removed  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | hasLadbrokesAccount      | y                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | hasCoralAccount          | N                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | hasGalaBingoAccount      | 1                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | hasGalaCasinoAccount     | 0                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | salutation               | Mr.                                                |
|  4          | SFMC_CUSTOMER_INFO_LIFE | selfExclusionStartDate   | 2018-02-10 00:10:00.000000000                      |
|  4          | SFMC_CUSTOMER_INFO_LIFE | selfExclusionEndDate     | 2018-02-18 00:00:10.000000000                      |
|  4          | SFMC_CUSTOMER_INFO_LIFE | doNotCall       | 0                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | promoEmail      | 1                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | promoSMS        | 1                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | promoDirectMail | 0                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | promoPhone      | 1                                                  |
|  4          | SFMC_CUSTOMER_INFO_LIFE | verificationStatus | inprocess                                       |
|  4          | SFMC_CUSTOMER_INFO_LIFE | accountStatusIMS | Active                                      |
|  4          | SFMC_SEGMENTATIONS | custProdSegmntTopLevel | Unknown                                            |
|  4          | SFMC_SEGMENTATIONS | custProdSegmntMidLevel | abcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | custProdSegmntLowLevel | bbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | brandBehavSuperSegmnt  | cbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | brandBehavSubSegmnt    | 1234567890                                         |
|  4          | SFMC_SEGMENTATIONS | brandValueSuperSegmnt  | dbcde FGHIJ kLmNo Pr123456789                      |
|  4          | SFMC_SEGMENTATIONS | brandValueSubSegmnt    | 1234567890                                         |
|  4          | SFMC_SEGMENTATIONS | sourceOfPlaySegmnt     | ebcde FGHIJ kLmNo Pr123456789012345678901234567890 | Removed |
|  4          | SFMC_SEGMENTATIONS | custChannelSegmnt      | fbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | shopCardMgmntSegmnt    | gbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | lifecycleSegmnt        | hbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | custSportsBetSegmnt    | ibcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | custSportsMarketSegmnt | jbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | custSportsTimeSegmnt   | kbcde FGHIJ kLmNo Pr123456789012345678901234567890 | Removed |
|  4          | SFMC_SEGMENTATIONS | custBonusSegmnt        | lbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | preferredDepositingQOM | mbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | preferredDepositingDOW | nbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_SEGMENTATIONS | paymentPattern         | obcde FGHIJ kLmNo Pr123456789012345678901234567890 | Removed |
|  4          | SFMC_SEGMENTATIONS | custCohortPersona      | pbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
|  4          | SFMC_STATUS | custPreviousVIP               | True                                               |
|  4          | SFMC_STATUS | custsPreviousVIPLevel         | 132                                                |
|  4          | SFMC_STATUS | accountStatus                 | Active                                             |
|  4          | SFMC_STATUS | crmAccountStatusLastUpdated   | NOW-8                                              |
|  4          | SFMC_STATUS | autoSuppress                  | false                                              |
|  4          | SFMC_STATUS | autoSuppressLastUpdated       | NOW-8                                              |
|  4          | SFMC_STATUS | optinEmail                    | Full                                               |
|  4          | SFMC_STATUS | optinSMS                      | Full                                               |
|  4          | SFMC_STATUS | optinPush                     | Full                                               |
|  4          | SFMC_STATUS | optinDirectMail               | Full                                               |
|  4          | SFMC_STATUS | optinOBTM                     | Full                                               |
|  4          | SFMC_STATUS | pokerCRMStatus                | Passed for Poker                                   |
|  4          | SFMC_STATUS | sportsCRMStatus               | Passed for Sports                                  |
|  4          | SFMC_STATUS | casinoCRMStatus               | Passed for Casino                                  |
|  4          | SFMC_STATUS | bingoCRMStatus                | Passed for Bingo                                   |
|  4          | SFMC_STATUS | selfExclude                   | True                                               |
|  4          | SFMC_STATUS | nearestRetailShop             | to2b7b8g  MGiDhqJNqOuKy8x79@D3Wt zLM7KjOCvyENSqAJK |
|  4          | SFMC_STATUS | shopCardAccountType           | NISMwj0WlhHx7$LRJeH rjeONTL a PsSJvpF%WP Wy$WwsxFw |
|  4          | SFMC_STATUS | distanceToShop                | 70.45                                              |
|  4          | SFMC_STATUS | referredAFriend               | true                                               |
|  4          | SFMC_STATUS | sportsStakeFactorPreValue     | tsgrcg                                             |
|  4          | SFMC_STATUS | retailDistanceToPreferredShop | 12.75                                              |
|  4          | SFMC_STATUS | retailNearestShopName         | Watford                                            |
|  4          | SFMC_STATUS | consentExpiryDate             | 2018-01-20 01:00:00.000000000                      |
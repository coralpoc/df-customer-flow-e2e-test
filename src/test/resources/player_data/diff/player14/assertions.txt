| brand_seq | ApiName                      | Value                                              |
| 4         | First_Bet_Amt__c             | 945367.13                                          |
| 4         | Second_Bet_Amt_Prod__c       | 7647454.67                                         |
| 4         | Last_Bet_Amt__c              | 123457                                             |
| 4         | LastBetDateTime__c           | 2015-04-19                                         |
| 4         | Last_Login_Date__c           | 2015-07-22T12:10:05.000+0000                       |
| 4         | BonusBalance__c              | 123456.99                                          |
| 4         | First_Bet_Date_Time__c       | 2018-01-19T01:10:05.000+0000                       |
| 4         | First_Deposit_Date_Time__c   | 2015-12-06T00:00:00.000+0000                       |
| 4         | TotalAmtDeposited__c         | 223456.1                                           |
| 4         | Num_Deposit_Last_7_Days__c   | null                                               |
| 4         | LastName                     | Bloggs-Smyth 4567890123456789012345678901234567890 |
| 4         | FirstName                    | Joe 567890123456789012345678901234567890           |
| 4         | Channel__c                   | In Shop                                            |
| 4         | CustProdSegmntMidLevel__c    | abcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | CustPreviousVIP__c           | true                                               |
| 4         | Old_VIP_Level__c             | 132                                                |
| 4         | Last_Bet_Prod_Type__c        | AbcdefGHijklmn Opr ! 12345678901234567890123456789 |
| 4         | Last_Bet_Outcome_Win_Lose__c | lOse                                               |
| 4         | NumActiveDays__c             | 4001                                               |
| 4         | LastUnsettledBetDate__c      | null                                               |
| 4         | PreferredBingoRoom__c        | AbcdefGHijklmn Rom ! 12345678901234567890123456789 |
| 4         | MobilePlayer__c              | true                                               |
| 4         | HasBetOnCasino__c            | true                                               |
| 4         | HasBetInPlay__c              | true                                               |
| 4         | SportsLastBetDate__c         | 2017-06-02                                         |
| 4         | SportsTotalStakeAmt__c       | 523156.22                                          |
| 4         | SportsTotalNGRAmt__c         | 523256.22                                          |
| 4         | CasinoLastBetDate__c         | 2017-06-03                                         |
| 4         | CasinoTotalStakeAmt__c       | 523356.22                                          |
| 4         | CasinoTotalNGRAmt__c         | 523456.21                                          |
| 4         | BingoLastBetDate__c          | 2017-06-04                                         |
| 4         | PokerLastBetDate__c          | null                                               |
| 4         | PokerTotalStakeAmt__c        | null                                               |
| 4         | PokerTotalNGRAmt__c          | null                                               |
| 4         | BetOnline__c                 | false                                              |
| 4         | Sec_To_Last_Bet_Date__c      | 2017-06-06T01:10:07.000+0000                       |
| 4         | LastPlayedSlotsDate__c       | 2017-06-07                                         |
| 4         | LastUsedMainApp__c           | 2017-06-08                                         |
| 4         | LastUsedSlotsGamesApp__c     | 2017-06-09                                         |
| 4         | Sports3mnthAvgStakeAmt__c      | 112.21546                       |
| 4         | Casino3mnthAvgStakeAmt__c       | 2533.21546                                         |
| 4         | Bingo3mnthAvgStakeAmt__c           | 2808.21345                                         |
| 4         | LiveCasino3mnthAvgStakeAmt__c     | 523356.21546                                         |
| 4         | Brand__c                     | Coral                                              |
| 4         | DateLastFailedLogin__c       | 2015-09-17                                         |
| 4         | CashBalance__c               | 223456.98                                          |
| 4         | BuzzPointsBalance__c         | 423456.98                                          |
| 4         | BalanceUpdateTime__c         | 2015-10-16                                         |
| 4         | CompPoints__c                | 0.97                                               |
| 4         | First_Bet_Prod_Type__c        | FVv0lTSNsAGhIXBDbnbFWEKksrRUKGyOjugNYsjzZIO54m1yuV |
| 4         | First_Bet_Outcome_Win_Lose__c | lose                                               |
| 4         | Second_Bet_Type_Prod__c       | ubixcGRxQLAT7X3Go7JqrveKGfgZA3PkTh7zzkyZRrQrxgBQuR |
| 4         | Second_Bet_Outcome__c         | win                                                |
| 4         | Third_Bet_Type_Prod__c        | null                                               |
| 4         | Sports_First_Bet_Date__c      | 2018-01-19                                         |
| 4         | Casino_First_Bet_Date__c      | 2018-01-20                                         |
| 4         | Bingo_First_Bet_Date__c       | 2018-01-21                                         |
| 4         | Poker_First_Bet_Date__c       | 2018-01-22                                         |
| 4         | Second_Deposit_Date_Time__c    | 2015-12-07T00:00:00.000+0000 |
| 4         | Third_Deposit_Date_Time__c     | 2015-12-08T00:00:00.000+0000 |
| 4         | First_Deposit_Amt__c           | 463728.91                    |
| 4         | Second_Deposit_Amt__c          | 263728.92                    |
| 4         | Third_Deposit_Amt__c           | null                         |
| 4         | First_Declined_Deposit_Date__c | 2015-12-09                   |
| 4         | First_Shop_Deposit_Date__c     | 2015-12-10                   |
| 4         | First_Shop_Deposit_Amt__c      | null                    |
| 4         | Last_Deposit_Date__c         | 2015-07-20                   |
| 4         | LastDepositAmt__c            | null                         |
| 4         | TotalNumDepositDays__c       | null                         |
| 4         | LastDeclinedDepositDate__c   | 2017-12-28                   |
| 4         | LastDepositDayAmt__c         | null                         |
| 4         | NumFailedDeposit__c          | 323                          |
| 4         | RetailLastShopDepositAmt__c  | 523457                       |
| 4         | RetailTotalAmtDeposited__c   | 523456.33                    |
| 4         | RetailLastWithdrawDate__c    | null                         |
| 4         | RetailLastWithdrawAmt__c     | null                         |
| 4         | TotalRetailAmtWithdraw__c    | 723456.11                    |
| 4         | RetailLastShopDepositDate__c | 2017-12-30                   |
| 4         | TotalNumDeposit__c           | 123456                       |
| 4         | Total_Amt_Deposit_Last_7_Days__c  | null        |
| 4         | Num_Deposit_Last_28_Days__c       | null        |
| 4         | Total_Amt_Deposit_Last_28_Days__c | 3436726.67  |
| 4         | Num_Deposit_Prev_Period__c        | null        |
| 4         | Total_Amt_Deposit_Prev_Period__c  | null        |
| 4         | Num_Deposit_Curr_Period__c        | null        |
| 4         | RetailAmtDepositLast7Days__c      | null        |
| 4         | RetailAmtDepositLast28Days__c     | null        |
| 4         | RetailAmtWithdrawLast7Days__c     | null        |
| 4         | RetailAmtWithdrawLast28Days__c    | null        |
| 4         | PVIPFlag__c              | true      |
| 4         | PVIPScore__c             | 143895.92 |
| 4         | PredLTV__c               | 143893.91 |
| 4         | PredChurnScore__c        | 143892.42 |
| 4         | PredWiseGuyFlag__c       | false     |
| 4         | PredWiseGuyScore__c      | 143894.41 |
| 4         | FeaturespaceScore__c     | null      |
| 4         | FeaturespaceReason__c    | null      |
| 4         | BonusUpliftPrediction__c | null      |
| 4         | pHarmScore__c            | 1         |
| 4         | pHarmReason__c           | null      |
| 4         | PersonEmail             | longfirstname.longlastname@test.ladbrokescoral.com |
| 4         | SportsbookCustId__c     | SbCI123456                                         |
| 4         | Bingo_Username__c       | coral:1130000012345678901234                       |
| 4         | Username__c             | ladblokeJoe234567890123456789012                   |
| 4         | PersonMobilePhone       | +447000123456                                      |
| 4         | Phone                   | +442000123456                                      |
| 4         | PersonMailingStreet     | Adanfairpwllgwyngyllgogerychwyrndrobwllllantysilio |
| 4         | PersonMailingCity       | Cianfairpwllgwyngyllgogerychwyrndrobwlll           |
| 4         | PersonMailingPostalCode | SW12 1AA89                                         |
| 4         | PersonMailingCountry    | Coanfairpwllgwyngyllgogerychwyrndrobwllllantysilio |
| 4         | Gender__c               | null                                               |
| 4         | PersonBirthdate         | 1982-12-06                                         |
| 4         | CurrencyIsoCode         | EUR                                                |
| 4         | BettingCurrency__c      | EUR                                                |
| 4         | Profile_ID__c           | Some string 345678901234567890                     |
| 4         | SignupProdVertical__c   | Sports                                             |
| 4         | Registered_Date__c      | 2005-10-16T12:10:05.000+0000                       |
| 4         | UkCountry__c            | Northern Ireland                                   |
| 4         | VIP_Level__c                | 156                                                |
| 4         | VIPLevelChangeDate__c       | 2018-01-19                                         |
| 4         | CreditCardExpiryDate__c     | null                                               |
| 4         | Last4CardDigts__c           | null                                               |
| 4         | SportsbookStakeFactor__c    | 4863737.48                                         |
| 4         | Bonus_Abuse__c              | true                                               |
| 4         | ShopCardCust__c             | true                                               |
| 4         | ShopCardDateTime__c         | 2018-01-19                                         |
| 4         | ShopCardRegStore__c         | qFcyptjP2rYjznL1XbC9cYuVrFxmDjp610o5V8YbZUYSoIjFhn |
| 4         | SportsStakeFacChangeDate__c | null                                               |
| 4         | HasLadbrokesAccount__c      | true                                               |
| 4         | HasCoralAccount__c          | false                                              |
| 4         | HasGalaBingoAccount__c      | true                                               |
| 4         | HasGalaCasinoAccount__c     | false                                              |
| 4         | CustProdSegmntTopLevel__c     | Unknown                                            |
| 4         | CustProdSegmntLowLevel__c     | bbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | BrandBehavSuperSegmnt__c      | cbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | BrandBehavSubSegmnt__c        | 123456                                             |
| 4         | BrandValueSuperSegmnt__c      | dbcde FGHIJ kLmNo Pr123456789                      |
| 4         | BrandValueSubSegmnt__c        | 123456                                             |
| 4         | SourceOfPlaySegmnt__c         | null                                               |
| 4         | CustChannelSegmnt__c          | Fbcde Fghij Klmno Pr123456789012345678901234567890 |
| 4         | ShopCardMgmntSegmnt__c        | gbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | LifecycleSegmnt__c            | hbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | CustSportsBetSegmnt__c        | ibcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | CustSportsMarketSegmnt__c     | Jbcde Fghij Klmno Pr123456789012345678901234567890 |
| 4         | CustSportsTimeSegmnt__c       | null                                               |
| 4         | CustBonusSegmnt__c            | Lbcde Fghij Klmno Pr123456789012345678901234567890 |
| 4         | PreferredDepositingQOMName__c | mbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | PreferredDepositingDowName__c | nbcde FGHIJ kLmNo Pr123456789012345678901234567890 |
| 4         | PaymentPatternName__c         | null                                               |
| 4         | CustCohortPersona__c          | Pbcde Fghij Klmno Pr123456789012345678901234567890 |
| 4         | AccountStatus__c                 | Active                                             |
| 4         | OptinEmail__c                    | Full                                               |
| 4         | OptinSMS__c                      | Full                                               |
| 4         | OptinPush__c                     | null                                               |
| 4         | OptinDirectMail__c               | Full                                               |
| 4         | OptinOBTM__c                     | Full                                               |
| 4         | PokerCRMStatus__c                | Passed for Poker                                   |
| 4         | SportsCRMStatus__c               | Passed for Sports                                  |
| 4         | CasinoCRMStatus__c               | Passed for Casino                                  |
| 4         | BingoCRMStatus__c                | Passed for Bingo                                   |
| 4         | Self_Excluded__c                 | true                                               |
| 4         | SelfExclude__c                   | true                                               |
| 4         | NearestRetailShop__c             | to2b7b8g  MGiDhqJNqOuKy8x79@D3Wt zLM7KjOCvyENSqAJK |
| 4         | ShopCardAccountType__c           | NISMwj0WlhHx7$LRJeH rjeONTL a PsSJvpF%WP Wy$WwsxFw |
| 4         | DistanceToShop__c                | null                                               |
| 4         | ReferredAFriend__c               | false                                              |
| 4         | SportsStakeFactorPreValue__c     | null                                               |
| 4         | AutoSuppress__c                  | false                                              |
| 4         | RetailDistanceToPreferredShop__c | null                                               |
| 4         | RetailNearestShopName__c         | null                                               |
| 4         | CustomerSEQ__c                   | null                                               |
| 4         | Salutation                       | Mr.  |
| 4         | Self_Exclusion_Start_Date__c     | 2018-02-10  |
| 4         | Self_Exclusion_End_Date__c       | 2018-02-18  |
| 4         | Last_Withdrawal_Date__c          | 2015-07-22  |
| 4         | PersonDoNotCall       | false  |
| 4         | Promo_Email__c        | true   |
| 4         | Promo_SMS__c          | true   |
| 4         | Promo_Direct_Mail__c  | false  |
| 4         | Promo_Phone__c        | true   |
| 4         | Verification_Status__c | Active Grace Period |
| 4         | Account_Status__c | Active |
| 4         | MarketingPermissionExpiryDate__c | 2017-07-22 |
| 4         | RetailMobileVerified__c            | false  |
| 4         | First_Retail_Bip_Bet_Date__c       | null |
| 4         | SFMC_Record__c    | true |
| 4         | SFMC_Error__c	    | false |
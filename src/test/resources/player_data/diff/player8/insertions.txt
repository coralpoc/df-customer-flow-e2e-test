| brand_seq  | table         | column             | value |
| 3          | SFMC_CUSTOMER_INFO_LIFE | salutation   | ----  |
| 3          | SFMC_CUSTOMER_INFO_EARLY | bettingCurrency | PLN      |
| 3          | SFMC_CUSTOMER_INFO_EARLY | lastName        | Stephens |
| 3          | SFMC_LOGIN    | lastLoginDate         | 2017-10-16 12:10:05.000000000                      |
| 3          | SFMC_SEGMENTATIONS | custProdSegmntTopLevel | Lotto |
| 3          | SFMC_STATUS | accountStatus                 | Closed                                             |
| 3          | SFMC_STATUS | crmAccountStatusLastUpdated   | NOW-6                                              |
| 3          | SFMC_STATUS | autoSuppress          | True  |
| 3          | SFMC_STATUS | autoSuppressLastUpdated       | NOW-8  |
| 3          | SFMC_STATUS | consentExpiryDate             | 2018-01-20 01:00:00.000000000                      |
| 3          | SFMC_BET_LIFE | lastBetDateTime       | null   |
| 3          | SFMC_DEPOSIT_LIFE | lastDepositDate          | null |
| 3          | SFMC_DEPOSIT_LIFE | lastWithdrawalDate       | null |

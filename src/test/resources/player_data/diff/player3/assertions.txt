| brand_seq | ApiName               | Value     |
| 4         | BetOnline__c          | true      |
| 4         | MobilePlayer__c       | false     |
| 4         | HasBetOnCasino__c     | false     |
| 4         | HasBetInPlay__c       | false     |
| 4         | LastBetDateTime__c    | null      |
| 4         | Brand__c              | Coral     |
| 4         | PVIPFlag__c           | false     |
| 4         | PredWiseGuyFlag__c    | true      |
| 4         | PredChurnFlag__c      | false     |
| 4         | LastName              | Hopkins                          |
| 4         | PersonEmail           | invalid.email@ladbrokescoral.com |
| 4         | PersonMobilePhone     | null                             |
| 4         | Phone                 | null                             |
| 4         | Gender__c             | null                             |
| 4         | CurrencyIsoCode       | GBP                              |
| 4         | BettingCurrency__c    | GBP                              |
| 4         | SignupProdVertical__c | Bingo                            |
| 4         | Channel__c                  | In Shop               |
| 4         | ShopCardCust__c             | true                  |
| 4         | VIP_Level__c                | 999                   |
| 4         | SportsbookStakeFactor__c    | null                  |
| 4         | ShopCardDateTime__c         | null                  |
| 4         | ShopCardRegStore__c         | null                  |
| 4         | HasLadbrokesAccount__c      | false                 |
| 4         | HasCoralAccount__c          | false                 |
| 4         | HasGalaBingoAccount__c      | false                 |
| 4         | HasGalaCasinoAccount__c     | false                 |
| 4         | CustProdSegmntTopLevel__c | Sports |
| 4         | CustPreviousVIP__c | true        |
| 4         | Old_VIP_Level__c   | 999         |
| 4         | AccountStatus__c   | Closed      |
| 4         | OptinEmail__c      | None        |
| 4         | OptinSMS__c        | None        |
| 4         | OptinDirectMail__c | None        |
| 4         | OptinOBTM__c       | None        |
| 4         | PokerCRMStatus__c  | Passed for Poker        |
| 4         | SportsCRMStatus__c | Passed for Sports       |
| 4         | CasinoCRMStatus__c | Passed for Casino       |
| 4         | BingoCRMStatus__c  | Passed for Bingo        |
| 4         | Self_Excluded__c   | true        |
| 4         | SelfExclude__c     | true        |
| 4         | AutoSuppress__c    | true        |
| 4         | UkCountry__c       | null        |
| 4         | CustomerSEQ__c     | null        |
| 4         | Salutation                       | Ms.  |
| 4         | PersonDoNotCall       | true   |
| 4         | Promo_Email__c        | false  |
| 4         | Promo_SMS__c          | true   |
| 4         | Promo_Direct_Mail__c  | true   |
| 4         | Promo_Phone__c        | false  |
| 4         | Verification_Status__c | Under Review |
| 4         | Account_Status__c | Closed |
| 4         | MarketingPermissionExpiryDate__c  | 2017-07-20 |
| 4         | SFMC_Record__c    | true |
| 4         | SFMC_Error__c	    | false |
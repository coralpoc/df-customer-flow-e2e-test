#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@delete-from-db @delete-from-sf @migration @all
Feature: Migration with data from multiple DPL views

  Scenario: Full view migration
	
	Given we insert the data only into the following tables
      | table                     |
      | SFMC_BALANCE              |
      | SFMC_BET_EARLY            |
      | SFMC_BET_LIFE             |
      | SFMC_CUSTOMER_INFO_EARLY  |
      | SFMC_CUSTOMER_INFO_LIFE   |
      | SFMC_DEPOSIT_EARLY        |
      | SFMC_DEPOSIT_LIFE         |
      | SFMC_LOGIN                |
      | SFMC_STATUS               |
      | SFMC_DEPOSIT_RELATIVE     |
      | SFMC_PREDICTIONS          |
      | SFMC_SEGMENTATIONS        |

    And players are inserted into database for feature "diff"
    When trigger lambda for "migration"
 #   Then all "critical" views status should be "SENT_TO_KAFKA" in DDB
#    And all "medium critical" views status should be "SENT_TO_KAFKA" in DDB
 #   And all "low critical" views status should be "SENT_TO_KAFKA" in DDB
 #   And all "non critical" views status should be "SENT_TO_KAFKA" in DDB
   	Then the players are available in SF for "MIGRATION"

@delete-from-db @delete-from-sf @diff @all
Feature: Daily diff with data from multiple DPL views

  Background:
  	Given we insert the data only into the following tables
      | table                     |
      | SFMC_BALANCE              |
      | SFMC_BET_EARLY            |
      | SFMC_BET_LIFE             |
      | SFMC_CUSTOMER_INFO_EARLY  |
      | SFMC_CUSTOMER_INFO_LIFE   |
      | SFMC_DEPOSIT_EARLY        |
      | SFMC_DEPOSIT_LIFE         |
      | SFMC_LOGIN                |
      | SFMC_STATUS               |
      | SFMC_DEPOSIT_RELATIVE     |
      | SFMC_PREDICTIONS          |
      | SFMC_SEGMENTATIONS        |
#      | DIM_RTL_CUSTOMERS_CONNECT |
#      | SF_MASTER_SUBCRIBERS      |
    And players are inserted into database for feature "diff"
    And clean the existing data availability info
    And dynamo db status table cleaned up

  Scenario: Rule 1 - if it is pre-deadline and all views listed below are available, then send them all.
  Includes: 1st batch
  SFMC_CUSTOMER_INFO_EARLY
  SFMC_CUSTOMER_INFO_LIFE
  SFMC_STATUS
  SFMC_BET_EARLY (likelihood that these will not be available)
  SFMC_BET_LIFE (likelihood that these will not be available)
  SFMC_DEPOSIT_EARLY
  SFMC_DEPOSIT_LIFE
  SFMC_DEPOSIT_RELATIVE
  SFMC_BALANCE
  SFMC_LOGIN

  Excludes: 
  Segmentation
  Predictions


    Given set dpl_deadline to 1 hour ahead
    And dpl load status is set to available to start from 5 min ago
      | table                     |
      | SFMC_BALANCE              |
      | SFMC_BET_EARLY            |
      | SFMC_BET_LIFE             |
      | SFMC_CUSTOMER_INFO_EARLY  |
      | SFMC_CUSTOMER_INFO_LIFE   |
      | SFMC_DEPOSIT_EARLY        |
      | SFMC_DEPOSIT_LIFE         |
      | SFMC_LOGIN                |
      | SFMC_STATUS               |
      | SFMC_DEPOSIT_RELATIVE     |
    And trigger lambda for "diff"
    Then all "critical" views status should be "SENT_TO_KAFKA" in DDB
    And all "medium critical" views status should be "SENT_TO_KAFKA" in DDB
    And all "low critical" views status should be "SENT_TO_KAFKA" in DDB
    But table names should "NOT EXIST" in view status in DDB
      | SFMC_PREDICTIONS              |
      | SFMC_SEGMENTATIONS            |
    And the players are available in SF for "DIFF" and batch number "0"
 #   And the joining end time set to 120 minutes ago for tables:
 #     | SFMC_BALANCE             |
 #     | SFMC_BET_EARLY           |
 #     | SFMC_BET_LIFE            |
  #    | SFMC_CUSTOMER_INFO_EARLY |
  #    | SFMC_CUSTOMER_INFO_LIFE  |
  #    | SFMC_DEPOSIT_EARLY       |
  #    | SFMC_DEPOSIT_LIFE        |
  #    | SFMC_LOGIN               |
  #    | SFMC_STATUS              |
  #    | SFMC_DEPOSIT_RELATIVE    |

  #  And all "non critical" views status should be "SENT_TO_KAFKA" in DDB
   # And the players are available in SF for "DIFF" and batch number "0"

  


